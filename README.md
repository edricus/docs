# edricus-docs
## Getting started

### Set a virtual env  
```bash
pip install --user virtualenv  
virtualenv venv  
source venv/bin/activate  
pip install -r requirements.txt
```
## Activate

  `mkdocs serve`

Open up http://127.0.0.1:8000/ in your browser.

