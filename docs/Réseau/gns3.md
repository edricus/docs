<figure markdown>
[![gns3](../src/gns3.png){ width="300" }](https://www.gns3.com/)
</figure>

# GNS3
est un simulateur/emulateur de réseaux informatique.  

### Installation 
??? note "Debian"
    GNS3 n'est plus disponible en .deb depuis Debian 11, on utilise desormais pip.  
    Étape 1: Désinstaller GNS3 et purger avant de faire ces manips  
    ```console
    # apt purge --autoremove gns3-server gns3-gui
    ```
    Étape 2: Installer python-pip
    ```console
    # apt install python3-pip
    ```
    Étape 3: Installer gns3 avec pip et ses dépendances avec apt
    ```console
    # pip3 install gns3-server && pip3 install gns3-gui
    # apt install -y dynamips vpcs python3-sip python3-pyqt5 python3-pyqt5.qtsvg python3-pyqt5.qtwebsockets
    ```
??? note "Ubuntu"
    ```console
    # add-apt-repository ppa:gns3/ppa
    # apt-get update
    # apt-get install gns3-server gns3-gui
    ```
    
### Problèmes courants  

#### Le serveur local ne démarre pas  

Solution : choisir "127.0.0.1" au lieu de "localhost" lors de la configuration  
Ouvrir le port TCP 3080  

#### Les icones sont trop petites  

Solution : ajouter une variable d'environnement QT  

```title="~/.local/share/applications/gns3.desktop"
// sur la ligne "Exec="
Exec=env QT_SCALE_FACTOR=1.15 gns3 %f
// redémarrer l'ordinateur
```  

#### VPCS executable version must be >= 0.6.1 but not a 0.8  

Solution : compiler la dernière version des VPCS

```console
$ sudo apt purge --autoremove -y vpcs 
$ git clone https://github.com/GNS3/vpcs
$ cd vpcs/src
$ ./mk.sh
$ sudo mv vpcs /usr/bin/
```

#### uBridge is not available, path doesn't exist  

Solution : compiler la dernière version de ubridge  

```console
$ sudo apt purge --autoremove -y ubridge
$ sudo apt install -y libpcap-dev
$ git clone https://github.com/GNS3/ubridge
$ cd ubridge
$ make -j $(nproc)
$ sudo make install
```
