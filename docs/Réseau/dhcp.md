# DHCP
pour Dynamic Host Configuration Protocol, est un service qui fournit un bail réseau à un équipement.  
Il utilise l'UDP sur les ports 67 et 68.  
Le DHCP sert à attribuer une IP à une machine.  
## Fonctionnement  
<figure markdown>
![dhcp](../src/dhcp.svg)
</figure>
- **DHCP DISCOVER**  
le client va envoyer une trame sur le broadcast pour trouver un serveur DHCP, les autres machines drop le paquet  
- **DHCP OFFER**  
le serveur DHCP receptionne la trame et propose une IP au client  
- **DHCP REQUEST**  
le client va dire au serveur qu’il accepte  
- **DHCPACK**  
le serveur DHCP va confirmer la réception de cette dernière l’information  
## Fonctionnalitées
Le serveur DHCP peut fournir :  

- Adresse IP/masque  
- Broadcast  
- Passerelle  
- Serveur DNS  
- Serveur NTP  
- Routes statique

## Lexique
#### Bail  
Comme dans l'immobilier, le client va "louer" une IP pour une certaine durée.  
Cette durée est personnalisable.  
Un bail n'est pas annulé si le client se déconnecte, il est annulé seulement à la fin de la durée sauf si rupture ou renouvellement de la part du client.  
#### Étendue
Il va sagir des IPs attribuables au clients, range en anglais.  
#### Exclusion  
Dans un étendue, il est possible d'exclure une IP, faire en sorte qu'elle ne soit pas alouée automatiquement.  

## Installation d'un serveur DHCP sous Linux
### Installation
```console
# apt install isc-dhcp-server
```
### Configuration
IP du serveur : **192.168.0.5**  
Interface : **enp0s3**  
Adresse MAC du client : **08:00:27:78:96:E6**  
##### Interfaces à écouter  
```console title="/etc/default/isc-dhcp-server.conf"
INTERFACESv4="enp0s3"
INTERFACESv6="enp0s3"
```
##### Configuration générale
```console title="/etc/dhcp/dhcpd.conf"
subnet 192.168.0.0 netmask 255.255.255.0 {
  range 192.168.0.10 192.168.0.100;
  range 192.168.0.110 192.168.0.120;
  option domain-name-servers 192.168.0.5;
  option domain-name "konexio.lan";
  option routers 192.168.0.1;
  option broadcast-address 192.168.0.255;
  default-lease-time 600;
  max-lease-time 7200;  
}
```  

??? note "Explications"
    - `subnet` : le sous réseau où l'on va attribuer nos IP  
    - `range` : nos étendues  
    - `domain-name-servers` : le serveur dns, ici le serveur DHCP lui même  
    - `domain-name` : le nom de domaine du serveur dhcp  
    - `routers` : la passerelle attribuée aux clients  
    - `default-lease-time` : le temps du bail par defaut  
    - `max-lease-time` : le temps de bail maximum  

##### Ajouter manuellement un client
```console title="/etc/dhcp/dhcpd.conf"
host client_1 {
  hardware ethernet 08:00:27:78:96:E6;
  fixed-address 192.168.0.80;
}
```
## Installation d'un serveur DHCP sous Windows  
à venir...
