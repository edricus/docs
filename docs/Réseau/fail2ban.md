<figure markdown>
[![fail2ban](../src/fail2ban.png){ width="150" }](https://fail2ban.org/)
</figure>
# Fail2Ban
est un logiciel de prévention d'intrusions.  
Il analyse les logs à la recherche de motifs spécifiques et procède à un bannissement en ajoutant l'IP à [iptables](iptables.md).  
Il offre un niveau de sécurité minimum, fail2ban ne pourra pas protéger une installation non sécurisée, il est **préventif**.
## Installation
```console
# apt install fail2ban
```
## Configuration de la prison
!!! warning "Les fichiers `/etc/fail2ban/fail2ban.conf` et `/etc/fail2ban/jail.conf` ne doivent PAS être modifiés car les mises à jour peuvent  écraser les modifications. Ils peuvent néanmoins servir de référence pour la configuration utilisateur".

On utilise le fichier `/etc/fail2ban/fail.d/custom.conf` pour configurer fail2ban.
```console title="/etc/fail2ban/jail.d/custom.conf"
[DEFAULT]
ignoreip = 127.0.0.1 192.168.1.0/24
maxretry = 3
findtime = 1m
bantime = 1h
```
`ignoreip` : IPs à exclure de la surveillance, ici la boucle locale et le réseau local  
`maxretry` : le nombre de tentatives maximum  
`findtime` : l'intervalle entre les essais, défini le délai de réinitialisation de "maxretry"  
`bantime` : la durée de bannissement

## Configuration des services
On configure fail2ban pour qu'il surveille le service ssh.  
:warning: remplacer "2222" par le port ssh s'il a été modifié, sinon ce paramètre peut être omis.
```console title="/etc/fail2ban/jail.d/custom.conf"
[sshd]
enabled = true
port = 2222
logpath = /var/log/auth.log
maxretry = 3
```

## Activation
```console
# systemctl restart fail2ban
# systemctl enable fail2ban
```
## Tests
Il est nécessaire de tester fail2ban comme tous les services relatifs à la sécurité

### Vérifier l'état des prisons
```console 
# fail2ban-client status  
```
```console
Status
|- Number of jail:      1
`- Jail list:           sshd
```
### Vérifier l'état des services
```console
# fail2ban-client status sshd
```
```console
Status for the jail: sshd
|- Filter
|  |- Currently failed:	0
|  |- Total failed:	0
|  `- File list:	/var/log/auth.log
`- Actions
   |- Currently banned:	0
   |- Total banned:	0
   `- Banned IP list:
```
#### Examiner les logs
Fail2ban dispose aussi de logs plus classiques
```console
# tail -f /var/log/fail2ban.log
```
## Commandes
#### Bannir une IP manuellement
```console
# fail2ban-client set [nom de la prison] banip [IP]
```
#### Dé-bannir une IP manuellement
```console
# fail2ban-client set [nom de la prison] unbanip [IP]
```
