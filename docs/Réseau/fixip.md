# Fixer une IP
**Interface** : enp0s3  
**IP à fixer** : 192.168.1.100  
**Passerelle** : 192.168.1.254  
**Masque** : 255.255.255.0  

## Netplan
est le logiciel par defaut pour fixer les IPs sur Ubuntu Server

```yaml title="/etc/netplan/00-installer-config.yaml"
ethernets:
  enp3s0:
    addresses: [192.168.1.100/24]
      nameservers:
        addresses: [80.67.169.12,80.67.169.40]
  version: 2
```
??? note "Explications"
    **addresses** : l'adresse IP  
    **nameservers** : les serveurs DNS  
    **version** : la version de netplan à utiliser pour la syntaxe

## Interfaces
On le retrouve plutôt sur Debian
```title="/etc/network/interfaces"
auto lo
iface lo inet loopback

auto enp3s0
iface enp3s0 inet static
address 192.168.1.100
netmask 255.255.255.0
gateway 192.168.1.254
```
??? note "Explications"
    bloc lo : configure l'interface "lo" en tant que loopback  
    block enp3s0 :  
      - iface inet static : configure l'interface "enp0s3" en tant que statique  
      - address : l'adresse IP  
      - netmask : masque de sous réseau en décimal  
      - gateway : passerelle  
## nmcli
Dès lors que NetworkManager est installé par defaut  
Determiner le nom de la connection:  
```console
$ nmcli con show
```
Modifier et activer cette connection
```console
# nmcli con mod "Connection filaire 2" ipv4.addresses 192.168.1.100/24
# nmcli con mod "Connection filaire 2" ipv4.gateway 192.168.1.254
# nmcli con mod "Connection filaire 2" ipv4.method manual
# nmcli con up "Connection filaire 2"
```
## Méthode graphique sur GNOME
![fixip-1](../src/fixip-1.png)
![fixip-2](../src/fixip-2.png)
![fixip-3](../src/fixip-3.png)
![fixip-4](../src/fixip-4.png)
