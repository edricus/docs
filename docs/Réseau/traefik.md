[![traefik](../../src/traefik.png){ width="150" align=left}](https://doc.traefik.io/traefik/){ target=_blank }  
# Traefik
Traefik est un "edge-router", il fait office de pont entre internet et les applications interne à notre infrastructure.  
On qualifie ce genre d'application de "reverse proxy".  

## Lexique

- **Edge Router**  
application qui traite les requêtes (externes) et les rediriges vers les bons services (internes). 

- **Entrypoints**  
les ports d'écoute, les requêtes arrivent par là et transmettent les requêtes aux routeurs (de traefik).  

- **Routeurs**
les routeurs (de traefik) vont traiter les requêtes http/tcp et udp et les rediriger vers les applications.  
Ce sont des fonctions qu'on definis dans traefik.  

- **Services**
les services sont des fonctions de traefik fournissant du load balancing.  
On peut faire du health check, des "sticky session" (du load balancing avec des cookies), du round robin, etc.

- **Middlewares**
ce sont des intermediaires entre les routeurs et les services qui vont modifier la requête par exemple pour réécrire le path, compresser la donnée, fournir une authentification dans le navigateur...  

- **Providers**  
les fournisseurs d'informations, des "plugins" à traefik qui vont fournir des informations relatifs a leur service.  
Avec le provider docker par exemple, traefik va avoir accès à tout nos conteneurs et va pouvoir établir des routes pour mieux prendre en charge le comportement de ceux si.  

## Installation
Traefik s'oppère de 2 manières differentes :  

 - un fichier de configuration au format toml ou yaml 
 - à l'aide de commandes docker  

On va deployer Traefik avec docker et l'administrer avec un fichier toml.  

### Configuration basique
On va commencer par deployer traefik avec docker-compose
```docker title="docker-compose.yml"
version: '3'

services:
  traefik:
    image: traefik:v2.10
    ports:
      - "80:80"
      - "443:443"
      - "8080:8080"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - ./traefik.toml:/traefik.toml

networks:
  traefik_network:
    - external=true
```
On ouvre les ports 80 et 443 pour accepter le trafic web.  
Le port 8080 sert à acceder au dashboard de traefik.  
On déclare aussi un réseau "traefik_network", qu'on a pas encore créé; ce qu'on fait maintenant :  
```console
$ docker network create traefik_network
```
Il va servir à mettre toutes nos applications dans un même réseau par soucis d'accessiblité.  
  
Le fichier traefik.toml :
```toml title="traefik.toml"
[providers.docker]
  endpoint = "unix:///var/run/docker.sock"
[api]
  insecure=true
  dashboard=true
[entrypoints]
  [entrypoint.http]
    address=":80"
  [entrypoint.https]
    address=":443"
```
En premier on a un ==provider== : docker.  
Traefik a une fonction pour detecter automatiquement les applications deployées par ce qu'il appèle un provisionneur, ici docker, qu'il faut déclarer.  
On a ensuite un bloc ==api==, dans le sens api de traefik.  
**insecure** : permet de nous connecter à l'api de traefik depuis le port 8080.  
**dashboard** : active le dashboard, celui disponible sur le port 8080.  
Le bloc ==entrypoints== va servir à traiter les informations des differents ports.  
On peut les nommer comme on veut, par exemple [entrypoint.mon-app].  

