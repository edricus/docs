# SSH 
pour Secure Shell, est un protocole de communication à distance.  
Il utilise le port TCP 22 par defaut et chiffre tout le trafic.

## Installation
```console
# apt install ssh
```

## Configuration
```title="/etc/ssh/sshd_config"
Port = 42000				# on change le port par defaut (protection par dissimulation)
PermitRootLogin no			# interdit l'utilisateur root
MaxAuthTries 3				# protège du bruteforce
LoginGraceTime 20			# diminue le durée d'authentification
PermitEmptyPasswords no		# désactive l'authentification des comptes sans mdp
X11Forwarding no			# à désactiver sauf exception
PermitUserEnvironment no	# désactive les variables d'environement
AllowAgentForwarding no		# désactive les tunnels
AllowTcpForwarding no		# désactive les tunnels
PermitTunnel no				# désactive les tunnels
DebianBanner no				# désactive la bannière ssh donnant des infos sur le système
StrictModes yes				# empêche le service sshd de démarrer au cas de problème même mineur
```
On échange ensuite les clés publiques avec 
```console
$ ssh-keygen
$ ssh-copy-id -p <port> <utilisateur>@<ip>
```
Après avoir échangé les clés, on peut désactiver l'authentification par mots de passe.  
On en profite pour désactiver toutes autre méthodes d'authentification, puisqu'on ne les utilisent pas.
```title="/etc/ssh/sshd_config"
PasswordAuthentication no
ChallengeResponseAuthentication no
KerberosAuthentication no
GSSAPIAuthentication no
```

## Port forwarding / Tunnel SSH
On veut se connecter en RDP (port 3389) à un PC dont l'ip est ==192.168.1.150==.  
Ce PC est inacessible depuis notre réseau.  
Par contre on est connecté à un serveur vpn qui se trouve sur ce même réseau (son adresse : 10.8.0.1).  
On va donc faire un tunnel passant par ce serveur pour acceder au PC avec la commande :  
```console
$ ssh -L 3389:192.168.1.150:3389 10.8.0.1
```
Dans notre client RDP, l'adresse du serveur sera localhost.
!!! info "Il n'est pas possible de forward le port 22, dans un scenario où on veut se connecter en ssh à la machine cible, il faudra changer le port source. Exemple: 2222:192.168.1.150:22 10.8.0.1"  

## Proxy SSH
On veut maintenant acceder à un service grâce à un port ouvert sur notre machine distante par exemple une page web.  
La connection ssh se fait dans le terminal, notre navigateur n'a pas accès à la ressource.  
On configure donc ssh comme serveur proxy, en configurant notre navigateur pour passer par ce serveur proxy nouvellement créé, il est possible d'acceder à des services web.  
```console
$ ssh -D 1234 192.168.1.150
```
**On n'ouvre pas le port du service qu'on veut acceder, on ouvre un port qui sera utilisé pour notre proxy.**  
On configure ensuite notre navigateur, exemple avec firefox :
![proxy-ssh-1](../src/proxy-ssh-1.png)  
![proxy-ssh-2](../src/proxy-ssh-2.png)  

## Problèmes
**Demande de mot de passe même avec échange de clés**  
Debug : `/var/log/auth.log`  
- L'authentification par mot de passe se fait que pour l'utilisateur ayant executé `ssh-copy-id`.  
- De mauvaises permissions sur le dossier utilisateur peut faire échouer l'authentification par clé publique.  
```console
$ chmod 750 $HOME
```
**Demande d'interactions**
Pour supprimer les messages du genre "Are you sure you want to continue connecting (yes/no)?", on ajoute les options suivantes  
```
$ ssh -o StrictHostKeyChecking=accept-new . . .
```
On peut aussi faire mettre "no", ce qui va éviter les erreurs du type "REMOTE HOST IDENTIFICATION HAS CHANGED!" 
