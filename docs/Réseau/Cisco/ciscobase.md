# Commandes de base Cisco  
<figure markdown>
[![cisco](../../src/cisco.svg){ width="200" }](https://www.cisco.com/c/fr_fr/)
</figure>
## Les modes
#### Mode utilisateur
Exprimé par un ">", c'est le mode par défaut au démarrage qui ne permet pas de paramétrer le matériel.  
#### Mode privilégié
Accessible avec la commande `enable` ou `en` depuis le mode **utilisateur**.  
Exprimé par "#", c'est le mode qui permet d'accéder aux commandes de dépannage et d'écrire dans la mémoire du routeur.  
#### Mode configuration
Accessible avec la commande `configuration terminal` ou `conf t` depuis le mode **privilégié**.  
Exprimé par "(config)#", c'est le mode qui permet la configuration du matériel.  
#### Mode interface
Accessible avec la commande `interface <interface>` ou `int <interface>` depuis le mode **configuration**.  
Exprimé par "(config-if)#", c'est le mode qui permet la configuration des interfaces du matériel (les ports physiques).  
## Commandes
!!! note "Il y a qu'une seule commande par cellule, le tableau coupe les commandes avec des retours à la ligne"
#### N'importe quel mode
| Commande | Abréviation | Description |
| :- | :- | :- |
| exit | exit | revenir au mode précédent |
| end | end | retourner au mode privilégié |
| do | do | exécute une commande en mode privilégié depuis n'importe quel mode |
| no | no | à mettre devant une commande pour l'annuler |
| ? | ? | lister les paramètres disponibles, s'utilise au milieu d'une commande |  

#### Mode privilégié
| Commande | Abréviation | Description |
| :- | :- | :- |
| #show run | #sh run | commande de dépannage permettant d'accéder à toutes les commandes exécutées sur le matériel |
| #config terminal | #conf t | mode paramétrage |
| #write | #wr | enregistrer la configuration |
| #reload | #reload | redémarrer le routeur |
| #write erase | #wr erase | :warning: supprimer la configuration |
| #delete | #del | :warning: supprime un fichier, on évite généralement de l'utiliser |


