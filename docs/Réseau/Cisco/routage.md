# Routage  
<figure markdown>
[![cisco](../../src/cisco.svg){ width="200" }](https://www.cisco.com/c/fr_fr/)
</figure>
## Routeur
![routeurs.png](../../src/routeurs.png){ width="256" }  
Le routeur est un équipement réseau de couche 3 (OSI).  
Il permet de connecter 2 réseaux ou plus entre eux.  
Il a pour rôle de trouver un chemin et d'envoyer un paquet.  
Quand il connecte le réseau à internet, on l'appelle la passerelle (ou gateway).  
On le représente de la manière qui suit :  
![router.svg](../../src/router.svg){ width="64" }  
#### Ports
Les routeurs disposent de peu de ports contrairement aux switchs, ils sont généralement haut débit.  
Il en existe 2 types :  
**Auxiliaire** : utilisés pour le routage, en Ethernet ![ethernet.png](../../src/ethernet.png){ width=20 }  
**Console** : pour configurer le routeur, souvent en Serial ![serial.png](../../src/serial.png){ width=32 }  

## Paquet
est l'objet que le routeur transporte.  
Il va contenir l'adresse IP source et destination, ainsi que la version du protocole (IPv4 ou IPv6).  
Ci-dessous, il est représenté ==dans la zone bleue==.  
![paquet.svg](../../src/paquet.svg){ width=600 }
#### FCS
pour **Frame Check Sequence**, est une somme de contrôle (checksum) ajoutée à la fin de la trame permetant de detecter des erreurs dans cette dernière.  
#### TTL  
pour **Time To Live**, est une valeur multiple de 2 qui va décrémenter à chaque passage de réseaux.  
Une fois à 0 le paquet va s’autodétruire, utile si le paquet ne trouve pas destinaire car sans TTL il resterait indéfiniment sur le réseau.  
==Elle est inclue dans le paquet.==
#### Version  
Il existe 2 versions du protocole IP:  
IPv4: ip sur 32bits sous la forme 192.168.1.0 (décimal)  
IPv6: ip sur 64bits sous la forme 2001:0db8:3c4d:0015:0000:d234::3eee:0000 (hexadécimal)  

## Routes
Le routeur va créer des routes vers d’autres réseaux.  
Il utilise pour ça une table de routage.  
### Type de routes
##### Connecté  
automatique, le routeur connaît le réseau sur lequel il est connecté.  
##### Dynamique  
semi-automatique, on met en place un protocole qui va définir lui même les routes à partir des routes connectés déclarées.  
Le routeur a besoin d’envoyer des paquets pour savoir en temps réel les routes.  
Efficace pour gagner du temps pour la configuration des routes.  
Génère de la charge sur le réseau et en cas de faux contact le réseau va reporter l’état du routeur défectueux aux autres routeurs.  
##### Statique  
manuel, on indique le réseau sur lequel on va communiquer.  
Plus rapide, pas besoin d'interroger les autres routeurs. Il a déjà des routes et s’y fie.  
Moins on a de routes plus le routage est rapide.  
On se sert des routes statiques pour les petits réseaux.  
On se sert des routes dynamique pour les grands réseaux qui sont trop compliqués à router de façon statique.  
##### Route 0  
Egalement nommée "route par défaut" est la route que le paquet va emprunter si aucune route n'est définie.  
Utile pour les routeurs d'extremitées qui n'on qu'un seul chemin à prendre, pas besoin de définir une route statique.  
La route 0 est définie par l'adresse ==0.0.0.0/0==.  

## Routage dynamique
Permet de gérer les routes automatiquement.  
Pour celà on déclare les routes connectées par lesquelles on veut que les routeurs passent.  
Le méthode de configuration dépend du protocole.  
#### RIPv1 et v2  
envoie la table de routage toute les 30 secondes
#### OSPF  
détecte en temps réel les connections sur le réseau, se base sur la ==métrique==
??? info "Métrique"
	Valeur attribuée à un itinéraire IP pour une interface réseau particulière. Il identifie le coût associé à l’utilisation de cet itinéraire. Par exemple, la mesure peut être évaluée en termes de vitesse de liaison, de nombre de sauts ou de délai.

## Routage “on the stick”
Il y a généralement peu de ports sur un routeur, pour connecter plus de réseaux qu’il n’est possible avec les câbles (un câble par réseau) on utilise des VLAN.  
Le protocole utilisé est 802.1Q.  
Inconvénient : toutes les connections se partage le débit du port  

## Commandes
```title="Configurer une route statique"
#(config) ip route <ip-reseau> <masque> <next-hop>
```
```title="Configurer une route dynamique RIPv2"
#(config)router rip
#(config-router)version 2
#(config-router)network <ip>
```
```title="Configurer une route dynamique OSPF"
#(config)router ospf <pid>
#(config-router) network <ip> <masque> <area>
#(config-router) redistribute-connected
```
```title="Configurer l'encapsulation dot1Q"
Prérequis: configurer une VLAN
#(config)interface gi0/0.<vlan>
#(ifconfig)no shutdown
#(ifconfig)encapsulation dot1q <vlan>
#(ifconfig)ip address <ip> <mask>
```

## Mise en situation
Pour la topologie suivante  
![routage-1.png](../../src/routage-1.png)  
On configurera des routes statiques et ensuite dynamique (OSPF).  
#### Configuration des routeurs
```title="R2 adressage IP"
R2#conf t
R2(config)#int e1/0
R2(config-if)#ip addr 172.16.255.254 255.255.0.0
R2(config-if)#no sh
R2(config-if)#exit
R2(config)# int e1/1
R2(config-if)#ip addr 10.0.10.1 255.255.255.252
R2(config-if)#no sh
R2(config-if)#do wr
```
```title="R3 adressage IP"
R3#conf t
R3(config)#int e1/1
R3(config-if)#ip addr 10.0.10.2 255.255.255.252
R3(config-if)#no sh
R3(config-if)#exit
R3(config)#int e1/3
R3(config-if)#ip addr 172.20.100.254 255.255.255.0
R3(config-if)#no sh
R3(config-if)#exit
R3(config)#int e1/0
R3(config-if)#ip addr 10.0.10.5 255.255.255.252
R3(config-if)#no sh
R3(config-if)#exit
R3(config)#int e1/2
R3(config-if)#ip addr 10.0.10.9 255.255.255.252
R3(config-if)#no sh
R3(config-if)#do wr
```
```title="R4 adressage IP"
R4#conf t
R4(config)#int e1/0
R4(config-if)#ip addr 10.0.10.6 255.255.255.252
R4(config-if)#no sh
R4(config-if)#exit
R4(config)#int e
R4(config)#int e1/1
R4(config-if)#ip addr 172.17.255.254 255.255.0.0
R4(config-if)#no sh
R4(config-if)#do wr
```
```title="R5 adressage IP"
R5#conf t
R5(config)#int e1/2
R5(config-if)#ip addr 10.0.10.10 255.255.255.252
R5(config-if)#no sh
R5(config-if)#exit
R5(config)#int e1/0
R5(config-if)#ip addr 192.168.1.254 255.255.255.0
R5(config-if)#no sh
R5(config-if)#exit
R5(config)#int e1/1
R5(config-if)#ip addr 192.168.2.254 255.255.255.0
R5(config-if)#no sh
R5(config-if)#do wr
```
#### Configuration des VPCs
PC1 : 172.16.0.1/24  
PC2 : 172.20.100.1/24  
PC3 : 172.17.0.1/16  
PC4 : 192.168.1.1/24  
PC5 : 192.168.2.1/24  
#### Routage statique
```title="R2 routage statique"
R2#conf t
R2(config)#ip route 0.0.0.0 0.0.0.0 10.0.10.2
R2(config)#do wr
```
```title="R3 routage statique"
R3#conf t
R3(config)#ip route 172.16.0.0 255.255.0.0 10.0.10.1
R3(config)#ip route 172.17.0.0 255.255.0.0 10.0.10.6
R3(config)#ip route 192.168.1.0 255.255.255.0 10.0.10.10
R3(config)#ip route 192.168.2.0 255.255.255.0 10.0.10.10
R3(config)#do wr
```
```title="R4 routage statique"
R4#conf t
R4(config)#ip route 0.0.0.0 0.0.0.0 10.0.10.5
R4(config)#do wr
```
```title="R5 routage statique"
R5#conf t
R5(config)#ip route 0.0.0.0 0.0.0.0 10.0.10.9
R5(config)#do wr
```
#### Test des routes
Arrivé à ce niveau, on va pinger tout les réseaux depuis le PC1 pour voir si les routes fonctionnent.  
  

![routage-2.png](../../src/routage-2.png){ width=512 }  
(ça fonctionne)  
#### Routage dynamique (OSPF)
!!! info "Je n'ai pas reussi à redistribuer les routes 0"
On va supprimer toute nos routes et les remplacer par de l'OSPF.  
```title="R2 routage OSPF"
R2#conf t
R2(config)#no ip route 0.0.0.0 0.0.0.0 10.0.10.2
R2(config)#router ospf 1
R2(config-router)#network 172.16.0.0 0.0.255.255 area 0
R2(config-router)#network 10.0.10.0 0.0.0.3 area 0
R2(config-router)#do wr
```
```title="R3 routage OSPF"
R3#conf t
R3(config)#no ip route 172.16.0.0 255.255.0.0 10.0.10.1
R3(config)#no ip route 172.17.0.0 255.255.0.0 10.0.10.6
R3(config)#no ip route 192.168.1.0 255.255.255.0 10.0.10.10
R3(config)#no ip route 192.168.2.0 255.255.255.0 10.0.10.10
R3(config)#router ospf 1
R3(config-router)#network 172.20.100.0 0.0.0.255 area 0
R3(config-router)#network 10.0.10.0 0.0.0.3 area 0
R3(config-router)#network 10.0.10.4 0.0.0.3 area 0
R3(config-router)#network 10.0.10.8 0.0.0.3 area 0
```
```title="R4 routage OSPF"
R4#conf t
R4(config)#no ip route 0.0.0.0 0.0.0.0 10.0.10.5
R4(config)#router ospf 1
R4(config-router)#network 172.17.0.0 0.0.255.255 area 0
R4(config-router)#network 10.0.10.4 0.0.0.3 area 0
R4(config-router)#do wr
```
```title="R4 routage OSPF"
R5#conf t
R5(config)#no ip route 0.0.0.0 0.0.0.0 10.0.10.9
R5(config)#router ospf 1
R5(config-router)#network 192.168.1.0 0.0.0.255 area 0
R5(config-router)#network 192.168.2.0 0.0.0.255 area 0
R5(config-router)#network 10.0.10.8 0.0.0.3 area 0
R5(config-router)#do wr
```
#### Test des routes
On va encore pinger tout les réseaux depuis le PC1.  

![routage-3.png](../../src/routage-3.png){ width=512 }  
(ça fonctionne)  
