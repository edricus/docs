# VLANs
pour Virtual Area Network, sont des réseaux locaux virtuels.  
Ils servent à séparer un réseau en sous-réseaux isolés entre eux.  
Ils offrent un plus haut niveau de sécurité et de contrôle sur une infrastructure.  
Ils permettent aussi des économies en équipements physiques.  
Les VLANs on la possibilité de communiquer entre eux grâce aux ==ACL== (Access Control List).  
Les ACL permettent de gérer l'accès aux ressources à partir de listes (blacklist ou whitelist).  

![vlans](../../src/vlans.svg){ width=350 }

## Modes VLAN Cisco
Les VLANs peuvent se configurer pour opérer de différentes manières :  
- **Port access (par défaut)**  
utilisé pour connecter des équipements  
- **Port trunk**  
utilisé pour connecter des switch entre eux pour les VLAN  
- **Port dynamic**  
si le port en face est en mode trunk, bascule automatiquement en mode trunk  

![trunkaccess](../../src/trunkaccess.svg){ width=350 }

## VTP
pour VLAN Trunking Protocol, est un protocole pour gérer les VLAN.  
Au lieu de configurer les VLAN sur chaque switch, on nomme un switch serveur et des switch clients.  
Le switch serveur va transmettre la configuration des VLAN aux clients.  
On peut configurer differents ==domaines== donc differents serveurs.  
Par défaut tous les switch sont en VTP mode server, on nomme donc que des clients.  


## Commandes
##### Commandes de base
- Activer une VLAN (avec un nom)  

```
#(config)vlan 100
#(config-vlan)name <nom>
```

- Faire rejoindre une interface à une VLAN

```
#(config)int FastEthernet 0/1
#(config-if)switchport access vlan 100
```

- Faire rejoindre plusieurs interfaces à une VLAN

```
#(config)int range FastEthernet0/1-10
#(config)switchport access vlan 100
```

- Voir la configuration des VLANs

```
#show vlan brief
```
##### Configuration des modes
- Configurer une interface en mode trunk

```console
#(config) int FastEthernet 0/2
#(config-if) switchport mode trunk
```

- Configurer une interface en mode access

```
#(config)int FastEthernet 0/2
#(config-if)switchport mode access
```

##### Configuration du VTP
- Configurer un serveur VTP

```
#(config) vtp mode server
#(config) vtp domain <domaine>
```

- Configurer un client VTP

```
#(config) vtp mode client
#(config) vtp domain <domaine>
```

- Voir le status VTP

```
#show vtp status
```
