# ACL
pour Access Control Lists, sont des listes de règles qui sont appliquées aux entrées et sorties des ports de routeurs ou de switch.  
Elles permettent d’empêcher certaines communications inter-réseaux.  
Une ACL se place sur le port d’une interface, on la place en général au plus près de la machine/réseau concernée.  
exemple: si on veut bloquer à un poste le port 80 d’un serveur, on va positionner notre ACL sur le port du poste puisque c’est ce dernier qui va essayer d'accéder à ce port.  
## Type d’ACL

- **ACL standard**  
numéro d’identification de 0 à 99, autorise/interdit des IPs réseau  
- **ACL extended**  
numéro d’identification de 100 à 199, autorise/interdit des IPs réseau, IPs hôte et protocoles tcp/udp  

La majeure partie du temps on utilise les ACL étendues.  

## Blacklist vs Whitelist

- **Blacklist**  
Autorise tout et bloque au cas par cas  
- **Whitelist**  
Bloque tout et autorise au cas par cas

La whitelist est le choix le plus sécurisé et colle au [principe de moindre privilège](https://fr.wikipedia.org/wiki/Principe_de_moindre_privil%C3%A8ge), bien que plus compliquée à maintenir.  

## Commandes
Tout ce fait en mode #(config)  

- Autorise/Interdit un réseau source à communiquer avec réseau cible

```
access-list <100-199> <permit/deny> ip <source-ip> <source-wildcard-mask> <dest-ip> <dest-wildcard-mask>
```

- Autorise/Interdit un hôte source à communiquer avec hôte cible

```
access-list <100-199> <permit/deny> ip host <source-ip> host <dest-ip>
```

- Autorise/Interdit un réseau source à communiquer avec le port d’un hôte cible

```
access-list <100-199> <permit/deny> <tcp/udp> <source-ip> <source-wildcard-mask> host <dest-ip> eq <port>
```

!!! warning "Une whitelist se termine toujours par `access-list <100-199> deny ip any any`"  
!!! warning "Une blacklist se termine toujours par `access-list <100-199> permit ip any any`"
Supprimer cette dernière règle entraîne la suppression des règles précédentes, ==COPIER== ces commandes avant de la supprimer.  
Une ACL standard permet QUE de filtrer les IP réseau, pas les IP hôtes.  
Quand on indique l’IP d’un équipement: `ip host <ip>`  
Quand on indique l’IP d’un réseau: `ip <ip> <wildcard-mask>`  
Quand on indique le port d’un protocole `<tcp/udp>` avant l’ip et `eq <port>` à la fin  
## Exemple d'ACL
![acl-exemple](../../src/acl-exemple.png){ width="550" }  
==Whitelist==  
Autorise le reseau **192.168.1.0/24** à acceder au port **80** du **Server0** (192.168.30.1)  
Sur Router2:
```
#(config) access-list 100 permit tcp 192.168.1.0 0.0.0.255 host 192.168.30.1 eq 80
#(config) access-list 100 deny ip any any
#(config) int gi0/2
#(config-if) ip access-group 100 in
```
==Blacklist==  
Interdit **Server0** (192.168.30.1) à communiquer avec **Server1** (192.168.40.1)  
Sur Router2:  
```
#(config) access-list 110 deny ip host 192.168.30.1 host 192.168.40.1
#(config) access-list 110 permit ip any any
#(config) int gi0/1
#(config-if) ip access-group 110 in  
```

