# UFW
Juste à titre indicatif car il faut utiliser iptables si possible

## Désactiver les règles IPv6 ajoutés automatiquement par UFW
```title="/etc/ufw/ufw.conf"
IPV6=no
```
Recharger les règles
```console  
# ufw reload
```
## HTTP / HTTPS
```console
# ufw allow 80/tcp
# ufw allow 443/tcp
```
## SSH (strict)
Entrant
```console
# ufw allow from 192.168.1.0/24 port <port-ssh> proto tcp            
```
Sortant
```console
# ufw allow from 127.0.0.1 to <ip-cible> port <port-ssh> proto tcp   
```
## Whitelist / Bloquer tout ports entrant
```console
# ufw default deny incoming
```

