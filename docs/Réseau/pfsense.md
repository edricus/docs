<figure markdown>
[![pfsense.png](../src/pfsense.png){ width="300" }](https://www.pfsense.org/)
</figure>
# pfSense
est une distribution FreeBSD open-source servant de routeur et de pare-feu.  

??? note "Mise en place avec VirtualBox"
    #### Prérequis 
    Une autre VM connectée au réseau interne "intnet"
    #### Caractéristiques

    - **CPU** : 1  
    - **Stockage** : 1 Go  
    - **Réseau 1** : NAT  
    - **Réseau 2** : Interne (intnet)  
    - **Stockage** : 15 Go  
    
    ### Création
    Les commandes si dessous vont créer la VM
    ```console
    $ export vdi=$(VBoxManage list systemproperties | grep "machine folder" | cut -d: -f2 | sed 's/  //g')
    $ VBoxManage createvm --name pfSense --ostype "Linux_64" --register
    $ VBoxManage modifyvm pfSense --cpus 1 --memory 1024 --vram 16 --graphicscontroller vmsvga \
      --nic1 nat --nic2 intnet
    $ VBoxManage createhd --filename "$vdi/pfSense/pfSense.vdi" --size 15360 --format VDI
    $ VBoxManage storagectl pfSense --add sata --name SATA
    $ VBoxManage storageattach pfSense --storagectl SATA --port 0 \
      --type hdd --medium "$vdi/pfSense/pfSense.vdi"
    $ VBoxManage storageattach pfSense --storagectl SATA --port 1 \
      --type dvddrive --medium emptydrive
    ```
    Il ne reste plus qu'à attacher le CD
    ```console
    $ VBoxManage storageattach pfSense --storagectl SATA --port 1 \
      --type dvddrive --medium "/chemin/de/iso"
    ```
    !!! warning "Penser à retirer le CD à la fin de l'installation : ==Devices > Optical Drives > décocher pfsense== sinon la VM va redémarrer en boucle sur l'installation"

## Installation
On choisis les options par défaut  
![pfsense-2.png](../src/pfsense-2.png){ width="500" }
![pfsense-3.png](../src/pfsense-3.png){ width="500" }
![pfsense-4.png](../src/pfsense-4.png){ width="500" }  

### Configuration
!!! info "Pour passer en AZERTY: `kbdcontrol -l /usr/share/syscons/keymaps/fr.iso.kbd` (non permanent)"
#### Étape 1 : Configuration des IPs  

- **NAT** : laisser le DHCP  
- **Interne** : ==192.168.0.254==  

Choisir l'option 2 pour configurer les IPs
![pfsense-5.png](../src/pfsense-5.png){ width="500" }  

Choisir l'interface ==LAN==  
![pfsense-6.png](../src/pfsense-6.png){ width="500" }  

#### Étape 2 : Configuration utilisateur
  
On va ensuite sur l'autre VM et on se connecte à l'interface web à l'adresse ==192.168.0.254==  

**Login**: admin  
**Mot de passe**: pfsense  
![pfsense-7.png](../src/pfsense-7.png){ width="500" }  

Arrivé sur cet interface il faudra ==créer un nouvel utilisateur== et ==désactiver l'utilisateur admin==  
On va pour se faire dans "Change the password in the User Manager"  
![pfsense-8.png](../src/pfsense-8.png){ width="700" }  

Pour désactiver l'utilisateur admin  
![pfsense-9.png](../src/pfsense-9.png){ width="400" }  

Et on va ensuite créer un nouvel utilisateur  
![pfsense-10.png](../src/pfsense-10.png){ width="650" }  

Remplir les informations de base et l'ajouter dans le groupe admin en appuyant sur  
==Move to "Members of" list==  
![pfsense-11.png](../src/pfsense-11.png){ width="600" }  
