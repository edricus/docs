# Commandes réseau en vrac
## Désactiver le powersave wifi
```title="/etc/NetworkManager/conf.d/default-wifi-powersave-on.conf"
[connection]
wifi.powersave = 2
```
```console
# systemctl restart NetworkManager
```
Pour les RPI :
```title="/etc/rc.local"
iw wlan0 set power_save off
```

## Ports en écoute
```console
$ ss -ltn
```
## Processus attachés au port
```console
# lsof -i :<port>
```
## nmcli
Scanner le reseau
```console
$ nmcli d wifi
```    
Se connecter au reseau pour la première fois
```console    
$ nmcli d wifi connect <SSID> -a
```    
Se connection à un reseau existant
```console
$ nmcli con <SSID>
```
## Configurer WakeOnLan avec ethtool
```console
# apt install ethtool
```
```console
$ ip a | grep enp
```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
2: enp0s31f6: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
3: tun0: <POINTOPOINT,MULTICAST,NOARP,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN group default qlen 500
```
```console
# ethtool enp0s31f6 | grep Wake-on
```
```
Supports Wake-on: pumbg
Wake-on: d
```
`d` (disabled), `p` (PHY activity), `u` (unicast activity), `m` (multicast activity), `b` (broadcast activity), `a` (ARP activity), and `g` (magic packet activity)  
Le WakeOnLan est désactivé ici (`d`), il faut qu'il soit à la valeur `g`.  
```
# ethtool -s enp0s31f6 wol g
```
Il faut maintenant rendre cette configuration persistante :
```title="/etc/systemd/system/wol@enp0s31f6.service"
[Unit]
Description=Wake-on-LAN for %i
Requires=network.target
After=network.target

[Service]
ExecStart=/usr/bin/ethtool -s %i wol g
Type=oneshot

[Install]
WantedBy=multi-user.target
```
```console
# systemctl daemon-reload
# systemctl enable --now wol@enp0s31f6
```
On note ensuite l'adresse MAC de la carte réseau
```console
$ ip a s enp0s31f6 | grep -o -E ..:..:..:..:..:.. | cut -f1 -d $'\n'
```
Maintenant on peut eteindre la machine, et, depuis une autre machine (linux):  
```console
# apt install wakeonlan
$ wakeonlan <adresse-mac>
```  

## Changer les serveurs DNS  
!!! info "Les serveurs DNS choisis en exemple sont ceux du [FDN](https://www.fdn.fr/)"  
### Méthode sale  
Peux poser problème pour les reseaux necessitant un portail de connection
```title="/etc/resolv.conf"
nameserver 80.67.169.12
nameserver 80.67.169.40
```    
```console
# chattr +i /etc/resolv.conf
```  
??? "Pour enlever l'immunité au cas où le fichier doit être modifié"  
    chattr -i /etc/resolv.conf

### Méthode clean  
À faire pour chaque point d'accès
```console
$ nmcli con mod <SSID> ipv4.dns "80.67.169.12 80.67.169.40" ipv4.ignore-auto-dns yes
```
