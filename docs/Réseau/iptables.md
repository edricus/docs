# IPtables
est un outil en ligne de commande permettant de configurer Netfilter, un pare-feu implémenté dans Linux.  
IPtables permet de créer des règles de filtrages basés sur des blacklists ou des whitelists.  
Les distributions RedHat, centos et [Fedora](../Système/Distributions/fedora.md) n’utilisent pas IPtables mais firewalld pour gérer Netfilter.  
  

## Commandes
##### Lister les règles
```console
# iptables -L
```
##### Supprimer toutes les règles
```console
# iptables -F
# iptables -X
```
##### Supprimer toutes les règles de la table NAT
```console
# iptables -t NAT -F
# iptables -t NAT -X
```
##### Supprimer une règle
```console
# iptables -L --line-number
# iptables -D INPUT <n° ligne>
```
##### Autoriser les ports entrant (web)
```console
# iptables -A INPUT -p tcp --dport 80 -j ACCEPT
# iptables -A INPUT -p tcp --dport 443 -j ACCEPT
```
##### Autoriser le réseau local entrant
```console
# iptables -A INPUT -i lo -j ACCEPT
```
##### Autoriser le traffic d'un connection déjà établie
```console
# iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
```
##### Bloquer le réseau entrant
```console
# iptables -P INPUT DROP
```
##### Bloquer la chaine FORWARD (utilisé pour le routage)
```console
# iptables -P FORWARD DROP
```
##### Sauvegarder les règles
```console
# apt install iptables-persistant
# iptables-save > /etc/iptables/rules.v4
```
