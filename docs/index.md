# Edricus Documentation
![cover](src/cover2.jpg){ width="700" }


## Introduction
Ce projet vise à répertorier  mes connaissance en matière d'aministration Linux et Windows  
## Système
Memo des différentes commandes sur Linux  

- [Hardening](Système/hardening.md)  
- [Docker](Système/docker.md)  
- [Kubernetes](Système/kubernetes.md)  
- [Vagrant](Système/vagrant.md)  
- [Ansible](Système/ansible.md)  
- [Terraform](Système/terraform.md)  
- [MacOS](Système/macos.md)  
- [Samba](Système/samba.md)  
- [Deluge](Système/deluge.md)  
- [Wine](Système/wine.md)  
- [Git](Système/git.md)  
- [Virtualbox](Système/virtualbox.md)  
- [FFMPEG](Système/ffmpeg.md)  
- [Syncthing](Système/syncthing.md)  
- [Bash](Système/bash.md)  
- [SystemD](Système/systemd.md)  
- [Commandes système en vrac](Système/vracsysteme.md)  
- [Lexique système](Système/syslexique.md)  

## Distributions  
Commandes de bases et premiers pas sur differentes distributions  

- [Debian](Système/Distributions/debian.md)  
- [Ubuntu](Système/Distributions/ubuntu.md)  
- [Fedora](Système/Distributions/fedora.md)  
- [Alpine](Système/Distributions/alpine.md)  
- [OpenBSD](Système/Distributions/openbsd.md)  

## Réseau
Memo des différentes commandes réseau sur Linux  

- [pfSense](Réseau/pfsense.md)  
- [GNS3](Réseau/gns3.md)  
- [Fixer une IP](Réseau/fixip.md)  
- [Installer un serveur DHCP](Réseau/dhcp.md)  
- [IPtables](Réseau/iptables.md)  
- [UFW](Réseau/ufw.md)  
- [SSH](Réseau/ssh.md)  
- [Fail2Ban](Réseau/fail2ban.md)  
- [Traefik](Réseau/traefik.md)  
- [Commandes réseaux en vrac](Réseau/vracreseau.md) 

## Cisco
Les routeurs et switch Cisco  

- [Commandes de base](Réseau/Cisco/ciscobase.md)
- [VLANs](Réseau/Cisco/vlans.md)  
- [Routage](Réseau/Cisco/routage.md)
- [ACLs](Réseau/Cisco/acl.md)  

## Windows
Mise en place de Windows Server et de ses composants  

- [Active Directory](Windows/adds.md)
- [Réparer Windows après un redimensionnement](Windows/fix-windows-boot.md)  

## Guides
Des procedures completes  

- [Reverse-proxy avec SSL](Guides/ssl-reverseproxy.md)  
- [Installer un serveur Minecraft sur Debian 12](Guides/minecraft.md)  
- [Migrer Nextcloud vers Docker](Guides/migrer-nextcloud.md)  
- [Chiffrer un disque dur](Guides/chiffrer-disque-dur.md)  
- [Deploiement de Wordpress avec EKS](Guides/wordpress-eks.md)  
- [Configurer un boot silencieux](Guides/silent-boot.md)  
- [Configurer un serveur DHCP](Guides/dhcp.md)  
- [Activer l'hibernation sous Linux](Guides/hibernate.md)  
- [Mise en place de Steam Remote Play avec un WoL](Guides/wol.md)  
- [Faire fonctionner le lecteur d'empreinte sur Debian](Guides/fingerprint.md)
- [Migrer de trusted.gpg vers trusted.gpg.d](Guides/apt-key-deprecation.md)  

## Hacking
Des exploits  

- [Bypass l'authentification Windows 10](Guides/Hacking/bypass-windows-password.md)  
- [Installation et activation de produits Microsoft](Guides/Hacking/microsoft-warez.md)  
- [Augmenter artificiellement son ratio avec joal server](Guides/Hacking/ratio.md)  

## Azure
Des guides pour Microsoft Azure  

- [Deployer une landing zone avec Terraform](Azure/landing-zones.md)  
- [Synchroniser Azure DevOps et Azure Ressource Manager](Azure/link-devops-rg.md)  
- [Nextcloud avec Azure](Azure/nextcloud-azure.md)  
- [Utiliser Docker-Compose sur Azure](Azure/docker-compose-azure.md)  

## Android
- [Rooter un téléphone Android](Guides/Android/root.md)  
- [ADB](Guides/Android/adb.md)  

## Switch
- [Jailbreaker sa Switch](Guides/Switch/jailbreak-switch.md)  
- [Mise à jour d'Atmosphère](Guides/Switch/update-switch.md)  
- [Présentation de Tinfoil](Guides/Switch/tinfoil.md)  
- [Plugins et scripts](Guides/Switch/plugins-switch.md)  

## Veille
Selection de contenu servant de memo  
Bonnes pratiques, débats, formations, chaines Youtube...  

- [Vidéos](Veille/videos.md)  

## Cours  
Des cours complets  

- [Le modèle OSI](Cours/osi.md)  
- [Le protocole IP](Cours/ip.md)  
- [Cloud Computing](Cours/cloud.md)  
