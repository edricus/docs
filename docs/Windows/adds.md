# Active Directory
<figure markdown>
![ad-logo.svg](../src/ad-logo.svg){ width=450 }
</figure>
est un service d'annuaire de Microsoft inclus dans Windows Server.  
C'est une liste ordonnée de toutes les ressources présentes sur un réseau informatique.  
Il utilise le protocole ==LDAP== pour **Lightweight Directory Access Protocol**, utilisant le port TCP/389.  

## Objets AD
sont des informations contenues dans l'AD.  
Il en existe 3 grands types:  

- **Utilisateurs**  
ce sont les comptes qui vont pouvoir acceder aux ressources de l'AD.  
On leur attribut des ==rôles==, parmis les plus connus: 
	- Administrateur  
	dispose de toutes les permissions du domaine
	- Backup Operator  
	Rôle permettant l’accès à l’ensemble des droits en lecture sur toutes les permissions afin d’effectuer des sauvegardes
	- Cert Publishers  
	Rôle permettant de publier des certificats sur l’Active Directory
	- Allowed RODC Password Replication Group  
	Rôle autorisant la copie de l’annuaire LDAP et des mots de passe contenus dans celui-ci
- **Ressources**  
sont les elements materiels, tel que les ordinateurs et les imprimantes partagés dans l'AD.  
- **Groupes**  
gère les droits des utilisateurs sur les ressources.  

## Organisation des objets AD
<figure markdown>
![ou](../src/ou.svg){ width=550 }  
</figure>
- Les objets (domain components, ou DC) sont contenus dans une ==unité d'organisation==.  
- Les unités d'organisation (organisation units, ou OU) sont contenues dans des ==domaines==.  
Il est commun d'utiliser une OU pour hiérarchiser et catégoriser les services d’une entreprise.  
- L'ensemble des domaines est appellé une ==fôret==.  
On possède plusieurs domaines dès lors qu'on opère sur plusieurs zones géographiques.  

En pratique :  
![adds-16.png](../src/adds-16.png){ width=550 }
## Mise en place d’un Active Directory
#### Étape 1 : Installation du rôle Active Directory  
![adds-1](../src/adds-1.png)  
![adds-2](../src/adds-2.png){ width=500 }  
#### Étape 2 : Délégation du serveur au rôle d'Active Directory
!!! warning "Il ne faut pas déléger le rôle d'Active Directory au compte "Administrateur" sans créer au préalable un autre utilisateur administrateur car le compte utilisé sera ==converti== et si aucun autre compte administrateur est présent sur la machine on perd l'accès en administrateur à la machine."
![adds-3](../src/adds-3.png){ width=350 }  
![adds-4](../src/adds-4.png){ width=500 }  
!!! note "Si un service DHCP était présent avant l’installation de l'AD, il faut l’autoriser. Dans le gestionnaire DHCP, faire clic-droit sur le nom du serveur est "Autoriser""
#### Étape 3 : Ajout d'un utilisateur  
![adds-5](../src/adds-5.png){ width=300 }  
![adds-6](../src/adds-6.png){ width=400 }  
![adds-7](../src/adds-7.png){ width=400 }  

## Rejoindre un domaine
Maintenant que l'AD est installé sur le serveur, il faut connecter des clients.  
Sur une machine Windows 10 :  
![adds-8](../src/adds-8.png){ width=600 }  
![adds-9](../src/adds-9.png){ width=400 }  
![adds-10](../src/adds-10.png){ width=250 }  
![adds-11](../src/adds-11.png){ width=400 }  
![adds-12](../src/adds-12.png){ width=200 } ![adds-13](../src/adds-13.png){ width=220 }  

## GPO
pour **Group Policy Object**, sont des outils permettant la gestion des ordinateurs et des utilisateurs dans un environnement Active Directory.  
Il en existe 2 types :  

- Configuration ordinateur  
pour tous les utilisateurs se connectant sur l'ordinateur, redémarrage requis.  
![adds-14](../src/adds-14.png){ width=200 }  
- Configuration utilisateur  
pour l’utilisateur se connectant sur tous les ordinateurs, déconnection requise.  
![adds-15](../src/adds-15.png){ width=200 }  

#### Création d'une GPO
On va prendre la modification de l'arrière plan dans cet exemple
![adds-17](../src/adds-17.png){ width=300 }  
![adds-18](../src/adds-18.png){ width=300 }  
![adds-19](../src/adds-19.png){ width=300 }  
![adds-20](../src/adds-20.png){ width=300 }  
![adds-21](../src/adds-21.png){ width=300 }  
![adds-22](../src/adds-22.png){ width=600 }  
![adds-23](../src/adds-23.png){ width=300 }  
![adds-24](../src/adds-24.png){ width=300 }  
