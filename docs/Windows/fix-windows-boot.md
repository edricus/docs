# Réparer l'EFI de Windows
Après le redimensionnement d'une partition Windows, il est possible que ce dernier ne démarre plus.  
Dans ce cas là il faudra supprimer le repertoire Microsoft de la partition EFI et le reconstruire avec la commande `bcdboot`.  

## Prérequis

- Une clé d'installation Windows
- Disque dur non-chiffré

## Booter l'ISO et acceder à un terminal
- Booter sur l'ISO d'installation
- Selectionner "Réparer l'ordinateur"
- Dépannage
- Invite de commandes

## Identifier la partition Windows
Pour lister les volumes :
```shell
> diskpart
DISKPART> list vol
```
Identifier la partition Windows en listant les fichiers des differents volumes avec la commande `dir`  
exemple: `dir C:`  
En général la partition Windows se trouve sur C: ou D:

## Monter la partition EFI dans l'installateur

```shell
> diskpart
DISKPART> list disk
DISKPART> select disk <n° du disque avec la partition EFI>
DISKPART> select partition 1
DISKPART> assign letter=V
```

La partition EFI est disponible sur V:

## Suppression du repertoire Microsoft de la partition EFI

```shell
> V:
V:> rmdir EFI\Microsoft
```

## Reconstruction du repertoire EFI

```shell
> bcdboot C:\Windows /s V: /f UEFI
```
C:\Windows : remplacer la lettre au besoin  
/s : spécifie le volume EFI  
/f UEFI : spécifie UEFI ou Legacy  

Une fois l'opération terminée, on peut redémarrer le PC.
??? bug "Si ça ne fonctionne pas"
    Il faudra formater la partition EFI et refaire la commande bcdboot
    ```shell
    > diskpart
    DISKPART> list disk
    DISKPART> select disk <n° disque avec Windows>
    DISKPART> list part
    DISKPART> select part <n° partition EFI (normalement 1)>
    DISKPART> format fs=fat32 quick label=Boot
    DISKPART> exit
    > bcdboot C:\Windows /s V: /f UEFI
    ```

