# Actions de base Windows
Executer un programme rapidement : Win+R
### Gestion d'utilisateurs
Netplwiz permet de gerer les utilisateurs
![netplwiz](../src/netplwiz.png)
#### En ligne de commande
```powershell
New-LocalUser "NEW_ACCOUNT_NAME" -Password $Password -FullName "USER_FULL_NAME" -Description "DESCRIPTION"
```
### GPO
pour Group Policy Object, sont des outils permettant la gestion des ordinateurs et des utilisateurs dans un environnement Active Directory.  
**2 types**
- Stratégie = tout le temps, bloquée
Configuration ordinateur (tous les utilisateurs se connectant sur l'ordinateur), on doit redémarrer pour appliquer la GPO
- Préférence = une seule fois, modifiable par la suite par l’utilisateur
Configuration utilisateur (l’utilisateur se connectant sur tous les ordinateurs), on doit se déconnecter pour appliquer GPO

### Utilisateur dans un domaine vs local
Un utilisateur dans un domaine ne peut pas modifier des paramètres locaux et vise-versa.  
Pour se connecter en local après la création d'un domaine, il faut mettre ==.\Administrateur==.
