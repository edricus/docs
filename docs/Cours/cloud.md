# Cloud Computing
désigne des services de traitement informatique sur des serveurs distants vendus en tant que prestations.  
Ces services incluent les serveurs, le stockage, les bases de données, le réseau, les logiciels analytiques etc…  
L’objectif est de **flexibiliser** les coûts via la facturation à l’usage.  
!!! warning "Si on sait mal s’en servir, ça peut revenir plus cher. Exemple: laisser une machine tourner, mal choisir la performance de la VM, mal choisir son type de service…"  

## Leaders
Les acteurs américains dominent le marché  
Amazon avec **AWS**  
Microsoft avec **Azure**  
Google avec **Google Cloud Platform**  
  

![leaders](../src/leaders.svg){ width=500 }  
  
## Acteurs europeen
À cause néanmoins du [Patriot Act](https://fr.wikipedia.org/wiki/USA_PATRIOT_Act){target=_blank} aux États-Unis obligant la transparance des données sur les applications hébérgées sur le sol américain, il est nécessaire pour les autres pays de développer leurs propres services cloud au risque de ne pas garantir la confidentialité des données.  

Certains services français très importants on choisis d’utiliser les acteurs américains, comme L’Assurance Maladie et la SNCF.  

Les pricipaux acteurs Français sont **Scaleway** et **OVH**.  

## Types de services cloud
Il existe 3 type de services de Cloud  

### IaaS
louer une infrastructure (serveurs, stockage, réseau).  
Tout ce qui est physique est géré par le fournisseur, le logiciel est gérée par nous même  

### PaaS
louer une plateforme.  
Le fournisseur s’occupe du physique, OS et des dépendances.   
On gère la base de donnée et l’application  

### SaaS
louer une application.  
On utilise l’application du fournisseur et on a aucun contrôle sur son installation et sa gestion (en dehors des options que le fournisseur met à notre disposition à l'intérieur de l’application)  
exemple: Netflix, Google Drive, boite mail  

| On-Site | IAAS | PAAS | SaaS |
| :- | :- | :- | :- |
| Applicatio | ns 
| Data			 |
| Runtime			
| Middleware | 
| OS
| Virtualiza | tion
| Servers
| Storage
| Networking | 

