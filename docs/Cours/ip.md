# Le protocole IP
## Introduction
Ce protocole permet de gérer l’acheminement des paquets d’une machine à une autre.  
Un protocole informatique designe les règles et conventions régissant l'échange de données entre ordinateurs.  
Il se situe dans la couche 3 du ==modèle OSI==.  
Il existe 2 types d'adresses IP :  
### IPv4  
encore utilisé aujourd’hui, est composé de 4 octets (32 bits) en décimal, chaque octets est séparés par un point.  
##### exemple: 192.168.1.1  
Avec 32 bits le nombre total d'adresses IP pouvant exister est de ==4 294 967 296==  
4 milliards, ce n'est pas tant que ça compte tenu de la croissance d'internet.  
Pour remedier à la pénurie d'adresse IPv4, l'IPv6 a été inventé.  
### IPv6  
nouveau protocole permettant d'accueillir beaucoup plus d'addresses, est composé de 8 octets (64 bits) en hexadécimal, chaque octets est séparé par deux points.  
##### exemple: 2a01:cb08:a37b:ac00:70eb:c4b7:56de:f7a9  
Avec 64 bits le nombre total d'adresses IP pouvant exister est de **18 446 744 073 709 551 616**

## IPv4
Les réseaux IPv4 sont catégorisées par ==classes== pour savoir qui communique avec qui.  
Cela permet de créer des réseaux de tailles différentes  
Chaque classes a un usage particulier  

### Adresses publiques  
Les adresses publiques sont dites **routable**, on peut les utiliser pour communiquer sur internet, attribués au routeurs.  

| Classe | Début | Fin | CIDR | Masque | 
| :- | :- | :- | :- | :- |
| Classe A | 0.0.0.0 | 126.255.255.255 | /8 | 255.0.0.0 |
| Classe B | 128.0.0.0 | 191.255.255.255 | /16 | 255.255.0.0 | 
| Classe C | 192.0.0.0 | 223.255.255.255 | /24 | 255.255.255.0 |

### Adresses privées  
Les addresses privées sont dites **non-routable** elle sont reservées à un usage en local.  

| Début | Fin | CIDR | Masque |
|:- | :- | :- | :- |
| 10.0.0.0 | 10.255.255.255 | /8 | 255.0.0.0 |
| 172.16.0.0 | 172.31.255.255 | /12 | 255.255.0.0
| 192.168.0.0 | 192.168.255.255 | /16 | 255.255.255.0

### Masques de sous réseau  
Sépare la partie réseau et la partie hôte  
==Hôte== : la partie réservée à l’attribution des machines  
==Réseau== : pour communiquer sur internet  
CIDR: notation lié au masque = nombre de bits du masque  
```title="Exemple"
Adresse réseau :            192.168.1.0  
Masque de sous réseau :     255.255.255.0

              Réseau                    Hôte
┌─────────────────────────────┬───────────┐
│ 1100 0000 . 1010 1000 . 0000 0001 │ 0000 0000   │ 
│ 1111 1111 . 1111 1111 . 1111 1111 │ 0000 0000   │ 
└─────────────────────────────┴───────────┘ 
```
!!! info "Adresses privées spéciales"
    ==127.0.0.0== est l'adresse de loopback, c'est à dire soit même  
    ==169.254.0.0/24== est le réseau APIPA, reservé aux clients n'ayant pas obtenu une adresse IP de la part du serveur DHCP

### Sous-réseaux
On peut augmenter la taille du masque pour diviser notre réseau en **sous-réseaux**.  
Lorsque qu'on divise un réseau en sous-réseaux, les sous-réseaux ne peuvent pas communiquer entre eux.  
Utile lorsque l'on veut faire des pôles en entreprise, on divise les services comptabilité, techniciens, administrateurs...  
##### Exemple
On a l'adresse ip publique suivante : ==80.60.10.64/26==  
Le masque nous est imposé par le fournisseur d’IP, on ne peut pas le réduire (/25 /24) pour avoir + d’IPs mais on peut l’augmenter (/27, /28).  
Avec un masque en /26 on a 64-2 hôtes disponibles  
??? note "Calcul"
    32 bits dans une IPv4 donc 2³²  
    CIDR de 26 donc 2³²⁻²⁶ = 2⁶ = 64  
!!! info "On enlève toujours 2 au nombre d'hôtes disponible car on va inclure le ==broadcast== (dernière IP) et le ==réseau== (première IP)"  

C'est en augmentant le masque que l'on fait des ==sous-réseaux==.    
On veut 3 sous-réseaux pour séparer l’admin, compta et RH  
Chaque service contient 10 postes  
On a alors en local :  

| Réseau n° | IP | Nombre d'hôtes |
| :-: | :- | :- |
| 1 | 192.168.1.0/28 | 10p en admin |
| 2 | 192.168.1.16/28 | 10p en compta |
| 3 | 192.168.1.32/28 | 10p en RH |

??? note "Calcul"
    ```
              128  64  32  16 | 8 4 2 1
    Réseau 1    0   0   0   0 | .. .. .. ..
    Réseau 2    0   0   0   1 | .. .. .. ..
    Réseau 3    0   0   1   0 | .. .. .. ..
    ```
    Pour faire nos sous-réseau on prends notre nombre de postes et on sépare la partie hôte du sous réseau avec le nombre le plus près de celui ci  
    Le trait sur le tableau représente cette séparation.  
    On a besoin de 10 postes, avec du binaire le nombre le plus près de 10 est 16 (8 est trop petit)  
    !!! warning "Il faut inclure l'IP le broadcast et du réseau, si on veut caser 15 postes, on ne peut pas choisir le réseau 16 car on a besoin de 17 IPs au total"  

    A partir de là on va incrémenter de 1 en binaire sur la partie réseau pour séparer nos sous-réseaux.  
    On a alors en premier 192.168.1.0, en deuxieme 192.168.1.16...  
    
    Si on veut faire + de sous-réseaux on continue avec le 64 et le 128  
    après ces 13 ip on rentre dans un autre réseau  

	Si on nous demande pas un nombre de postes mais de sous-réseaux on fait 2^n >= nombre de sous réseaux pour déterminer le nombre à ajouter au CIDR.  
	Par exemple: diviser 192.168.1.0/24 en ==5 sous-réseaux==.  
	2¹ = 1, 2² = 4, 2³ = 8.  
	2³ >= 5 donc on doit ajouter ==3 au CIDR==.  
	Ce qui nous fait /27.

### Sous-réseau : agrandissement
On fait l'inverse : on a pas assez d'hosts dans notre réseau et on souhaite l'élargir.  
On peut diminuer la taille du masque pour augmenter la taille de notre réseau.  
##### Exemple
On a le réseau privé suivant : ==192.168.200.0/24==  
Ce réseau peut accueilir **256** hosts, or on veut **1100** hosts.  
Le premier masque qui nous offre assez d'hosts est ==/21==, 2048 adresses donc 2046 hosts.  
Notre nouveau réseau ==192.168.200.0/21==.  
Première ip : 192.168.200.1  
Dernière ip : 192.168.207.254  

??? "Calcul de la dernière IP"
    Pour calculer la dernière IP on va déterminer l'IP du prochain réseau, c'est plus facile.  
    Il faut déjà décomposer notre masque en décimal.  
    /21 = 255.255.248.0  
    On prend le dernier chiffre qui n'est pas 0 ou 255, donc 248  
    On soustrait 256: 256 - 248 = ==8==  
    On ajoute ce 8 dans l'IP à l'endroit où se situait notre 248 :  
    ==192.168.208.0/21==  
    Pour obtenir la dernière IP de notre précédent réseau on recule de 2 IP:  
    ==192.168.207.254==  
    ==192.168.207.255== étant l'adresse de broadcast.

