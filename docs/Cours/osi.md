# Modèle OSI
pour **Open System Interconnection**, est une norme de communication, devenue standard par l’ISO (l'organisme international de normalisation)  

Ce modèle permet de représenter comment les systèmes réseau communiquent et envoient des données d'un expéditeur à un destinataire sous formes de couches.  

![osi](../src/osi.png){ width="500" }  

Chaques couches est dépendante de la précédente (d'un niveau plus bas).  
Lorsqu'on progresse vers une couche supérieure, on appelle ça ==la décapsulation==.  
Lorsqu'on progresse vers une couche inferieure, on appelle ça ==l'encapsulation==.  

## Description des couches
### 1. Physique  
composé des éléments physique permettant de connecter des équipements  
:arrow_right: paires torsadés : prise "RJ45" qui se branche sur un port "Ethernet"  
:arrow_right: fibre optique : tube qui transporte la lumière  
:arrow_right: sans-fil : ondes electromagnétiques (radio) interprétés par le protocole "WiFi"  
### 2. Liaison  
se situe le protocole ==MAC== (Media Access Control), les adresses MAC, appelées adresses physiques, permet entre autres d'établir une première connection pour demander une addresse IP  
Les données transmises sur la couche 2 s'appellent des ==trames==.  
:arrow_right: exemple d’adresse mac: e0:d5:5e:3d:71:12  
:arrow_right: on y trouve les équipement de type switchs  
### 3. Réseau
fournit un adressage et un transport inter-réseaux  
se situe le protocole ==IP== (ipv4, ipv6)  
:arrow_right: exemple d'adresse IP : 172.16.254.1  
:arrow_right: composé de 4 octets (32bits): 10101100 - 00010000 - 11111110 - 00000001  
:arrow_right: on y trouve les équipement de type routeurs, qui servent à relayer les messages entre différents réseaux pour les faire communiquer  
Les données transmises sur la couche 3 s'appellent des ==paquets==.
### 4. Transport
elle va assurer le transport des données sur un réseau et gérer les erreurs (TCP)  
se situe les protocoles TCP et UDP  
:arrow_right: TCP: pour **Transmission Control Protocol**, protocole qui sert à vérifier que les paquets sont bien arrivés à destination.  
:arrow_right: UDP: **User Datagram Protocol**, il est plus simple que le TCP, il ne s’assure PAS de la bonne réception. Plus rapide, il est utilisé par exemple pour la VoIP.
On y trouve les ==ports==, qui permettent de faire communiquer plusieurs clients avec un même serveur.  
Les données transmises sur la couche 4 s'appellent des ==segments==.
??? note "Le protocole VoIP"  
    Le protocole VoIP ou Voix sur IP permet de transmettre la voix sur des réseaux compatibles IP. Ce protocole sert donc à téléphoner grâce à internet mais pas que. On peut desormais transmettre des médias avec ce protocole ce qui permet l'utilisation des applications de messagerie instantanés.  
### 5. Session
Mécanisme de connexion, elle établit des sessions entre les systèmes et les ferme.  
On peut les voir comme des portes donnant accès à differents logiciels d'un système d'exploitation.  
??? note "Définition d'un système d'exploitation"
    Pour faire simple, un système d'exploitation (ou OS) est un ensemble de logiciels permettant de contrôler un appareil informatique.  
    On y trouve le ==noyau== (kernel) qui permet d'exploiter le materiel pour faire fonctionner les logiciels, d'où le terme "système d'exploitation"  
      
    ![os](../src/os.png){ width="300" }

### 6. Présentation  
Pour l'encodage/décodage, elle convertit par exemple du texte (ASCII) en format binaire compréhensible par la machine et vise versa.  
### 7. Application
Représente l’application finale avec laquelle l’utilisateur interagis comme un navigateur web, une application de messagerie, un jeu vidéo etc...  

Les données transmises sur la couche 5, 6 et 7 s'appellent des ==données==.
