# Mise à jour d'Atmosphère

!!! warning "Mise en garde"  
    Avant de faire ça, vérifier si la dernière version d'Atmosphère supporte la dernière version de la switch.
    Pour ça, se rendre sur le [github d'Atmosphère](https://github.com/Atmosphere-NX/Atmosphere/releases/) et lire la ligne "Support was added for ...", avec la version de la switch.  
    Si la version supportée par Atmosphère est **inférieure** à la version qui va être mis à jour, NE PAS FAIRE DE MISE À JOUR !  
!!! info "Les étapes marquées d'un "☠️" concernent le contenu pirate, elles sont facultatives"  

## Installer la dernière mise à jour de la Switch

![nintendo-maj](../../src/nintendo-update.jpg)  

Après ça, mettez la switch en mode RCM  

## Télécharger de la dernière version d'Atmosphère

```console
$ curl -s https://api.github.com/repos/Atmosphere-NX/Atmosphere/releases/latest \
| grep "browser_download_url.*" \
| cut -d : -f 2,3 \
| tr -d \" \
| wget -qi -
```

```console
mkdir Atmosphere
unzip atmosphere*.zip -d Atmosphere
```

## Télécharger la dernière version d'Hekate

```console
$ curl -s https://api.github.com/repos/CTCaer/hekate/releases/latest \
| grep "browser_download_url.*hekate_ctcaer" \
| cut -d : -f 2,3 \
| tr -d \" \
| wget -qi -
```

```console
mkdir Hekate
unzip hekate*.zip -d Hekate
```

## Télécharger un injecteur de payload

```console
$ curl -s https://api.github.com/repos/nh-server/fusee-interfacee-tk/releases/latest \
| grep "browser_download_url.*linux.zip" \
| cut -d : -f 2,3 \
| tr -d \" \
| wget -qi -
```

```console
mkdir fusee-interface-tk
unzip fusee-interfacee-tk-linux.zip -d fusee-interface-tk
```

## Télécharger le payload

```console
$ curl -s https://api.github.com/repos/Atmosphere-NX/Atmosphere/releases/latest \
| grep "browser_download_url.*.bin" \
| cut -d : -f 2,3 \
| tr -d \" \
| wget -qi -
```

## Télécharger l'outil de mise à jour des Sigpatches ☠️

Seulement si l'on souhaite lancer autre choses comme des homebrews

```console
$ curl -s https://api.github.com/repos/ITotalJustice/sigpatch-updater/releases/latest \
| grep "browser_download_url.*nro" \
| cut -d : -f 2,3 \
| tr -d \" \
| wget -qi -
```

à cette étape, on devrait avoir l'arborescence suivante  
![switch-tree](../../src/switch-tree.png)

## Booter sur Hekate

- Brancher la switch au PC  
- Lancer fusee-interface-tk  

```console
$ cd "fusee-interface-tk/fusee-interfacee-tk-linux/App For Linux"
$ sudo ./PayloadInjector
```  

- Cliquer sur "Select Payload"  
- Selectionner Hekate/hekate_ctcaer_<version>.bin
- Cliquer sur "Send Payload!"
!!! warning "Si le logiciel est bloqué sur 'Looking for Device...', ça veut dire que la switch n'est pas en mode RCM. Si on a AutoRCM, il suffit d'éteindre la switch et de la rallumer"

## Copier les nouveaux fichiers d'Atmosphère

- Dans l'interface de Hekate, aller dans Tools > USB Tools > SD Card  
- Sur l'ordinateur, copier le **contenu** du dossier Atmosphère (atmosphere, hbmenu.nro et switch) à la **racine** de la carte SD de la switch  
- Remplacer les fichiers comme demandé  
- Copier sigpatch-updater.nro dans le dossier **switch** de la carte SD de la switch ☠️  
- Copier fusee.bin dans le dossier **bootloader/payloads** de la carte SD de la switch
- **Éjecter la switch depuis l'explorateur de fichier de l'ordinateur** (important)
- Démarrer sur le payload avec le bouton **Payloads** dans le menu principal de Hekate

!!! info "Si Atmosphère n'arrive pas à démarrer, c'est sans doute à cause d'un Homebrew pas à jour, il faudra donc déplacer le contenu du dossier switch **(sauf daybreak, haze et reboot_to_payload)** dans un répertoire appart afin de permettre à Atmosphere de démarrer"

## Patcher Atmosphere pour autoriser les contenus téléchargés ☠️

- Booter sur Atmosphere
- Lancer hbmenu (Album)
- Lancer Sigpatch-Updater
- Sélectionner "Update this app" (une connexion internet est nécessaire)
- Sélectionner "Update Sigpatches"
- Rebooter Atmosphere

