<figure markdown>
![switch-logo](https://blog.chipnmodz.fr/wp-content/uploads/2012/12/logo-nintendo-switch.png){ width=500 }  
</figure>
# Jailbreak d'une Switch

Le jailbreak de la Switch sert essentiellement au lancement d'**homebrews**, c'est-à-dire des logiciels développés par une communauté indépendante de l'éditeur.  
Ces homebrews permettent entre autre d'overclocker la switch, utiliser des codes de triches, patcher les jeux pour atteindre le 60fps, afficher des informations sur la température, fréquence des puces, avoir un mode nuit etc...  
Il est aussi possible de lancer des jeux téléchargés illégalement grâce à Tinfoil, un eShop pirate.  

## Prérequis

- Une Switch vulnérable  
- Un lecteur de carte SD  
- Un PC, si possible sous Linux  
- Un cable USB-C  
- Un trombone ou [un jig à 8€](https://www.amazon.fr/RCM-pour-Nintendo-Switch-Court-Circuit/dp/B0BV27FTTF/){ target=about:blank_ }  

## Résumé

- Vérification de la compatibilité
- Préparer la carte SD
- Booter en mode recovery
- Injecter un payload
- Activer AutoRCM
- Booter sur Atmosphère

## Vérification de la compatibilité au jailbreak d'une Switch

!!! info "Il est préférable de faire cette vérification **AVANT** l'achat d'une Switch."  
Pas toutes les switch sont "jailbreakable".  
Le jailbreak se base sur une faille matérielle de la puce TegraX1 de Nvidia.  
Cette puce a été patchée sur les dernières versions des Switch, il est donc nécessaire de vérifier la compatibilité de cette faille sur la Switch.  

- [Se rendre sur le lien suivant](https://ismyswitchpatched.com/){ target=about:blank_ }.  
- Entrer son numéro de série (présent en dessous de la console).  
- Si la Switch est jailbreakable, le message suivant s'affiche  

![switch-ispatched](../../src/switch-ispatched.png){ width=500 }  

Inutile de chercher un moyen de jailbreaker une switch qui n'est pas compatible, il faut la revendre et en chercher une autre.

## Préparer sa carte SD

- Éteindre la Switch
- Retirer la carte SD
- Brancher la carte SD au PC
- Télécharger l'archive suivante : [lien]( https://www.sdsetup.com/console?switch#atmosphere;atmos_musthave;hekate_musthave;appstorenx;fusee-primary;hekate;){ target=about:blank_ }
- Décompresser l'archive
- Placer le ==contenu== du dossier "sd" à la ==racine== de la carte SD
- Ne pas supprimer l'archive de l'ordinateur (on aura besoin du contenu du dossier "payloads")
  
## Boot en mode RCM

Il s'agit de l'étape la plus difficile.  
Le mode RCM pour **R**e**c**overy **M**ode va servir à injecter ce qu'on appelle un "Payload": un petit logiciel exploitant une vulnérabilité.  
Pour booter en mode RCM, il faut **connecter un pin terre avec le pin RCM de l'emplacement de la joycon droite** et ensuite appuyer sur les boutons **Volume UP** et **Power**  

Si la Switch démarre normalement, la manipulation n'a pas fonctionné.
Si la Switch ne démarre pas, que l'écran reste noir et que rien ne s'est affiché à l'écran, elle a démarré en mode RCM.  

Les pins 1, 2 et 7 sont des terres et le pin 10 est le pin RCM.  

![switch-pin](../../src/switch-pin.png){ width=600 }  

2 méthodes pour faire ça :  

- Utiliser un trombone  
- Acheter un "Jig", un objet imprimé en 3D avec les bonnes mesures

??? info "Méthode du trombone"
    1 - Éteindre la Switch  
    2 - Ecarter les deux bords du trombone de **==6mm==** (marge d'erreur : 0,5mm)  

    ![switch-pin2](../src/switch-pin2.png){ width=600 }  
    3 - Connecter le trombonne  
    ![switch-pin3](../src/switch-pin3.png){ width=600 }  
    4 - Maintenir le bouton **Volume Up** et **Power**  

    Si la Switch démarre normalement, la manipulation n'a pas fonctionné.    
    Si la Switch ne démarre pas, que l'écran reste noir et que rien ne s'est affiché à l'écran, elle a démarré en mode RCM.  

## Injecter le Payload

Étant maintenant en mode RCM, la priorité est de faire en sorte de ne plus avoir besoin de connecter les pins de la joycon pour pouvoir rentrer en mode RCM.  
Pour ça, on va utiliser **Hekate**, un bootloader pour Switch qui a une fonctionnalité appelée **AutoRCM** qui permet d'automatiquement booter sur le mode RCM quand la switch démarre.  

!!! info "Point négatif : si la switch n'a plus de batterie ou qu'elle est juste éteinte, on aura besoin d'un PC pour la démarrer, il n'y aura aucune méthode de démarrer sa switch que d'injecter un payload"

Instructions pour booter Hekate :  
??? info "Sur Windows"  
??? info "Sur Linux"
    - Télécharger [fusee-interface-tk](https://github.com/nh-server/fusee-interfacee-tk/releases/), une application permetant d'injecter des payloads  
    - Executer PayloadInjector avec **sudo**
    - Cliquer sur "Select Payload"  
    - Choisir le fichier "hekate_ctcaer_X.X.X.bin" présent dans le dossier "payloads" de l'archive téléchargée au début du guide dans [Préparer sa carte SD](#preparer-sa-carte-sd)  
    - Cliquer sur "Send Payload!"  
    !!! warning "Si le logiciel est bloqué sur 'Looking for Device...', ça veut dire que la switch n'est pas en mode RCM."

Arrivé sur l'interface d'Hekate, installer le mode **AutoRCM**  

![switch-rcm](../src/switch-rcm.png){ width=600 }  

La Switch démarrera dorénavant automatiquement sur le RCM  
==Attention à ne pas confondre ça avec un dysfonctionnement==.  

## Booter sur Atmosphère

![atmosphere-logo](https://github.com/Atmosphere-NX/Atmosphere/raw/master/img/banner.png)  
Atmosphère est le custom firmware le plus populaire sur Switch, il permet le lancement d'homebrews et d'installer des plugins.  
La dernière version d'Atmosphere a été installé sur la carte SD lors de l'étape [Préparation sa carte SD](#préparer-sa-carte-sd).  
Dans l'interface de Hekate, sélectionner **"Payloads"** puis "**fusee.bin**" pour le lancer.  
Pour vérifier si l'on est bien sur Atmosphere, il suffit de lancer **hbmenu**.  
HBMenu est un plugin permettant de faire apparaitre un menu donnant sur les homebrews.  
Ce plugin se lance en sélectionnant l'application **Album** de la Switch.  
Pour afficher l'application Album, il suffit de maintenir le bouton R et de la lancer.  
