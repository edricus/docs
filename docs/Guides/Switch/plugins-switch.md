# Plugins et scripts Switch

Cette repertorie les plugins interessants ainsi que des scripts pour Linux pour télécharger les dernières versions de certains logiciels.  

#### Vrac
Site pour télécharger les firmwares officiels : [Darthsternie's Firmware Archive](https://darthsternie.net/switch-firmwares/)  
Github d'Atmosphère : [Atmosphere-NX/Atmosphere](https://github.com/Atmosphere-NX/Atmosphere/releases)  
Github d'Hekate : [CTCaer/hekate](https://github.com/CTCaer/hekate/releases)  
