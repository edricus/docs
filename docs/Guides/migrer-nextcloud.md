# Migrer Nextcloud vers Docker
Le but est de migrer son instance instalée en local vers la version docker de Nextcloud.  

## Environnement

- Dossier nextcloud : /var/www/nextcloud  
- Dossier data : /var/www/nextcloud/data  
- Base de donnée : MariaDB  
  Utilisateur : nextcloud  
- Tous les mots de passe : P@ssword2024  
- Version de Nextcloud: 28  
- URL de Nextcloud : nextcloud.example.com
  Utilisateur : nextcloud

## Préparer Nextcloud

### Mode maintenance
Le mode maintenance va "figer" l'instance, elle sera comme arrêtée mais toujours joignable.  
```console
# sudo -u www-data php /var/www/nextcloud/occ maintenance:mode --on
```
### Noter la version
```console
# sudo -u www-data php /var/www/nextcloud/occ status
```
```
version: 28.0.6.1
```
On arrondira à 28.  

### Backup de la BDD
```console
$ mysqldump -u nextcloud -pP@ssword2024 nextcloud > nextcloud.sql
```

### Backup des données utilisateur
Si les données ne sont pas trop volumineuses, faire un backup
```console
$ rsync -ah --info=progress2 /var/www/nextcloud/data nextcloud-data
```
Sinon faire un mv.  

## Docker-compose
Maintenant on va préparer Nextcloud côté Docker.  
  
Ici un docker-compose pour une installation automatique.  
!!! info "Rappel : changer 'P@ssword2024' par les vrais mots de passe et 'nextcloud.example.com' par le nom de domaine"
```yaml
---
services:
  nextcloud:
    image: nextcloud:28-apache
    container_name: nextcloud
    environment:
      MYSQL_DATABASE: "nextcloud"
      MYSQL_USER: "nextcloud"
      MYSQL_PASSWORD: "P@ssword2024"
      MYSQL_HOST: "db"
      NEXTCLOUD_ADMIN_USER: "nextcloud"
      NEXTCLOUD_ADMIN_PASSWORD: "P@ssword2024"
      NEXTCLOUD_TRUSTED_DOMAINS: "nextcloud.example.com"
      REDIS_HOST: "redis"
      OVERWRITEPROTOCOL: "https"
      PHP_UPLOAD_LIMIT: "100G"
    volumes:
      - ./nextcloud-data:/data
      - ./nextcloud-config:/var/www/html
    restart: unless-stopped
  
  db:
    image: mariadb:latest
    container_name: mariadb
    command: --transaction-isolation=READ-COMMITTED --log-bin=binlog --binlog-format=ROW
    environment:
      MYSQL_DATABASE: "nextcloud"
      MYSQL_USER: "nextcloud"
      MYSQL_PASSWORD: "P@ssword2024"
      MYSQL_RANDOM_ROOT_PASSWORD: "true"
    volumes:
      - ./nextcloud.sql:/nextcloud.sql
      - ./mariadb-data:/var/lib/mysql
    restart: unless-stopped
  
  redis:
    image: redis:latest
    container_name: redis
    volumes:
      - ./redis-data:/data
```

## Restaurer le backup
L'installation complétée on a un utilisateur avec les fichiers dans le conteneur ( /data )  
mais pas connectée à l'instance qui a pris celui par défaut ( /var/www/html/data ).  
Il faut aussi restaurer la BDD.  

### Restaurer la BDD
On va d'abord démarrer le service de BDD pour avoir la BDD créer et vide.  
```console
$ docker-compose up db -d
```
On s'y connecte  
```console
$ docker exec -it mariadb bash
```
On restaure
```console
$ mariadb -u nextcloud -pP@ssword2024 nextcloud < nextcloud.sql
```
Et on fini de deployer l'application
```console
$ docker-compose up -d
```

### Changer le répertoire data
Après l'installation complète il faut modifier le répertoire data.  
Dans le cas où on l'aurai fait avant l'installation, elle n'aurait pas pu se faire car Nextcloud ne voudra pas créer un utilisateur avec des fichiers déjà existants.  
```console title="nextcloud-config/config/config.php"
[ ... ]
'datadirectory' => '/data',
[ ... ]
```
Redémarrer l'instance
```console
$ docker-compose restart
```
Déclancher un scan
```console
$ docker exec -u 33 -it nextcloud php occ files:scan --all
```

## Basculer l'instance
Ne pas oublier de désactiver apache2
```console
# systemctl disable --now apache2
```
