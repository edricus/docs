# Reverse-proxy avec SSL
Nginx en reverse proxy vers un site hébérgé sous apache avec un certificat letsencrypt généré par certbot  
##### Logiciels utilisés  

- NGINX  
- Apache  
- Certbot  
- un nom de domaine  

##### Exemple utilisé  
**Site hebergé** : Nextcloud  
  - Url sur internet : https://domaine-exemple.org/nextcloud  
**Ports** :  
  - Apache : 9001  
  - Nginx : 80,443  
**Fichiers** :  
  - Site : `/var/www/nextcloud/`  
  - Apache : `/etc/apache2/sites-availables/nextcloud.conf`  
  - Nginx : `/etc/nginx/sites-availables/reverse-proxy`  
  - Certificat : `/etc/letsencrypt/live/domaine-exemple.org/fullchain.pem`  
  - Clé certificat : `/etc/letsencrypt/live/domaine-exemple.org/privkey.pem`
## Génerer un certificat
Certificat LetsEncrypt avec certbot
!!! info "Remplacer 'domaine-exemple.org' par le nom de domaine voulu"
```console
# apt install python3-certbot
# certbot certonly --standalone -d domaine-exemple.org
```
Les fichiers sont maintenant disponibles dans `/etc/letsencrypt/live/domaine-exemple.org/`
## Configuration Apache
Apache écoutera sur le port **9001**  
```apache title="/etc/apache2/ports.conf"
<IfModule ssl_module>
  Listen 9001
</IfModule>
<IfModule mod_gnutls>
  Listen 9001
</IfModule>
```
```apache title="/etc/apache2/sites-available/nextcloud.conf"
alias '/nextcloud' /var/www/nextcloud
<VirtualHost *:9001>
  DocumentRoot /var/www/nextcloud
  ErrorLog /var/log/nextcloud_error.log
  CustomLog /var/log/nextcloud_access.log combined
  <Directory /var/www/nextcloud>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    </IfModule>
    <IfModule mod_headers.c>
      Header always set Strict-Transport-Security "max-age=15552000; includeSubDomains; preload"
    </IfModule>
  </Directory>
  SSLEngine on
  SSLCertificateFile /etc/letsencrypt/live/domaine-exemple.org/fullchain.pem
  SSLCertificateKeyFile /etc/letsencrypt/live/domaine-exemple.org/privkey.pem
</VirtualHost> 
```
Activer le site
```console
# a2ensite nextcloud.conf
```
Démarrer Apache
```
# systemctl restart apache2
# systemctl --enable apache2
```
??? note "Debug"
    ```console 
    # journalctl -u apache2
    ```
    Ctrl+G pour aller à la fin du fichier

## Configuration Nginx
Nginx écoutera sur le **80** et **443**  
Nginx renverra les requêtes **http** vers **https**
```nginx title="/etc/nginx/sites-available/reverse-proxy"
server {
  server_name domaine-exemple.org;
  listen 443 ssl;
  location /nextcloud {
    proxy_pass https://127.0.0.1:9001;
    proxy_set_header Host $host;
  }
  ssl_certificate /etc/letsencrypt/live/domaine-exemple.org/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/domaine-exemple.org/privkey.pem;
  include /etc/letsencrypt/options-ssl-nginx.conf; 
  ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; 
}

server {
  server_name edricloud.org;
  listen 80;
  location /nextcloud {
    return 301 https://127.0.0.1:9001;
  }
}
```
Activer le site
```console
# ln -s /etc/nginx/sites-available/reverse-proxy /etc/nginx/sites-enabled/
```
Démarrer Nginx
```
# systemctl restart nginx
# systemctl --enable nginx
```
??? note "Debug"
    ```console 
    # journalctl -u nginx
    ```
    Ctrl+G pour aller à la fin du fichier

