# Activer l'hibernation sous Linux
L'hibernation sous Linux n'est pas activée par défaut.  
Alors que Microsoft a largement popularisé l'hibernation, la raison pour laquelle elle n'est pas disponible par defaut sur linux est [l'incompatibilité avec le Secure Boot](https://wiki.debian.org/SecureBoot#Secure_Boot_limitations){ target=about:_blank }, activé par défaut sur la plupart des machines.  
Une autre explication est que l'utilisation principale de Linux demeure encore les serveurs; l'utilisation de Linux en tant que "daily driver" est récente, surtout sur Debian.  

## Prérequis

 - Une partition swap de la taille de la moitié de la RAM, idéalement de la taille de la RAM  
 - Secure Boot désactivé

## Paramètrage de initramfs
On va éditer le fichier `/etc/initramfs-tools/conf.d/resume` en indiquant la partition swap.  

 - Déterminer l'UUID de la partition swap  
 ```console
 # fdisk -l
 ```
 ```
 Device             Start       End   Sectors   Size Type
 /dev/nvme0n1p1      2048    976895    974848   476M EFI System
 /dev/nvme0n1p2    976896   9365503   8388608     4G Linux swap
 /dev/nvme0n1p3   9365504  67958783  58593280  27.9G Linux filesystem
 /dev/nvme0n1p4  67958784 767358975 699400192 333.5G Linux filesystem
 /dev/nvme0n1p5 767358976 976773119 209414144  99.9G Microsoft basic data
 ```
 La partition "/dev/nvme0n1p2" est de type "Linux swap"  
 ```console
 # blkid | grep nvme0n1p2
 ```
 ```
 /dev/nvme0n1p2: UUID="0d39b5fa-5493-43ae-b9e3-d3ef18da1ad9" TYPE="swap" PARTLABEL="linux-swap" PARTUUID="6c35837f-8e74-466d-9d51-f70a83a5a4ff"
 ```
 On copie l'UUID "0d39b5fa-5493-43ae-b9e3-d3ef18da1ad9"  
 
 - On modifie le fichier 
 ```title="/etc/initramfs-tools/conf.d/resume"
 RESUME="UUID=0d39b5fa-5493-43ae-b9e3-d3ef18da1ad9"
 ```

 - On met à jour l'initramfs
 ```console
 # update-initramfs -u
 ```

## Modifier les paramètres d'alimentation pour hiberner au lieu d'éteindre/mettre en veille

 - Pour GNOME, rajouter un bouton d'alimentation  
 [Télécharger "Hibernate Status Button" sur GNOME Extensions](https://extensions.gnome.org/extension/755/hibernate-status-button/){ target=about:_blank }  
 ![hibernate-gnome](../src/hibernate-gnome.png){ width=300 }  

 - Changer le comportement du bouton d'alimentation  
 ![hibernate-gnome-2](../src/hibernate-gnome-2.png){ width=700 }
