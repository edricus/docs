# Installer un serveur Minecraft sur Debian 12
Ce guide a pour but l'installation et la configuration d'un serveur minecraft.  
Le serveur se basera sur minecraft ==1.20.1== sur ==Fabric==.  

## Prérequis 

- Debian 12 sans interface de préférence  
- 4Go de RAM  
- Les paquets suivants :  
```console
# apt-get install curl nano sudo screen cron rsync
```

## SSH

- On configure déjà l'accès à distance.  

```console
# apt-get install ssh
```
On peut laisser la config par défaut, sinon voir [la page de guide ssh](../Réseau/ssh.md){ target=about:_blank } pour plus d'infos.  

- On le démarre et l'ajoute au démarrage :  
```console
# systemctl enable --now ssh
```

## Création d'un utilisateur

Une bonne pratique est de faire un utilisateur par service.  

- On crée un utilisateur minecraft  
```console
# useradd -m /home/minecraft -s /bin/bash minecraft
# passwd minecraft
< choisir un mot de passe fort >
```
??? info "Détails pour la commande useradd"
    -m : le "home" de l'utilisateur, son dossier personnel  
    -s : le shell qui sera utilisé, on peut aussi empêcher la connexion à l'utilisateur en métant "/bin/nologin" 

## Installation de Java
En fonction des versions de minecraft, la version de java change.  
Celle utilisée par la version 1.20.1 de Minecraft est ==17==.  
Il faut aussi noter que toutes les versions de java ne sont pas disponibles sur Debian 12, seulement la 11 et la 17, il faut le prendre en compte dans le choix de la version de Minecraft.  

```console
# apt-get install openjdk-17-jre-headless
```

## Récupération du .jar du serveur Fabric

- On va se connecter à l'utilisateur
```console
$ su - minecraft
```

- Et télécharger le .jar avec curl
```console
$ curl -OJ https://meta.fabricmc.net/v2/versions/loader/1.20.1/0.16.3/1.0.1/server/jar
```
!!! question "Pour info: cette commande est trouvable ici : [https://fabricmc.net/use/server/](https://fabricmc.net/use/server/){ target=about:_blank }"

Le serveur a besoin d'un ==server.properties== pour démarrer.  

- On va créer un fichier vide  
```console
$ touch server.properties
```

Ensuite, il faut accepter les conditions d'utilisation.  

- On va créer un fichier ==eula.txt== avec le contenu ==eula=true== :  
```console
$ echo 'eula=true' > eula.txt
```

- On va ensuite faire un démarrage initial pour voir si tout fonctionne
```console
$ java -Xmx4G -jar fabric-server-mc.1.20.1-loader.0.16.3-launcher.1.0.1.jar nogui
```
!!! check "Si le serveur indique `[Server thread/INFO]: Done (34.117s)! For help, type "help"` c'est que tout est bon"
!!! warning "Sinon, regarder les lignes `[main/ERROR]` et `[main/WARN]`"
Pour configurer le serveur, modifier le fichier ==server.properties==, qui a une configuration par défaut après le premier démarrage.  

## Configuration de screen
Le problème avec l'état actuel du serveur c'est qu'il ne s'exécute pas en arrière-plan.  
Un autre problème est que l'exécution en arrière-plan nous fait perdre l'interaction avec notre serveur (voir les logs, envoyer des commandes).  
La solution est l'application ==screen== : un multiplexeur de terminal.  
On va simplement faire tourner un terminal en arrière-plan, le meilleur des deux mondes.  

Screen est "normalement" installé (voir: [#Prérequis](#prerequis))  

- On lance notre serveur, mais cette fois si dans un terminal screen
```console
$ screen -d -m -S minecraft-1.20.1 java -Xmx4G -jar fabric-server-mc.1.20.1-loader.0.16.3-launcher.1.0.1.jar nogui
```

??? note "Détail de la commande"
    -d -m : lancer screen détaché, en arrière-plan  
    -S : le nom de la session (ici, minecraft-1.20.1)

- Une fois le serveur lancé, on peut lister les sessions screen : 
```console
$ screen -ls
```
```
There is a screen on:
 28620.minecraft-1.20.1 (08/31/2024 03:40:17 PM) (Detached)
1 Socket in /run/screen/S-minecraft.
```

- On s'y connecte :  
```console
$ screen -r minecraft-1.20.1
```

À partir d'ici, il faut connaitre les raccourcis clavier de base dans screen pour naviguer et quitter notre terminal sans toucher à la commande à l'intérieur :  

| Action | Commande |
| :- | :- |
| Quitter la session | Ctrl+A puis d |
| Défiler dans la session | Ctrl+A puis flèches, Échap pour quitter ce mode |  
| Tuer la session | Ctrl+C, puis "exit" |

## Service systemd
Maintenant, on va configurer un service pour démarrer/arrêter/activer notre serveur au démarrage.  
Déjà, il faut quitter l'utilisateur minecraft, qui n'a pas les droits pour créer un service :  
```console
$ exit
```
Ensuite, on fait un fichier ==.service== dans le répertoire suivant avec ce contenu :  
```title="/etc/systemd/system/minecraft.service"
[Unit]
Description=Minecraft service
Wants=network-online.target
After=network-online.target

[Service]
User=minecraft
RemainAfterExit=yes
WorkingDirectory=/home/minecraft/covier120
ExecStart=screen -d -m -S minecraft-1.20 java -Xmx8G -jar fabric-server-mc.1.20.jar nogui
Restart=always
RestartSec=10
NoNewPrivileges=yes
[Install]
WantedBy=multi-user.target
```
Pour les détails d'un service systemd, voir [cette page de doc](../Système/systemd.md){ target=about:_blank }

- On va actualiser la liste des services :  
```console
# systemctl daemon-reload
```

- On va démarrer notre serveur :  
```console
# systemctl start minecraft
```

- On va démarrer notre serveur au démarrage de l'ordinateur :  
```console
# systemctl enable minecraft
```

!!! warning "Pour utiliser screen, il faudra se connecter avec l'utilisateur minecraft, les sessions screen sont séparées par utilisateurs"

## Installer des mods
Le plus simple est d'utiliser ==Filezilla==, pas besoin de serveur ftp, filezilla peut se connecter en ==sftp== (SSH).  

- Sur un client muni d'une interface graphique, télécharger Filezilla :  
[https://filezilla-project.org/download.php?platform=win64](https://filezilla-project.org/download.php?platform=win64){ target=about:_blank }  

- Sur linux :  
  ```console
  # apt install filezilla
  ```

- Arrivé sur filezilla, on se connecte avec l'utilisateur minecraft :  

![filezilla](../src/filezilla.png){ width=700 }  

- On va créer un dossier mods :  
![filezilla2](../src/filezilla2.png){ width=400 }  

Et on fait des glisser-déposer depuis notre machine cliente vers ce nouveau dossier.  

Ensuite, on redémarre notre serveur :   

- Se connecter avec un compte disposant de sudo  
- `sudo systemctl restart minecraft`

!!! note "Il est préférable de regarder les logs du serveur avec screen ici."

## Configurer un backup
Optionnel, mais très recommandé : on va backup le dossier ==world==.  
On va utiliser pour ça ==cron==, qui est un automatiseur de tâches.  
On va lui donner un script à exécuter périodiquement.  
Pour éviter que ça prenne trop de place, le script va faire supprimer les anciens backups automatiquement.  

- On se connecte à notre utilisateur minecraft
- Créer un dossier pour les backups :  

```console
$ mkdir backup
```

- On crée notre script :  

```title="backup.sh"
#!/bin/bash
rsync -ah world backup/world$(date +%d%m%Y)
find backup/ -mtime +7 -type d -exec rm -rf {} \;
```
La commande rsync copie, la find supprime (tous les 7 jours, avec -mtime)

- On lui donne les bons droits :  

```console
$ chmod u+x backup.sh
```

- On crée la tâche cron :  

```console
$ crontab -e
```
```
0 1 * * * /home/minecraft/backup.sh
```
Ici, on fait un backup tous les jours à 1h du matin

- On test notre script, très important :  

```console
$ ./backup.sh
```


