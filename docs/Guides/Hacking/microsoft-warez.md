# Installation et activation de produits Microsoft
## Les sites
Le site a privilégier pour le téléchargement des logiciels Windows est [massgrave.dev](https://massgrave.dev){ target=about:blank }.  
Il existe aussi la communauté [r/Piracy](https://www.reddit.com/r/Piracy/){ target=about:blank } sur reddit, qui regroupe toutes les information sur le piratage en général.  
Il existe aussi un "megathread" qui regroupe tous les outils et logiciels liés au piratage.  
[https://github.com/Shakil-Shahadat/awesome-piracy](https://github.com/Shakil-Shahadat/awesome-piracy).    

Il faut éviter les sites comme ==ThePirateBay== qui contiennent énormément de virus.  
Il faut aussi éviter les ==recherches directes sur Google==.  
Les premiers liens sont souvent des virus, comme le célèbre KMSPico ([plus d'informations](https://www.malekal.com/kmspico-trojan/){ target=about:blank }.  
Les =="builds"== qui sont des logiciels avec une interface pour installer et activer les produits Microsoft sont aussi à éviter, il est très facile d'y inclure des virus et ils ne sont pas opensource.  

**En général, la meilleure méthode est de télécharger le produit tel quel sur les serveurs Microsoft ou sur massgrave.dev pour ensuite l'activer avec MAS, qui est un script disponible sur GitHub.**

## Outils
#### [Microsoft Activation Scripts (MAS)](https://github.com/massgravel/Microsoft-Activation-Scripts/){ target=about:blank }
Le meilleur activateur, licences Windows 10 et Office, il permet aussi de changer d'édition de Windows.  
Il s'exécute dans un terminal en super-utilisateur.  
En plus de ça, grâce à sa nature de script, il est totalement transparent contrairement aux outils avec une interface.  
Il est possible de l'exécuter directement à partir de powershell
```powershell
irm https://massgrave.dev/get | iex
```
#### [Massgrave.dev](https://massgrave.dev/genuine-installation-media.html) 
Le site de MAS regroupant toutes les versions de Windows et Office.  
Si on ne sait pas quoi prendre, il faut privilégier les options avec un "❤️"  

#### Installation d'Office
Se rendre sur [massgrave.dev](https://massgrave.dev/office_c2r_links.html) et sélectionner la version souhaitée d'Office.  
![office-1.png](../../src/office-1.png)

1. Double-cliquer sur le .exe  
2. Suivre les étapes d'installation  

#### Activation d'Office
On exécute MAS  
```powershell
irm https://massgrave.dev/get | iex
```

Une fois ce script exécuté, on a cette interface  
![office-2.png](../../src/office-2.png){ width=450 }  
Sélectionner ==Ohook== (2) puis ==Install Ohook Office Activation== (1)  
Le script va chercher et activer Office.  

#### Installation de Windows 11
La meilleure version de Windows 11 est ==Windows 11 Business==.  
C'est une version épurée qui ne contient aucune publicité, la télémétrie est aussi réduite au strict minimum.  
Pour télécharger Windows 11 Business, se rendre sur [massgrave.dev](https://massgrave.dev/genuine-installation-media.html){ target=about:blank } et cliquer sur ==Windows 11== puis descendre jusqu'à ==Windows 11 Business <version>==.  
Ensuite, choisir la version avec la langue de notre choix.  

#### Activation de Windows 11
Une fois Windows 11 Business installé, on lance MAS :  
```powershell
irm https://massgrave.dev/get | iex
```
Selectionner ==HWID== (1) puis ==Activate== (1).  

#### Installation de Windows 10
La meilleure version de Windows 10 est ==Windows 10 Entreprise LTSC==.  
C'est une version épurée qui ne contient aucune publicité, la télémétrie est aussi réduite au strict minimum **et les mises à jour vont perdurer 10 ans après la fin de vie de Windows 10**.  
Pour télécharger Windows 10 Entreprise LTSC, se rendre sur [massgrave.dev](https://massgrave.dev/genuine-installation-media.html){ target=about:blank } et cliquer sur ==Windows 10 Entreprise LTSC==.  
Ensuite, choisir la version avec la langue de notre choix.  

#### Activation de Windows 10 Entreprise LTSC
Une fois Windows 10 Entreprise LTSC installé, on lance MAS :  
```powershell
irm https://massgrave.dev/get | iex
```
Sélectionner ==HWID== (1) puis ==Activate== (1).  

#### Migrer vers une version Business/Entreprise de Windows
Si on se retrouve avec une Windows 10/Windows 11 famille, il est possible de basculer sur Entreprise grâce à MAS.  
Cette action requiert une activation par MAS, la clé d'une Windows Famille/Pro ne fonctionnera pas sur Entreprise.  
Pour migrer vers Windows 10 Entreprise/Windows 11 Business, on lance MAS :  
```powershell
irm https://massgrave.dev/get | iex
```
Se rendre dans ==Extras (7)==, ==Change Windows Edition (1)== et choisir l'édition que l'on souhaite.  
Ensuite, répéter les étapes "Activation Windows 10" ou "Activation Windows 11".  
