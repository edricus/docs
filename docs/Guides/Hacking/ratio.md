# Augmenter artificiellement son ratio avec joal server
Joal est un logiciel développé en java disponible sur Windows, MacOS et Linux s'exécutant en ligne de commande et doté d'une interface web.  
Il vise à simuler un partage P2P.  
Il existe aussi une version desktop : [lien](https://github.com/anthonyraymond/joal-desktop/releases)  

## Installation de Joal
Créer un dossier joal dans /opt
```console
# mkdir /opt/joal
```
## Télécharger Joal
```console
$ curl -s https://api.github.com/repos/anthonyraymond/joal/releases/latest \
| grep "browser_download_url.*tar.gz" \
| cut -d : -f 2,3 \
| tr -d \" \
| wget --show-progress -qi -
$ tar -xf joal.tar.gz -C /opt/joal/
```
## Alimenter Joal en torrent
Joal a besoin de fichiers torrent pour simuler un partage.  
Avec des logiciels comme RatioMaster, on ne peut en mettre qu'un seul, mais Joal permet de simuler un partage avec plusieurs torrents.  
Il est préférable de télécharger des torrents ==avec le plus de leechers== c'est-à-dire des personnes téléchargant (représenté par une flèche pointant par le bas sur la plupart des sites de torrents).  
Si possible ==pas les torrents les plus populaires==, car Hadopi les surveillent, on n'est jamais trop prudent.  
Une fois les fichiers torrent téléchargés, il faut les déplacer dans ==/opt/joal/torrents/==

## Configuration de Joal
On va modifier le comportement de Joal avec le fichier ==config.json==.  
La configuration est assez minimale, on va lui renseigner :  

- le taux d'upload minimum, ==3Mo/s==.  
- le taux d'upload maximum, ==10Mo/s==.  
- le nombre de torrents partagés en simultanés, ==5==.  
- le client torrent, on peut laisser celui par défaut, ==utorrent==.  
- si on garde les torrents qu'ils n'ont pas de leechers, ==false==.  

Ce dernier paramètre va déplacer les torrents sans leechers dans le dossier torrents/archived.  
Par défaut, il est en ==true==, Joal va bien sûr arrêter le partage pour ne pas paraître suspicieux.  
On le met ici en ==false== car ces torrents ne sont plus très populaires et vont donc rester actif pour rien.  
### Fichier config.json en détail
```		
{
	"minUploadRate": 3000,
  "maxUploadRate": 10000,
  "simultaneousSeed": 5,
  "client": "transmission-3.00.client",
  "keepTorrentWithZeroLeechers": false
}
```
## Test de Joal
On va tester le bon fonctionnement de Joal avec la commande suivante :  
```console
# java -jar /opt/joal/jack-of-all-trades-2.1.31.jar --joal-conf="/opt/joal/" --spring.main.web-environment=true --server.port=9010 --joal.ui.path.prefix="joal" --joal.ui.secret-token="joal"
```
### Description de la commande
Le fichier exécutable de joal est jack-of-all-trades-[version].jar  

- --joal-conf: le dossier où se situe le fichier de configuration ainsi que les dossiers "torrent" et "clients"  
-  --spring.main.web-environment=true: booléen, active l'interface web  
-  --server.port: le port du serveur, il n'y en a pas par défaut  
-  --joal.ui.path.prefix: le répertoire dans l'URL, dans l'exemple "joal" ce qui donne l'URL d'accès suivante : localhost:9010/joal/ui  
-  --joal.ui.secret-token: un mot de passe à renseigner dans l'interface web pour se connecter au serveur  

J'ai personnellement laissé Joal en localhost, dans cet exemple la sécurité est très faible pour cette raison.

!!! warning "Penser à ajouter une règle pour autoriser le port 9010 (ou autre) dans le pare-feu si besoin"
On va accéder à l'interface web avec l'adresse  
[http://localhost:9010/joal/ui](http://localhost:9010/joal/ui)  
(remplacer "localhost" par l'IP du serveur s'il est à distance).  
On va atterir sur cette page :  
![joal-1.png](../../src/joal-1.png){ width=600 }  
Joal ne peut pas se connecter sans un ==prefixe== et ==mot de passe==.  
Il faut cliquer sur ==change connection settings==.  
Et remplir avec les informations données dans la commande utilisée pour exécuter Joal.  
![joal-2.png](../../src/joal-2.png){ width=500 }  
Pour enfin obtenir  
![joal-3.png](../../src/joal-3.png)
Joal a choisi 5 torrents avec des leechers parmi nos fichiers torrent dans le dossier "torrents".  
On peut aussi voir que la vitesse moyenne (Overall speed à gauche) est comprise entre ==3Mo/s== et ==10Mo/s==.  

## Fichier .service pour une exécution en arrière plan
Maintenant que Joal fonctionne, on va faire un service pour le laisser tourner en arrière-plan
### Création du service
```console
# vim /etc/systemd/system/joal.service
```
```
[Unit]
Description=Joal server
After=network.target
[Service]
User=root
WorkingDirectory=/opt/joal/
ExecStart=/usr/bin/java -jar ./jack-of-all-trades-2.1.31.jar --joal-conf="/opt/joal/" --spring.main.web-environment=true --server.port=9010 --joal.ui.path.prefix="joal" --joal.ui.secret-token="joal"
Restart=always
RestartSec=30
[Install]
WantedBy=multi-user.target
```
### Enregistrement et exécution
```console
# systemctl daemon-reload
# systemctl start joal
```
On peut vérifier l'état du service
```console
# systemctl status joal
```
