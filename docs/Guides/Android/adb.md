# ADB
pour Android Debug Bridge est un outil permettant d'envoyer des commandes à un appareil Android.  

## Installation
#### Activation des options développeurs
Sur Android, il faut aller activer les options pour développeurs.  
Il y a différentes manières de les activer en fonction du constructeur, mais dans la plupart des cas :

- Aller dans les paramètres  
- Sélectionner "À propos du téléphone"  
- Sélectionner "Version du kernel" plusieurs fois jusqu'à obtenir le message "Vous êtes développeur" (généralement 5 fois)  

Sur OxygenOS (OnePlus) il faut sélectionner "Numéro du build" au lieu de "Version du kernel" (qui n'est renseigné)  

#### Connecter ADB
Côté téléphone :

- Brancher le téléphone au PC
- Aller dans les options développeurs  
- Sélectionner "Débogage ADB"

Côté PC :

- Installer adb, sur Debian :

```console
# apt install adb
```
- Autoriser adb en envoyant une première commande

```console
# adb devices
```
Le téléphone devrait réagir avec une boite de dialogue demandant d'autoriser adb  
Cocher "toujours autoriser cet ordinateur" et sélectionner "Autoriser"

## Désinstaller des bloatwares
Un bloatware est un logiciel pré-installé et non-désinstallable.  
Grâce à ADB, il est possible de désinstaller complètement ces logiciels.  

```console title="Lister les applications"
# adb shell pm list packages
```

!!! warning "La commande `adb shell pm list packages` liste les applications sous le format `package:net.oneplus.launcher`. Le nom de l'application est ce qui se trouve après "package:"."

```console title="Désinstaller une application"
# adb shell pm uninstall -k --user 0 [nom-de-l'application]
```

La liste d'application étant assez signifiante, on peut d'abord ==désactiver== sur le téléphone les bloatwares avant de supprimer d'un coup toutes les applications désactivées avec adb.  
Cette méthode est beaucoup plus propre, car les applications système ne sont pas listées dans le gestionnaire d'applications Android.

```console title="Lister toutes les applications désactivés"
# adb shell pm list packages -d
```
```console title="Supprimer toutes les applications désactivés"
# for i in $(adb shell pm list packages -d | cut -d: -f2); do adb shell pm uninstall -k --user 0 $i; done
```

## Activer le mode 90/120Hz tout le temps
Android n'utilise pas tout le temps le mode 90 ou 120hz pour économiser de la batterie.  
Il est possible de forcer le mode haute fréquence.  

```console
# adb shell settings put system display_refresh_mode 0
```

Pour voir si ça a fonctionné, aller dans les paramètres d'Android à l'endroit où la fréquence d'affichage peut être changée.  
Si l'écran indique "60hz" mais que ==aucun des 2 options n'est sélectionnée== cela veut dire que la commande a fonctionné.  
Dans le cas où elle ne fonctionne pas, essayer différentes commandes :

```console
# adb shell settings put system display_refresh_mode 3
# adb shell settings put system peak_refresh_rate 90.0
# adb shell settings put system min_refresh_rate 90.0
```


