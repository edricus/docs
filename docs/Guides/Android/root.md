# Rooter un smartphone Android
!!! info "Ce guide concerne seulement les téléphones sur Android Stock ou légère surcouche"

## Prérequis

  - Un smartphone sous Android Stock ou très légère surcouche  
    Google, OnePlus et Fairphone sont de bons candidats  
    Xiaomi, Samsung, Huawei, Oppo en sont des mauvais  
  - La dernière mise à jour **STABLE**  
  - Un ordinateur avec [fastboot et adb d'installé](https://xdaforums.com/t/official-tool-windows-adb-fastboot-and-drivers-15-seconds-adb-installer-v1-4-3.2588979/){ target=about:blank }  
  - Un backup de ses données (un formatage est requis)    
  - Une **BONNE RAISON** de root son smartphone  
    Exemple: installer des applications comme Adaway, Netguard ou remplacer les services Google par des variantes Open Source.    

## Lexique

  - **Rooter**  
    Déverrouiller l'accès root à son téléphone.  
    Par défaut, il n'est pas possible d'aller plus loin que son répertoire utilisateur (intitulé /sdcard/).  
    L'accès à la racine permet de modifier des fichiers système et donc ajouter de nouvelles fonctionnalités et modifier celles existantes.  
  - **Bootloader**  
    Le programme qui va charger le système d'exploitation (Android)  
    Il monte les partitions, charge le [kernel](../../Système/syslexique.md#kernel) (ou noyau) et procède à des vérifications du système de fichier.  
    En gros, son but est de charger le **firmware**.  
    Le bootloader n'est pas modifiable par défaut, il y a **2 protections**:  
    -> Le ==verrouillage OEM==, qui va nous interdire de déverrouiller le bootloader  
    -> Le ==verrouillage bootloader==, qui va nous interdire de modifier le bootloader  
    Après ça on est libre de modifier son firmware.  
  - **Firmware**  
    Dans le contexte d'Android le firmware est l'OS, la version d'Android conçue pour le modèle de téléphone.  
    La composante de firmware qui nous intéresse pour le root est **boot.img**.  
  - **boot.img**  
    Grossièrement, il s'agit du noyau que le bootloader va charger au démarrage.  
    Ce fichier va être donné à **Magisk** qui va le modifier pour s'inclure dedans.  
  - **Magisk**  
  ![magisk-logo.png](../../src/magisk-logo.png){ width=65 align=left }

    L'application Android de gestion root le plus populaire.  
    Successeur de "SuperSu" de Chainfire.  
    Il gère :  
    
      - les accès root des applications  
      - permet de cacher son existante au vu des applications grâce à **zygisk**   
      - peut installer des "modules", qui sont des logiciels qui vont s'installer au coeur du système pour le modifier  

  - **Zygisk**  
    Anciennement connu sous le nom de "MagiskHide", est une fonction intégrée à Magisk permettant de cacher ce dernier.  
    Sa principale utilité est de passer outre les protections des applications détectant le root.  
    Une application voulant interdire son accès aux utilisateurs root a **2 protections** :  
    -> Safetynet, "l'antivirus" de Google  
    -> Rechercher les applications de gestion root (oui, les applications on accès à la liste complète des applications installés...)  
  - **SafetyNet**  
    "Antivirus" intégré à Android, plus précisément au Play Store.  
    Son but premier est d'empêcher l'installation d'applications suspectes (développeur inconnu) et **d'interdire l'accès d'appareils rootés à certaines applications**.  
    Pour détecter le root, il utilise son programme **CTS Profile**.  
  - **CTS Profile**  
    Pour "Compatibility Test Suite", c'est l'ensemble de tests de SafetyNet pour s'assurer qu'Android n'est pas rooté.  
    C'est ce qu'on va essayer de duper pour faire croire qu'Android n'est pas rooté.  
  - **Flasher**  
    Action de remplacer/installer un programme au coeur du système Android.  
    Ici, on va flasher boot.img (un composant du bootloader), le remplacer par une version autorisant le root.  
  - **Fastboot**
    Est une interface sur Android permettant d'effectuer des commandes de bas niveau.  
    On l'utilise avant tout pour manipuler le bootloader (le déverrouiller).  
    Sur Android on rentre en mode "Fastboot" pour se faire.  
    Il peut ensuite effectuer des commandes depuis un PC avec le logiciel en ligne de commande du même nom.  
  - **ADB**  
    Pour "Android Debug Bridge", permet de lancer des commandes sur Android à l'aide de lignes de commandes depuis un ordinateur.  
    Très utile pour récupérer les logs en direct et supprimer des applications système.  
    [Une page de doc y est consacré ici](adb.md){ target=about:blank }  

## Récupérer le boot.img du firmware
Pour rooter Android, on va utiliser **Magisk**.  
Magisk va nous patcher le boot.img de notre firmware qu'on va ensuite installer via fastboot.  
Pour cela il va nous falloir télécharger **le firmware de notre rom courante**.  
Sur un Pixel c'est très facile :  

  - Pour les téléphones Pixel : aller sur [https://developers.google.com/android/images](https://developers.google.com/android/images){ target=about:blank }  
  - Cliquer sur ==Acknowledge==  
  - À droite, cliquer sur le modèle de téléphone  
  - Descendre tout en bas du tableau  
  - Repérer le nom qui se fini par une date et pas par "AT&T", "Verizon" ou autre  
  - Cliquer sur ==Link==  
Ceci fait le boot.img se situe dans le .zip > dans le second .zip > boot.img.  
  - **Copier ce boot.img à la racine.**  

## Patcher le boot.img
Il faut maintenant installer Magisk.  
[Lien vers Magisk](https://github.com/topjohnwu/Magisk/releases/latest){ target=about:blank }  
Une fois ceci fait :

  - Ouvrir l'application
  - Magisk -> Install
  - Select and Patch a File  
  - Sélectionner le boot.img

**Le boot.img patché se situe dans Downloads/magisk_patched-XXXXXXX.img**

## Flasher le boot.img patché
### 1. Déverrouiller l'OEM
Faire ça va nous permettre d'utiliser des commandes fastboot pour déverrouiller le bootloader.  

  - Paramètres  
  - À propos du téléphone  
  - Taper 5 fois sur "Numéro de build"  
  - Rentrer le code pin du téléphone (l'écran de déverrouillage)  
  - Revenir en arrière  
  - Système  
  - Options pour les développeurs  
  - Déverrouillage OEM  

### 2. Déverrouiller le bootloader
!!! warning "Cette action déclenche un formatage COMPLET du téléphone (applications / fichiers)"
C'est après le déverrouillage du bootloader qu'on pourra toucher à notre boot.img  

  - Brancher le téléphone à un PC  
  - Transférer le boot.img patché (magisk_patched-XXXXXXX.img)  
  - Rentrer la commande suivante :
    `fastboot flashing unlock`  
  - Sur le téléphone, appuyer sur Vol+ ou Vol- pour sélectionner "Déverrouiller le bootloader"  
  - Rentrer la commande suivante pour patcher le boot.img :  
    `fastboot flash boot magisk_patched-XXXXXXX.img`
  - Rebooter le téléphone

### 3. Vérification du root  
À cette étape, le root devrait être installé; pour le vérifier: lancer Magisk.  
Si, en dessous de "Magisk" il est écrit "Ramdisk Yes" c'est que le root est installé.  

![magisk.png](../../src/magisk.png){ width=450 }  

## Bypass SafetyNet
SafetyNet est un logiciel qui va tenter de détecter si l'appareil est rooté ou non.  
Pour ce faire, il exécute des vérifications allant de l'audit du bootloader jusqu'à analyser le kernel à la recherche de modifications.  
**Il sert à bloquer l'accès aux utilisateurs root à certaines applications comme celles des banques**.  
Pour le désactiver, on va passer par un **module magisk**.  

  - [Télécharger le module](https://github.com/chiteroman/PlayIntegrityFix/releases/latest){ target=about:blank }  
  - Ouvrir Magisk
  - Aller dans ==Modules==
  - Install from storage
  - Sélectionner le module (.zip)
  - Redémarrer

Pour vérifier que ça marche, on va télécharger l'application [YASNAC](https://play.google.com/store/apps/details?id=rikka.safetynetchecker){ target=about:blank }  
Si tout se passe bien on devrait avoir l'écran suivant :  
![yasnac](../../src/yasnac.png){ width=400 }

## Cacher Magisk
### 1. Changer le nom et la signature de l'application
Cette étape va remplacer l'application Magisk par un nom générique.  

  - Ouvrir Magisk  
  - Paramètres (en haut à droite)  
  - Hide the Magisk app  
  - Garder "Settings" ou changer, tout sauf "Magisk" évidement  
### 2. Installer Zygisk
Une protection plus avancée qui interdit l'accès en lecture à certaines applications aux répertoires Magisk  

  - Ouvrir Magisk  
  - Paramètres  
  - Cocher "Zygisk" et "Enforce DenyList"  
  - Ouvrir "Configure DenyList"  
  - Sur les 3 points en haut à droite, cocher "Show system apps"  
  - Chercher et cocher : ==Google Play Store== , ==Google Services Framework== , ==Google Play Services== et ==Google Play Protect Service==  
  - En plus de ça, sélectionner toutes autres applications installées bloquant le root  
!!! note "Penser à déployer le menu pour tout cocher"
  - Supprimer le cache de ces applications (play store, play services inclus)  
  - Redémarrer le téléphone  
