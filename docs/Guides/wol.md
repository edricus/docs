# Mise en place de Steam Remote Play
Le but est de pouvoir allumer une machine Windows à distance et de la connecter à Steam afin de pouvoir jouer à distance sans avoir à laisser le PC allumé.  
<figure markdown>
![remote-play.png](../src/remote-play.svg){ width=600 }  
</figure>
## Prérequis

- Raspberry Pi (ou tout autre serveur)  
- Le PC à distance doit être connecté au réseau par câble  
- Steam doit s'authentifier automatiquement (cocher "Se souvenir de moi" sur Steam)

## Windows (PC Gamer)
#### Activer le WakeOnLan
WakeOnLan est un protocole permetant l'activation d'un PC à distance.  

- Ouvrir le Gestionnaire de périphériques
- Double cliquer sur la carte WiFi (cartes réseau -> nom de la carte réseau)  
![wol-windows-1](../src/wol-windows-1.webp){ width=300 }  

- Onglet Avancé -> Wake On Magic Packet -> Valeur : Activé(e)  
![wol-windows-2](../src/wol-windows-2.webp){ width=300 }  

#### Désactiver le mot de passe  
À noter qu'on désactive le mot de passe seulement au démarrage de l'ordinateur.  
On aura toujours besoin d'un mot de passe si la session est verrouillée (avec Ctrl+L par exemple).  

- Ouvrir l'éditeur de registre ==(regedit)==  
- Dans la barre d'adresse, rentrer  
`HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\PasswordLess\Device`  
- Fermer regedit  
- Double cliquer sur `DevicePasswordLessBuildVersion`  
![wol-windows-3](../src/wol-windows-3.png){ width=300 }  

- Définir la valeur à '0'  

!!! info "Si la clé DevicePasswordLessBuildVersion n'apparait pas, il suffit de la créer : Click droit sur la zone à droite -> New -> DWORD(32-bit) Value"

- Ouvrir l'outil de gestion des utilisateurs ==(netplwiz)==  
- ==Décocher== "L'utilisateur doit rentrer un mot de passe pour utiliser cet ordinateur"  
![wol-windows-4](../src/wol-windows-4.png){ width=300 }  

#### Récuperer l'adresse MAC de la carte réseau
Il nous faut l'adresse MAC de la carte réseau, c'est avec cette adresse qu'on va reveiller le PC
```powershell
ipconfig /all
```
![wol-windows-5](../src/wol-windows-5.png){ width=500 }  
L'adresse MAC est l'adresse à 2*6 caractères à côté de Physical Address  

## Raspberry
Sur la RaspberryPi, on va installer un serveur OpenVPN pour se connecter à distance dans le LAN, puis se SSH dedant afin de lancer notre commande WakeOnLan.  

#### Installation de OpenVPN
On va utiliser un script tout fait pour se simplifier la tâche
```console
$ curl -O https://raw.githubusercontent.com/angristan/openvpn-install/master/openvpn-install.sh
$ chmod +x openvpn-install.sh
$ sudo ./openvpn-install.sh
```
==Adresse IP== : l'adressse IP **publique** et non locale, se rendre sur <a href="https://ifconfig.me" target="_blank">ifconfig.me</a> pour la connaître  
Continuer avec les choix par defaut jusqu'à "Client name"  
==Client name== : un nom au choix, ce sera le nom du fichier .ovpn que le script va génerer et qui va servir à se connecter au VPN depuis un PC client.  
==Mot de passe== : sur un PC portable, il est préférable d'en mettre un pour éviter que n'importe qui avec le fichier .ovpn puisse se connecter à distance.  

À la fin du script, le fichier [client's name].ovpn sera créé dans le dossier courant.  
==Copier ce fichier== sur le client qui souhaite se connecter à distance, il va servir à s'authentifier sur le serveur.  

#### Configuration du WakeOnLan

- On installe l'outil ethtool

```console
# apt install ethtool
```

- On determine le nom de sa carte réseau physique

```console
$ ip a
```
Le nom commence par ==enp== et a une ip (inet)
```
2: enp88s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 48:21:0b:2d:90:ba brd ff:ff:ff:ff:ff:ff
    inet 192.168.1.121/24 brd 192.168.1.255 scope global enp88s0
       valid_lft forever preferred_lft forever
    inet6 fe80::4a21:bff:fe2d:90ba/64 scope link 
       valid_lft forever preferred_lft forever
```
Dans cet exemple le nom de la carte est ==enp88s0==

- On active le support du WakeOnLan sur cette interface

```console
# ethtool --change enp88s0 wol g
```

- Installer l'outil WakeOnLan

```console
# apt install wakeonlan
```

## Client (Linux)
#### Se connecter à la Raspberry

- Installer openvpn 

```console
# apt install openvpn
```

- Se connecter à son VPN (avec le fichier qu'on a copié depuis la Raspberry

```console
# openvpn --config [client's name].ovpn
```

- Se connecter en SSH à la Raspberry

```console
$ ssh [nom d'utilisateur sur la raspberry@[ip de la raspberry]
```

#### Allumer le PC Gamer à distance

```console
$ wakeonlan <adresse-mac>
```

À cette étape on peut essayer d'allumer le PC à distance avec cette commande.  
Il faut éteindre le PC et avec la raspberry, on lance la commande `wakeonlan <adresse-mac>.  
Si ça ne marche pas :  

- Vérifier que le PC est branché au réseau avec le câble  
- Vérifier s'il existe un paramètre dans le BIOS pour le WakeOnLan, parfois il faut aussi activer cette option en plus de celle dans Windows  
- Vérifier que l'adresse MAC est celle de la carte **Ethernet** et non **Wireless** (WiFi), le WakeOnLan ne peut pas fonctionner en WiFi.  

Après ça on ==désactive le VPN== puisque le cloud gaming avec Steam se fait via Internet, pas besoin d'être sur le même réseau.  
Les jeux installés sur le PC à distance devraient être listés comme s'ils étaient installés et la mention "Streamer" doit avoir remplacé "Jouer"
