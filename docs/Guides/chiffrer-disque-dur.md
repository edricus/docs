# Chiffrer un disque dur
L'action de chiffrer son disque dur est indispensable à la sécurité de ses données.  
Un mot de passe de session ne protège pas les données contrairement à ce que l'on croit, les données sont accessible simplement en accèdant au disque dur depuis un autre système d'exploitation.  
Un mot de passe de session n'est même pas protégé en soit, sur linux on peut utiliser la commande "chroot" depuis un autre système pour acceder à une session root sur le disque dur, et faire ce qu'on veut.  
Le chiffrement permet donc de rendre illisible toutes données qu'on essayerait d'acceder sans clé.  

Sur Linux, on utilise ==LUKS== (pour Linux Unified Key Setup), qui est le standard implémenté dans le noyau Linux pour le chiffrement des disques durs.  
Sur Windows on utilisera plutôt l'utilitaire de Microsoft : ==BitLocker==  
Il existe bien évidement des alternatives comme ==VeraCrypt== qui est le successeur de l'obsolète TrueCrypt qui a l'avantage d'être universel et open-source.  

## Chiffrement d'une partition home
La partition home est souvent la plus sensible, beaucoup choisissent de chiffrer seulement cette partition.  
Il faut évidement au préalable l'avoir séparé, ce qui n'est pas le choix par défaut des supports d'installation.

### Installation de cryptsetup
On va utiliser LUKS.  
L'utilitaire à installer est cryptsetup, qui fourni les commandes relatives au format LUKS.  
```console
# apt install cryptsetup
```

## Formatage au format LUKS
La partition doit maintenant être formatée au format LUKS.  
==Toutes donnée dessus sera perdue==.  
```console
# cryptsetup luksFormat /dev/sdXy
# cryptsetup luksOpen /dev/sdXy cryptpart
# mkfs.ext4 /dev/mapper/cryptpart
```
remplacer "ext4" par un autre format si besoin, par exemple xfs : mkfs.xfs  

## Montage au démarrage
On va déterminer l'UUID de la partition LUKS
```console
# blkid | grep LUKS
```
```
/dev/sda3: UUID="48a5cb4e-97b5-4fd3-8ce6-3ee02dfa6e60" TYPE="crypto_LUKS" PARTUUID="7d27c958-2a28-490d-86a8-487b38ac36bc"
```
On va modifier le fichier ==/etc/crypttab== qui permet de déchiffrer la partition au démarrage.  
```title='/etc/crypttab'
# <target name>	<source device>		<key file>	<options>
crypt_home  48a5cb4e-97b5-4fd3-8ce6-3ee02dfa6e60    none    luks,discard,initramfs
```
On va modifier le fichier ==/etc/fstab== qui est le fichier indiquant qu'elles partitions montrer au démarrage.  
```title='/etc/fstab'
/dev/mapper/crypt_home /home           xfs     defaults        0       0
```

## Déchiffrer sa partition en SSH
Dans le cas d'un serveur, on veut pas avoir à taper un mot de passe sur un clavier branché au PC.  
Pour ce faire il faut initier une connection SSH dans l'initramfs.  
On utilise pour ça **Dropbear SSH** qui est un serveur SSH conçu pour s'éxecuter dans ce genre d'environnement.  
### Installation
```
# apt install dropbear-initramfs
```
### Configuration de dropbear
On va d'abord générer une paire de clé sur l'ordinateur ==client==  
```console
$ ssh-keygen -b 4096
```
Ensuite copier la clée publique dans le fichier **authorized_keys** de dropbear
```console
ssh -t <user>@<ip-du-serveur> "sudo bash -c 'cat << EOF >> /etc/dropbear/initramfs/authorized_keys
<copier la clé publique ici>
EOF
"'
```
!!! info "`cat ~/.ssh/id_rsa.pub` pour afficher la clée publique"
Modifier le fichier config de dropbear  
```title="/etc/dropbear/initramfs/dropbear.conf"
...
DROPBEAR_OPTIONS="-I 180 -j -k -p 2222 -s -c cryptroot-unlock"
...
```
??? info "Description des options"
    **-I 180**  = Déconnecte la session si aucun trafic n'a été détécté en N secondes  
    **-j** = Désactive le ssh port forwarding en local  
    **-k** = Désactive le ssh port forwarding à distance  
    **-p** = Spécifie le port  
    **-s** = Désactive le mot de passe  
    **-c** = Spécifie une commande dès la connection  

Et mettre à jour l'initramfs  
```console
$ ssh -t <user>@<ip-du-serveur> "sudo update-initramfs -u"
```
avec cette commande, Dropbear va pouvoir s'initier lors de l'initramfs (un environnement linux minimal), il va aussi récuperer les clés publiques des fichiers "authorized_keys" des utilisateurs.  

### Fixer une IP dans l'initramfs
Par défaut, l'initramfs n'est pas connecté au réseau.  
On va donc fixer une IP pour pouvoir se connecter à SSH dans cet environnement.  
```title="/etc/initramfs-tools/initramfs.conf"
IP=192.168.1.200::192.168.1.254:255.255.255.0:debian
```
La syntaxe est
```console
IP=<ip>::<passerelle>:<masque>:<hostname>
```
Pour pas s'embêter on peut aussi lui dire de faire une requête DHCP.
```title="/etc/initramfs-tools/initramfs.conf"
IP=dhcp
```
...à condition d'avoir fait un bail fixe pour notre serveur dans l'interface de notre routeur  

Pour finir, on va charger le clavier en AZERTY au démarrage pour éviter de devoir taper notre mot de passe en QWERTY  
```title='/etc/initramfs-tools/initramfs.conf'
KEYMAP=y
```
On met à jour l'initramfs  
```console
# update-initramfs -u
```

### Dechiffrement
Pour se connecter à dropbear, on utilise le compte root  
```
$ ssh -p 2222 root@<ip-du-serveur>
```

