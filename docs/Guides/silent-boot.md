# Configurer un boot silencieux
Ce guide a pour but de démarrer de manière silencieuse sa distribution linux, en supprimant GRUB et en ajoutant une animation au démarrage, remplaçant le traditionnel écran noir montrant les services et messages du kernel.  

## Boot splash avec Plymouth
Plymouth est un logiciel s'exécutant très tôt au démarrage de linux (avant le montage de la partition root, donc à l'aide de l'initramfs) permettant de cacher les messages de démarrage, notamment ceux de systemd.  
Il est très personnalisable, il existe de ce fait beaucoup de thèmes personnalisés, le paquet `plymouth-themes` en offre déjà quelque uns, même si vieillissant en matière de goût.
#### Installation
!!! info "à noter que Plymouth est pré-installé sur certaines distributions, mais pas activé"
```  
# apt install plymouth plymouth-themes
```
#### Configuration de GRUB
On va ajouter l'option "splash" dans la commande de boot du kernel pour permettre à plymouth d'être exécuté à l'initialisation de celui-ci.  
```console
# vim /etc/default/grub
```
```
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"
```

#### Utilisation des thèmes génériques
On va maintenant installer le thème BGRT, ce thème a la particularité d'afficher le logo présent lors de l'allumage du PC (si présent et accessible).  

- Lister les thèmes pré-installés

```console
$ plymouth-set-default-theme -l
```
- Appliquer le thème

```console
# plymouth-set-default-theme bgrt
```
- Régénérer l'initramfs

```console
# update-initramfs -u
```

- Régénérer la configuration de GRUB

```console
# update-grub
```

#### Utilisation d'un thème personnalisé
En fouillant sur le web, on peut trouver ce projet reprenant l'esthétique de l'animation de Windows 10, mais avec le logo des fabricants.  
Il diffère peu du thème BGRT.  
[https://github.com/gevera/plymouth_themes](https://github.com/gevera/plymouth_themes)  

- Clonage du projet  

```console
$ git clone https://github.com/gevera/plymouth_themes
```

- Installation du thème "Thinkpad"

```console
$ cd plymouth_themes/thinkpad/think10
$ sudo make install
$ sudo plymouth-set-default-theme think10
```

- Régénérer l'initramfs

```console
# update-initramfs -u
```

- Régénérer la configuration de GRUB

```console
# update-grub
```

#### Résultat  
![preview](../src/preview.gif)   
(j'ai changé le logo thinkpad en debian, [lien du projet](https://gitlab.com/edricus/debian-plymouth-win10))
## Supprimer GRUB
GRUB est inutile si on ne fait pas de dual-boot.  
Il est possible de booter directement sur la partition grâce à la fonction "EFI-STUB" du kernel.  
On va créer une entrée EFI permettant de boot vmlinuz, la version compressée et exécutable du kernel linux.  
Dans cette même entrée on va renseigner la commande à envoyer à vmlinuz pour monter la partition root avec l'argument "quiet" et "splash".  
Il s'agit de la même commande que GRUB utilise pour booter.  

#### Copie de initrd et vmlinuz dans le répertoire EFI
```console
# mkdir /boot/efi/EFI/Debian-EFI
# cp /vmlinuz /initrd.img /boot/efi/EFI/Debian-EFI/
```
#### Script de post mise à jour du kernel
Une mise à jour du kernel est fréquente, il est donc indispensable d'automatiser la copie de vmlinuz et initrd après ces maj.  

- Le script

```bash
#!/bin/bash
echo "Copying initrd.img and vmlinuz to /boot/efi/EFI/Debian-EFI/ ..."
cp /vmlinuz /initrd.img /boot/efi/EFI/Debian-EFI/
```
On le nommera ==zz-update-efistub==.  

- Le rendre exécutable

```console
$ chmod +x zz-update-efistub
```

- Copie dans les repertoires de scripts post-update

```console
# cp zz-update-efistub /etc/kernel/postinst.d
# cp zz-update-efistub /etc/kernel/postrm.d
# mkdir -p /etc/initramfs/post-update.d/
# cp zz-update-efistub /etc/initramfs/post-update.d/
```

#### Entrée EFI
Maintenant, on va créer notre entrée EFI  
On a besoin de 3 informations :  
- Le nom du disque dans /dev  
- Le numéro de la partition EFI  
- L'UUID de la partition root

Nom du disque :  
```console
$ lsblk
```
```
NAME        MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
nvme0n1     259:0    0 465.8G  0 disk 
├─nvme0n1p1 259:1    0   476M  0 part /boot/efi
├─nvme0n1p2 259:2    0    30G  0 part 
├─nvme0n1p3 259:3    0     4G  0 part [SWAP]
├─nvme0n1p4 259:4    0  27.9G  0 part /
└─nvme0n1p5 259:5    0 403.4G  0 part /home
```
Ici ==/dev/nvme0n1==.  
La partition EFI est généralement la première d'environs 500Mo-2Go.  
Ici ==/dev/nvme0n1p1==, on ne retiendra que ==1==.  
Ensuite l'UUID de la partition root  
```console
# blkid
```
```
/dev/nvme0n1p1: UUID="8E44-5612" BLOCK_SIZE="512" TYPE="vfat" PARTLABEL="EFI" PARTUUID="c2bfa335-185e-4be6-8817-b90b29758c7d"
/dev/nvme0n1p2: LABEL="Gentoo" UUID="9f6117ed-cd17-426c-be11-0ceb9f68e306" BLOCK_SIZE="4096" TYPE="ext4" PARTLABEL="Gentoo" PARTUUID="82aad2b0-f951-4cb4-92fd-d3cd7b19672b"
/dev/nvme0n1p3: UUID="f17a2a59-b325-41b8-825a-fde7a99fc1cc" TYPE="swap" PARTLABEL="linux-swap" PARTUUID="6c35837f-8e74-466d-9d51-f70a83a5a4ff"
/dev/nvme0n1p4: LABEL="Debian-EFI" UUID="8fa786a6-81c4-4553-abe7-1a78cf3d98d8" BLOCK_SIZE="4096" TYPE="ext4" PARTLABEL="Debian 11" PARTUUID="66f0dda7-b93f-4ab3-b8a2-01c4f6b1a765"
/dev/nvme0n1p5: UUID="94febd75-1594-4062-b3cf-d5393160a5cf" UUID_SUB="5db013da-bfc4-4c49-bf69-d1d5d0091e1c" BLOCK_SIZE="4096" TYPE="btrfs" PARTLABEL="Data" PARTUUID="3a92101b-af9a-46c5-93b3-925493c9f695"
```
Cette commande liste toutes les informations des partitions, ce qui nous intéresse et le ==PARTUUID== de la partition ==root==.  
La partition root est celle montée sur '/', indiquée plus haut avec la commande `lsblk`.  
Ici ==66f0dda7-b93f-4ab3-b8a2-01c4f6b1a765== (le PARTUUID de /dev/nvme0n1p4).  

Donc le disque : /dev/nvme0n1  
Le numéro de la partition EFI: 1  
L'UUID de la partition root : 66f0dda7-b93f-4ab3-b8a2-01c4f6b1a765  

On peut maintenant créer notre entrée EFI :
```console
# efibootmgr -c -g -L 'Debian-EFI' -d /dev/nvme0n1 -p 1 -l '\EFI\Debian-EFI\vmlinuz' -u 'root=PARTUUID=66f0dda7-b93f-4ab3-b8a2-01c4f6b1a765 rw quiet splash rootfstype=ext4 add_efi_memmap initrd=\EFI\Debian-EFI\initrd.img'
```

!!! "Pour les systèmes de fichier BTRFS"
    ```console
    # efibootmgr -c -g -L 'Debian-EFI' -d /dev/nvme0n1 -p 1 -l '\EFI\Debian-EFI\vmlinuz' -u 'root=PARTUUID=66f0dda7-b93f-4ab3-b8a2-01c4f6b1a765 rw quiet splash rootfstype=btrfs rootflags=subvol=@ add_efi_memmap initrd=\EFI\Debian-EFI\initrd.img'
    ```
    où "@" est le subvol du root
    Pour lister les subvol BTRFS : `btrfs subvolume list /`

Pour lister les entrées EFI :
```console
# efibootmgr
```
```
BootCurrent: 0003
Timeout: 2 seconds
BootOrder: 0003,0001
Boot0001* Gentoo
Boot0002* Linux-Firmware-Updater
Boot0003* Debian-EFI
Boot0010  Setup
Boot0011  Boot Menu
Boot0012  Diagnostic Splash Screen
Boot0013  Lenovo Diagnostics
Boot0014  Startup Interrupt Menu
Boot0015  Rescue and Recovery
Boot0016* USB CD
Boot0017* USB FDD
Boot0018* NVMe0
Boot0019* NVMe1
Boot001A* ATA HDD0
Boot001B* USB HDD
Boot001C* PCI LAN
Boot001E* Boot Next Boot Option
```
On a notre entrée qu'on a nommée "Debian-EFI" sur Boot0003.  
