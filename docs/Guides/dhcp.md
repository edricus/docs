# Configurer un serveur DHCP
Ce guide a pour but l'installation et la configuration d'un serveur DHCP sur Debian 12.  
Ce serveur aura un accès internet et une réseau à part pour les machines.  

## Prérequis

  - 1x serveur sous Debian 12 avec 2 cartes réseau et une connection internet
  - 1x machine client

## Configuration du serveur
### Installation des paquets
```console
# apt-get install -y isc-dhcp-server iptables vim
```

### Attribution d'une IP statique au serveur
On va d'abord noter nos interfaces
```console
$ ip a
```
```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host noprefixroute 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:b4:8c:c9 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic enp0s3
       valid_lft 86340sec preferred_lft 86340sec
    inet6 fe80::a00:27ff:feb4:8cc9/64 scope link 
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e4:12:49 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::a00:27ff:fee4:1249/64 scope link 
       valid_lft forever preferred_lft forever
```

Ici l'interface qui sera utilisée par le serveur DHCP sera ==enp0s8==, c'est celle qui n'a pas d'IP.  
Ensuite on va fixer une IP :  
```title="/etc/network/interfaces"
allow-hotplug enp0s8
iface enp0s8 inet static
  address 10.0.0.1
  netmask 255.255.255.0
```

Dans ce fichier :  

  - `allow-hotplug enp0s8` : permet de réactiver la connection après un câble debranché  
  - `iface enp0s8 inet static` : défini l'interface en mode statique  
  - `address` : l'ip qu'on veut attribuer à cette interface  
  - `netmask` : le masque associé à l'IP  

Et on redémarre le service networking
```console
# systemctl restart networking
```
On peut vérifier que l'interface a bien une IP  
```console
$ ip a
```
```
[ ... ]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e4:12:49 brd ff:ff:ff:ff:ff:ff
    inet 10.0.0.1/24 brd 10.0.0.255 scope global enp0s8
[ ...]
```

!!! error "Debug : `# journalctl -u networking`"

### Configuration de isc-dhcp-server
On a besoin de definir quelle interface notre serveur dhcp va utiliser.  
Rappel : enp0s8 dans notre cas  
```title='/etc/default/isc-dhcp-server'
INTERFACESv4="enp0s8"
#INTERFACESv6=""
```

### Configuration de dhcpd
Il s'agit du daemon dhcp, il se configure comme suit :  
```title='/etc/dhcp/dhcpd.conf'
default-lease-time 600;
max-lease-time 7200;

subnet 10.0.0.0 netmask 255.255.255.0 {
  interface enp0s8;
  range 10.0.0.1 10.0.0.253;
  option subnet-mask 255.255.255.0;
  option routers 10.0.0.254;
}
```

  - `default-lease-time` : le temps minimum de bail  
  - `max-lease-time` : le temps maximum de bail  
  - `subnet` : le sous-réseau qui sera utilisé par notre dhcp  
  - `interface` : celle qu'on utilise pour notre dhcp  
  - `range` : l'étendue d'IP que les clients vont se voir attribués par notre dhcp  
  - `option subnet-mask` : le masque de notre subnet  
  - `option routers` : l'ip de notre serveur dhcp  

On peut maintenant démarrer notre serveur dhcp :  
```console
# systemctl restart isc-dhcp-server
# systemctl enable isc-dhcp-server
```

!!! error "Debug : `# journalctl -u isc-dhcp-server`"

### Configurer le NAT pour notre serveur DHCP

Maintenant que notre réseau est configuré, les clients peuvent se connecter.  
Le problème c'est qu'ils ne peuvent pas acceder à internet puisque le traffic sortant s'arrête à notre interface dhcp et ne va pas sur notre interface nat (celle qui a accès à internet).  
On va pour celà utiliser une règle MASQUERADE.  
Cette règle va être attribuée à notre interface disposant d'une connection internet, elle va servir à ==masquer== l'IP source du paquet (venant de notre client) et lui attribuer à la place l'IP de ==sortie== de notre interface accedant à internet.  
"L'IP de sortie de notre interface accedant à internet" est tout s'implement l'IP publique, donc le paquet du client va utiliser l'IP publique de notre serveur, qui est servie par l'interface accedant à internet.  

![dhcp](../src/dhcp.png)  

```console
# iptables -t nat -A POSTROUTING -o enp0s3 -j MASQUERADE
```
