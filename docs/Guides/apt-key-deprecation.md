# Migrer de trusted.gpg vers /usr/share/keyrings
Apt-key est obsolète.  
Avant, on utilisait apt-key pour ajouter des clés dans le fichier ==/etc/apt/trusted.gpg==.  
Cette méthode avait une faille majeure : il était possible de signer une application avec la clée d'une autre application (si toute deux était dans trusted.gpg).  
Le problème est que si la clé d'une application a expirée, elle peut toujours être signée par une autre, c'est un problème.  
!!! warning "Ne pas utiliser trusted.gpg.d pour les repos alternatifs !"
    La raison principale pour laquelle trusted.gpg est obsolète est parce qu'apt accepte les clés dans ce fichier qu'importe la source.  
    Ce comportement est le même pour les clés dans trusted.gpg.d.  
    Même si le message de apt-key incite à le faire, il est préférable de ne pas mettre ses clés dans /etc/apt/trusted.gpg.d.

## Migrer un dépôt installé
On va lister les clés contenues dans ==/etc/apt/trusted.gpg==
```console
# apt-key list
```
```
pub   rsa4096 2019-04-14 [SC] [expires: 2027-04-12]
      5E61 B217 265D A980 7A23  C5FF 4DFA B270 CAA9 6DFA
uid           [ unknown] Codium Signing Key <ftpmaster@codium.org>
sub   rsa4096 2019-04-14 [S] [expires: 2027-04-12]
```

Sur la ligne **pub** on a une suite en hexadécimal.  
On va l'exporter et la deplacer dans ==/usr/share/keyrings/==
==On a besoin des 4 dernier caractères==, dans notre exemple **CAA96DFA**.  
==On a aussi besoin d'un nom évocateur==, dans 'uid' on voit "codium signing key. Cette clée est celle de VSCodium.  
```console
# apt-key export CAA96DFA | gpg --dearmor -o /usr/share/keyrings/codium-key.gpg
```
??? info "Explication de la commande"
    `apt-key export` : exporte la clée au format gpg  
    `gpg --dearmor -o` : convertis la clée au format gpg binaire et la place dans /etc/apt/trusted.gpg.d sous le nom ==codium-key.gpg==  

Après ça on fait un `$ sudo apt update`  
  
## Ajouter un dépôt 
Il y a 2 manières d'ajouter un dépôt que l'on trouve sur internet.  
### La plus sûre 
On l'a trouve en majoritée sur internet et aucune modification est necessaire.    
Ces dépôts utilisent la fonction "signed-by" qui permet d'indiquer le chemin de la clé sur le stockage.  
Elle ressemble à ça
```console
$ wget -O- https://www.virtualbox.org/download/oracle_vbox_2016.asc | sudo gpg --dearmor -o /usr/share/keyrings/oracle-virtualbox-2016.gpg
$ echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/oracle-virtualbox-2016.gpg] https://download.virtualbox.org/virtualbox/debian stable contrib' | sudo tee /etc/apt/sources.list.d/virtualbox-6.1.list
```
On télécharge la clé dans ==/usr/share/keyrings== et dans la ligne du dépôt on indique le chemin vers cette clée.  
C'est d'ailleurs la méthode que apt-key recommande.  
### La méthode obsolète 
Elle ressemble à ça
```console
$ wget -qO - http://deb.opera.com/archive.key | sudo apt-key add -
$ echo "deb https://deb.opera.com/opera-stable/ stable non-free" | sudo tee /etc/apt/sources.list.d/opera-browser.list
```
On télécharge le fichier et on l'envoit vers la commande `apt-key` qui l'ajoute ensuite dans ==/etc/apt/trusted.gpg== (avec l'option add).  
Pour changer ça, on va envoyer la clée dans gpg et la convertir au format binaire pour la mettre dans ==/usr/share/keyrings==.  
```console
$ wget -qO - http://deb.opera.com/archive.key | gpg --dearmor -o /usr/share/keyrings/opera-browser.gpg
$ echo "deb [signed-by=/usr/share/keyrings/opera-browser.gpg] https://deb.opera.com/opera-stable/ stable non-free" | sudo tee /etc/apt/sources.list.d/opera-browser.list
```
### La deuxième méthode obsolète
En général quand on a pas de clé, la commande `apt update` nous renvoit
```
Err:4 https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs vscodium InRelease
  The following signatures couldn't be verified because the public key is not available: NO_PUBKEY 54678CF75A278D9C
```
Apt nous donne la clée non reconnue, dans l'exemple : ==54678CF75A278D9C==  
La commande commune est de l'importer depuis un serveur de clé, en général Ubuntu  
```console
# apt-key add --keyserver keyserver.ubuntu.com --recv-keys 54678CF75A278D9C
```
Cette commande ajoute la clée dans /etc/apt/trusted.gpg, ce qu'on ne veut pas.  
La commande à faire dorénavant :
```console
# gpg --no-default-keyring --keyring /usr/share/keyrings/vscodium.gpg --keyserver keyserver.ubuntu.com --recv-keys 54678CF75A278D9C
```
Ne pas oublier de changer ```/etc/apt/source.list.d/codium.list``` avec l'option [signed-by] :  
```
deb [signed-by=/usr/share/keyrings/codium-key.gpg] https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs vscodium main
```
