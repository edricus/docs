# Wordpress avec EKS
## EKS
pour Elastic Kubernetes Service, est un service qui va se charger de créer un cluster et déployer nos applications.  
Il va inclure un load balancer et une gestion des masters/workers comme le fait K3sup.  
Le but est de permettre un déploiement rapide dans un écosystème homogène.  
On va seulement utiliser Helm pour déployer nos services.  

## eksctl
est un outil en ligne de commande développé par Amazon permettant de gérer les clusters EKS.  
Tout peut se faire sur le site d’AWS, mais eksctl rend le déploiement beaucoup plus simple puisqu’il se charge automatiquement des permissions IAM, en plus de s'intégrer à kubectl.  

### Installation de eksctl

- Créer un dossier .aws à la racine du répertoire de l'utilisateur
```console
$ mkdir /home/$USER/.aws
```
- Dans ce dossier, créer un fichier 'config'
```console
$ cd /home/$USER/.aws
$ vim config
```
```
[default]
region=us-east-2
output=json
```
- Dans ce dossier, créer un fichier 'credentials'
```console
$ vim credentials
```
```
[default]
aws_access_key_id=<ACCESS_KEY>
aws_secret_access_key=<SECRET_KEY>
```
ℹ️  Pour obtenir ces 2 clés :

	a) Console EC2 ou tout autre service  
	b) Cliquer sur le nom du compte en haut à droite  
	c) Security credentials  
	d) Create access key  
  

- Installation de awscli  
Cet utilitaire est nécessaire pour eksctl, il va permettre la gestion d’aws, il crée les instances et les permissions.  
Eksctl va plus se concentrer sur les clusters et les nodegroups.  
```console
$ curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
$ sudo ./aws/install
```
- Installation de eksctl
```console
$ curl -sL "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp/
$ sudo mv /tmp/eksctl /usr/local/bin/
```

### Création du cluster
On va créer le cluster qui va accueillir nos nodes.  
À changer: `--name`
!!! warning "Sans le paramètre `--without-nodegroup`, la commande `eksctl create cluster` un groupe de nodes avec 2 nodes de type "m5.large"."  
```console
$ eksctl create cluster \
  --name edricus-eks-cluster \
  --region us-east-2 \
  --without-nodegroup
```
Après plusieurs dizaines de minutes, le cluster est créé.  

### Création du nodegroup
On va maintenant créer un groupe de nodes.  
Il va se composer de 3 nodes (minimum 2 maximum 4) de machines t2.medium.  
À changer: `--name` et `--cluster`
```console
$ eksctl create nodegroup \
  --cluster edricus-eks-cluster \
  --name edricus-eks-nodegroup \
  --node-type t2.medium \
  --nodes 3 \
  --nodes-min 2 \
  --nodes-max 4
```
Ce qui nous donne avec kubectl:
```console
$ kubectl get nodes
```
```
NAME                                           STATUS   ROLES    AGE
ip-192-168-24-95.us-east-2.compute.internal    Ready    <none>   1m
ip-192-168-62-193.us-east-2.compute.internal   Ready    <none>   1m
ip-192-168-86-154.us-east-2.compute.internal   Ready    <none>   1m
```

### Déploiement de Wordpress
Maintenant qu’on a un cluster et nos nodes, tout ça configuré pour être géré avec kubectl, on peut utiliser Helm pour installer notre Wordpress  

- Installation de Helm  
Helm est un outil permettant de faciliter l’installation de services dans K8s.  
C’est comme un gestionnaire de paquets, mais pour K8s.  
On va ajouter le repo et l’installer avec apt  

```console
$ curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg
$ sudo apt install apt-transport-https -y
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
$ sudo apt update
$ sudo apt install helm -y
```

- Ajout du dépôt Bitnami

Bitnami est une solution permettant de faciliter l’installation de nombreux services.  
Ils proposent un dépôt pour Helm, qui va nous permettre de déployer Wordpress facilement.  
```console
$ helm repo add bitnami https://charts.bitnami.com/bitnami
```

- Installation de Wordpress

```console
$ helm install edricus-wordpress bitnami/wordpress
```

Après l’install, on affiche les informations du service
```console
$ kubectl get svc --namespace default edricus-wordpress
```
Ce qui nous donne
```
NAME              TYPE           CLUSTER-IP       EXTERNAL-IP      PORT(S)
edricus-wordpress   LoadBalancer   10.100.215.195   <url-publique>   80:31214/TCP,443:31789/TCP
```
On va accéder au service grâce à l’url publique indiquée sous 'EXTERNAL-IP'


