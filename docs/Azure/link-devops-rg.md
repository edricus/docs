# Connecter Azure DevOps et Azure Ressource Manager
Pour créer et gérer des pipelines, il est necessaire de connecter ces 2 outils.  

## Créer un service principle
C'est un compte d'application qui va servir à connecter les 2 outils - Azure DevOps et Azure Ressource Manager.  

```console
$ az login
$ az ad sp create-for-rbac -n SP_DEVOPS_MAYTEDDY
```
```
"appId": "874a8f41-5248-4985-a733-ec8ce4578d2c",
"displayName": "SP_DEVOPS_MAYTEDDY",
"password": ". . .",
"tenant": "a2e466aa-4f86-4545-b5b8-97da7c8febf3"
```

On peut créer un secret dans un keyvault avec le mot de passe  

## Créer un managment group
Sur Azure:  
```
Managment Groups 
  -> Create  
```
La création dure quelque minutes  
```
IAM 
  -> Add Role assignment 
     -> Privileged administrator roles 
        -> Owner  
```
```
Members 
  -> <Nom du service principle>  
```

## Connecter le sp à devops
Sur Azure DevOps :  
```
Projet 
 -> Project settings  
    -> Services connection  
       -> Create service connection  
          -> Azure Resource Manager  
             -> Service Principal (manual)  
```
**Scope Level**: Management Group  
**Group ID**: le nom du managment group  
**Managment Group Name**: pareil  
**Service Principal Id**: le "appID" du service principal  
**Service principal key**: le "password" du service principal  
**Tenant ID**: apparait dans l'output du service principe, "tenant"  
