# Utiliser docker dans Azure
Déployer des services docker dans un environnement Azure grâce à Azure Container Instances.  

![docker-azure](../src/azure-docker.png)  

## Activer les experimental features à Docker
Docker ne permet pas de se connecter à Azure par défaut, il faut créer le fichier avec le contenu suivant 

```title="/etc/docker/daemon.json"
{
    "experimental": true
}
```
```console
# systemctl restart docker
```

On ferme le terminal et on le réouvre, sous peine d'avoir l'erreur "Error response from daemon: Get "https://azure/v2/": dial tcp: lookup azure: no such host"  
Après ça on peut se connecter à Azure, une fenêtre dans le navigateur doit s'ouvrir.

```console
$ docker login azure
```
```
login succeeded
```
Ensuite on peut créer un contexte docker pour azure

```
$ docker context create aci azure
```
```
? Select a resource group RG_Edri (francecentral)
Successfully created aci context "azure"
```

... et basculer dessus

```console
$ docker context use azure
```
```console
$ docker context ls
```
```
NAME                TYPE                DESCRIPTION                               DOCKER ENDPOINT               KUBERNETES ENDPOINT   ORCHESTRATOR
azure *             aci                 RG_Edri@francecentral
default             moby                Current DOCKER_HOST based configuration   unix:///var/run/docker.sock                         swarm
```
!!! warning "Afin d'éviter l'erreur humaine et engager des frais imprévus, mieux vaut rester sur le contexte par défaut et utiliser l'option --context de docker pour lui dire d'executer des commandes dans Azure."

Maintenant qu'on est dans le contexte azure, on peut utiliser des commandes, on va déployer un Wordpress.  

## Utiliser la commande docker
```console
$ docker run -p 80:80 --name wordpress wordpress:latest
```

## Utiliser docker-compose
!!! warning "À partir de novembre 2023, le support de l'intégration de ECS (Amazon) et ACI (Azure) sera abandonné dans docker compose"

```yaml title="docker-compose.yml"
version: '2'
services:
  wordpress:
    image: wordpress:latest
    ports:
      - 80:80
    restart: always
```
```console
$ docker compose up
```
!!! info "On utilise `docker compose up` et non `docker-compose up` car on utilise une fonction de docker, et non l'application docker-compose"

```
[+] Running 2/2
 ⠿ Group wordpress  Created                                                                                                        2.4s
 ⠿ wordpress        Created                                                                                                       60.9s
```
Une fois que tout est déployé, on peut faire

```console
$ docker ps
```

Pour voir l'IP du conteneur

```
CONTAINER ID          IMAGE               COMMAND             STATUS              PORTS
wordpress_wordpress   wordpress:latest                        Running             20.19.96.124:80->80/tcp
```

On peut désormais y accéder.

## Persistance
Pour activer la persistance il faut déjà au préalable avoir créé un compte de stockage et un file share.  
Éventuellement, on peut créer le file share avec docker

```console
$ docker volume create wordpress --storage-account <storage-name>
```

S'il n'y a qu'un seul compte de stockage, inutile de le préciser.  
Maintenant, on peut ajouter ce volume à notre docker-compose.yml :  

```yaml title="docker-compose.yml"
version: '2'
services:
  wordpress:
    image: wordpress:latest
    ports:
      - 80:80
    restart: always
    volumes:
      - ./wordpress:/var/www/html/

volumes:
  wordpress:
    driver: azure_file
    drivers_opts:
      share_name: wordpress
      storage_account_name: <storage-name>
```

Et pour l'utiliser avec la commande docker :  

```console
$ docker run -p 80:80 -v <storage-name>/wordpress:/var/www/html --name wordpress wordpress:latest
```
## Personnalisation de l'instance
Il est possible de modifier le nombre de CPU et de RAM de l'instance.  
```console
$ docker run --cpus 4 --memory 4G -p 80:80 -v <storage-name>/wordpress:/var/www/html --name wordpress wordpress:latest
```

## Commandes
Les commandes docker ne changent pas, mais il faut comprendre qu'on est dans un contexte Azure donc :

- Les conteneurs lancés ne dépendent pas de la machine locale, on peut faire un Ctrl+C après l'exécution d'une commande docker pour la laisser en arrière-plan  
- Les commandes `docker start` et `docker stop` fonctionnent dans le contexte azure  
- Docker gère les files share sous forme de volumes, donc `docker volume ls`, `docker volume rm` et `docker volume create` gère les files share du compte de stockage  
- Docker ACI ne supporte pas le mapping des ports, ça veut dire que l'option `-p` fonctionne en renseignant le même port sur l'host et dans le conteneur. `-p 8080:80` n'est pas possible, on doit utiliser `-p 80:80`  
- L'option `docker network` n'est pas disponible, seulement une ip publique est renseignée, ça veut dire qu'il faut passer par swarm ou kubernetes pour ajouter plusieurs conteneurs dans un même réseau  
- L'option `docker container` n'est pas disponible, on peut seulement afficher le conteneur avec `docker ps` et le gérer avec `docker stop` et `docker start`.  
- L'option `docker swarm` et `docker deploy` ne sont pas disponible, il faut utiliser une template ARM ou Bisep.  
