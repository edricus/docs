# Deployer un Nextcloud en HTTPS avec Azure
![nextcloud-azure](../src/nextcloud-azure.png){ width=550 }  
Il nous faut :  

 - 1 VM qui va heberger nextcloud  
 - 1 VM qui va heberger nginx proxy manager  
 - 1 Database qu'on va connection à nextcloud  

## 1 - Créer les 2 VMs
Les 2 doivent avoir des IP publique
Sur Debian 11

## 2 - Créer la DB
Utilisateur = "adminuser"

 - désactiver le ssl
 - autoriser l'accès au services azure OU ajouter une rêgle de pare-feu pour l'ip publique de votre vm
 - créer une db "nextcloud"

## 3 - Se connection à la VM nextcloud en ssh

 - se connecter la à la DB

```console
$ mariadb -h <nom-du-serveur>.mariadb.database.azure.com -u adminuser -p
```

 - créer l'utilisateur et la database

```sql
CREATE DATABASE nextcloud;
CREATE USER 'nextcloud'@'%' IDENTIFIED BY 'P@ssword2023';
GRANT ALL PRIVILEGES ON nextcloud . * TO 'nextcloud'@'%';
FLUSH PRIVILEGES;
quit;
```

utilisateur : nextcloud  
database : nextcloud  
mot de passe : P@ssword2023  

## 4 - Installer Nextcloud
On va utiliser docker-compose
```console
# apt install docker.io docker-compose -y
```
On va créer une docker-compose.yml
```yml
version: '2'

volumes:
  nextcloud:

services:
  app:
    image: nextcloud
    restart: always
    ports:
      - 80:80
      - 443:443
    volumes:
      - nextcloud:/var/www/html
    environment:
      - REDIS_HOST=redis
  redis:
    image: redis:alpine
    container_name: redis
    volumes:
      - /docker/nextcloud/redis:/data
    restart: unless-stopped
```
On va l'activer
```console
# docker-compose up -d
```
On va se rendre à l'url publique de notre VM
De là il faut créer l'utilisateur admin (au choix)
Pour la connection à la db :

 - Choisir "MariaDB/MySQL"
 - Utilisateur : nextcloud
 - Database : nextcloud
 - Mot de passe : P@ssword2023
 - Host : le nom d'hôte qu'on a utilisé dans la commande mariadb pour se connecter à la db, au complet, pas seulement le nom du serveur (le domaine)

## 5 - Nginx Proxy Manager
On se connecte en SSH sur notre autre VM
On installe docker-compose
```console
# apt install docker.io docker-compose vim -y
```
On va créer le docker-compose.yml
```yml
version: '3.8'
services:
  app:
    image: 'jc21/nginx-proxy-manager:latest'
    restart: unless-stopped
    ports:
      - '80:80' # Public HTTP Port
      - '443:443' # Public HTTPS Port
      - '81:81' # Admin Web Port

    volumes:
      - ./data:/data
      - ./letsencrypt:/etc/letsencrypt
```
On l'active
```console
# docker-compose up -d
```
On se rend sur l'ip de la VM avec le port 81  
Nom d'utilisateur : admin@example.com  
Mot de passe : changeme  

Arrivé sur l'interface on séléctionne "Hosts" -> "Add Proxy Host"
Details :  
 - Domain names : Il faut un nom de domaine (no-ip.com)  
 - Scheme : http  
 - Forward Hostname / IP : l'ip de notre machine nextcloud  
 - Foward Port : 80
SSL :  
le nom de domaine  
Force SSL - HTTP/2 Support - HSTS Enabled - HSTS Subdomains  

On sauvegarde et on accède à notre nextcloud avec le nom de domaine.
