![landing-zone.png](https://letmetechyou.com/wp-content/uploads/2023/04/azure-landing-zone-820x461.png)  
# Landing Zone
C'est un ensemble de bonnes pratiques Azure pour le deploiement du cloud chez un client.  
Elles comprends des policies qui vont modifier la création de ressources afin d'améliorer la sécurité de l'infra et être raccord avec les pricipes de deploiement de nouvelles infras en entreprise.  

## Deployer la template Terraform de base
Microsoft a mis à disposition des templates terraform  
[https://github.com/Azure/terraform-azurerm-caf-enterprise-scale/wiki/User-Guide](https://github.com/Azure/terraform-azurerm-caf-enterprise-scale/wiki/User-Guide)  
à droite dans "Exemples" il y a des templates sur plusieurs niveaux, de 100 à 400, qui correspondent à la complexité du deploiement.  
Le niveau le plus basique comporte ce .tf
```ruby
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 3.54.0"
    }
  }
}

provider "azurerm" {
  features {}
}

data "azurerm_client_config" "core" {}

module "enterprise_scale" {
  source  = "Azure/caf-enterprise-scale/azurerm"
  version = "<version>" 

  default_location = "<YOUR_LOCATION>"

  providers = {
    azurerm              = azurerm
    azurerm.connectivity = azurerm
    azurerm.management   = azurerm
  }

  root_parent_id = data.azurerm_client_config.core.tenant_id
  root_id        = "myorg"
  root_name      = "My Organization"
}
```
Les informations à completer sont :  
 
 - version (du module "entreprise_scale")  
On va aller sur la page de releases de terraform-azurerm-caf-entreprise-scale et choisir la dernière version.  
Ici, 4.2.0, donc version = =="=4.2.0"==  

- default_location  
En minuscule tout attaché, ici, ==westeurope==  

- root_id  
Ce serait l'id de notre managment group, pour faire propre on peut mettre un GUID généré aléatoirement  
Ici, ==61bf7fc6-aac4-4c21-9df8-62712e24c5bc==  

- root_name  
C'est le display name de notre managment group, au choix  
Ici, ==ESMG_EDRICUS==  

Et c'est tout bon, le fichier final :  
??? info "main.tf"
    ```ruby
    terraform {
      required_providers {
        azurerm = {
          source  = "hashicorp/azurerm"
          version = ">= 3.54.0"
        }
      }
    }
    
    provider "azurerm" {
      features {}
    }
    
    data "azurerm_client_config" "core" {}
    
    module "enterprise_scale" {
      source  = "Azure/caf-enterprise-scale/azurerm"
      version = "=4.2.0" 
    
      default_location = "westeurope"
    
      providers = {
        azurerm              = azurerm
        azurerm.connectivity = azurerm
        azurerm.management   = azurerm
      }
    
      root_parent_id = data.azurerm_client_config.core.tenant_id
      root_id        = "61bf7fc6-aac4-4c21-9df8-62712e24c5bc"
      root_name      = "ESMG_EDRICUS"
    }
    ```
```console
$ terraform plan
$ terraform apply
```
Le deploiement dure en moyenne 30 à 45min
