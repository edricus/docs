# Sites
## Anglais
News Linux (débutant)  
[9to5linux](https://9to5linux.com/)  
[Linuxize](https://linuxize.com/)  
[Omg!ubuntu](https://www.omgubuntu.co.uk/)  
[It's Foss](https://itsfoss.com/)  
News Linux (avancé)
[LinuxFoundation](https://www.linuxfoundation.org/press)  
[Phoronix](https://www.phoronix.com/)  
## France
[Korben](https://korben.info/)  
[Linuxtricks](https://www.linuxtricks.fr)  
