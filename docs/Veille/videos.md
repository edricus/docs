# Videos
## Informatique
### Anglais
[ChrisTitusTech](https://youtube.com/c/ChrisTitusTech)  
Astuces Windows et Linux.  
Titus est l'auteur d'un excellent script de debloat Windows[.](https://www.christitus.com/debloat-windows-10-2020/)  
[TechHut](https://www.youtube.com/c/TechHutHD/)  
News linux et Windows  
[The Linux Experiment](https://www.youtube.com/c/TheLinuxExperiment/videos)  
News linux  
[DistroTube](https://youtube.com/c/DistroTube)  
Astuces, news linux et questions en tout genre autour du libre et open-source  
[LearnLinuxTV](https://youtube.com/c/LearnLinuxtv)  
Apprendre les bases de Linux  
[Luke Smith](https://www.youtube.com/c/LukeSmithxyz/)  
Libriste assez hardcore qui parle de l'actualitée autour de libre et open-source et de sujets comme les cryptos avec beaucoup de cynisme.  
[Wolfgang's Channel](https://www.youtube.com/c/WolfgangsChannel)  
Passioné par le self-hosting et le hacking de Thinkpads autour des logiciels libre  
[Techlore](https://www.youtube.com/c/Techlore)  
Specialisé dans la vie privée et la sécurité  
[The Hated One](https://www.youtube.com/c/TheHatedOne)  
Aussi spécialisé dans la vie privée mais avec une ambiance edgy hacker, un peu trop extrême parfois, à prendre avec des pincettes.  
### Français
[Xavki](https://youtube.com/c/xavki-linux)  
ENORME contenu linux et formations en tout genre autour du devops  
[Adrien LinuxTricks](https://youtube.com/c/AdrienLinuxtricks)  
Spécialisé dans les distributions Linux, news et astuces  
[Cookie Connecté](https://youtube.com/c/Cookieconnect%C3%A9)  
Vulgarisation autour des themes du réseau et du devops  
[MiCode Enquêtes](https://youtube.com/c/Micode-Enqu%C3%AAtes)  
Vulgarisation autour de la cybersécuritée  
[Underscore](https://youtube.com/c/UnderscoreTalk)  
Talkshow avec MiCode autour d'anectodes et de news sur l'informatique en général  
[Cocadmin](https://www.youtube.com/c/cocadmin)  
Excellente chaine de vulgarisation adminsys/devops, à fond dans le story telling  
[Scilabus](https://www.youtube.com/c/scilabus)  
Chaine de science, en rapport avec l'avancée technologique en général  


