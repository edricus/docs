<figure markdown>
[![fedora](../../src/fedora.svg){ width="300" }](https://getfedora.org/)
</figure>
# Fedora
est une distribution basée sur RedHat Entreprise Linux (RHEL).  
Elle est gérée par sa communauté et axée sur le développement de nouvelles technologies.  

| Type | Stable |
| :- | :- |
| Version | Unique |
| Cycle de sortie | Tout les 6 mois |
| Support de version | 13 mois |

!!! info "Fedora et RHEL" 
    Fedora est liée à RHEL, les nouvelles versions des paquets issus de cette distribution sont plus tard intégrées à RHEL après avoir été stabilisées (à savoir testés et corrigés)

## Téléchargement
Fedora est disponible à partir d'un DVD sur la page principale du site, mais le mieux est de se procurer l'image "Everything" présente [à partir de ce lien](https://alt.fedoraproject.org).  
Cette image est une "netinstall" et permet de personnaliser l'installation en plus d'être la plus légère.  
!!! warning "Une connexion internet est nécessaire pour la netinstall."
## Gestion des paquets
Fedora utilise ==DNF== (Dandified Yum), c'est le successeur de YUM.

| Action | DNF | APT |
| :--- | :--- | :--- |
| Installer un paquet | dnf install | apt install |
| Supprimer un paquet | dnf remove | apt remove |
| Mettre à jour les paquets | dnf upgrade | apt upgrade |
| Supprimer les paquets en cache | dnf clean all | apt clean |
| Supprimer les dépendances inutiles | dnf autoremove | apt autoremove |

## Supprimer les anciens kernels  
Le cycle de mise de jour de Fedora étant court, il est nécessaire de configurer DNF pour ne pas garder les kernels éternellement.  
On garde donc seulement 2 versions du kernel, ce qui est largement suffisant
```console
# dnf remove $(dnf repoquery --installonly --latest-limit=-2 -q)
```
```title="/etc/dnf/dnf.conf"
installonly_limit=2
```
