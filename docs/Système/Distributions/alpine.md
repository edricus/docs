<figure markdown>
[![alpine](../../src/alpine.png){ width="300" }](https://alpinelinux.org/)
</figure>
# Alpine
est une distribution ultralégère utilisée pour les serveurs.  
Elle est un choix de référence pour les images Docker puisque l'image ne pèse que ~5Mo contre ~100Mo pour une Debian ou Ubuntu.  

<table cellspacing="0" border="0">
  <colgroup span="4" width="160"></colgroup>
    <tr>
    	<td align="left">Type</td>
	<td colspan=2 align="center">Stable</td>
	<td align="center">Rolling</td>
    </tr>
    <tr>
	<td align="left">Versions</td>
	<td align="center">main</td>
	<td align="center">community</td>
	<td align="center">testing</td>
    </tr>
    <tr>
	<td align="left">Cycle de sortie</td>
	<td colspan=2 align="center">Tout les 6 mois</td>
	<td align="center">N/A</td>
	</tr>
    <tr>
	<td align="left">Support de version</td>
	<td colspan=2 align="center">2 ans</td>
	<td align="center">N/A</td>
    </tr>
</table>
  
!!! info "C'est l'une des rares distributions à fournir que très peu d'outils GNU, son utilisation en dehors de Docker est donc pénible et peu pertinente." 
!!! quote "J'ai très peu d'expérience avec cette distrib, je l'ai utilisée dans Virtualbox, ce qui n'est pas l'approche souhaitée pour apprendre sa prise en main (autant apprendre à l'utiliser avec Docker)."  
!!! bug "Sur Virtualbox le réseau bridge ne fonctionnera pas"
## Install
Au cas où l'on veut installer alpine en tant qu'OS, l'ISO dispose d'un script d'installation interactif.
```console
# setup-alpine
```
!!! info "Préférer openntpd pour le NTP"

```console
# reboot
```
## Gestion des paquets  
Alpine utilise ==APK== pour gérer les paquets  
### Syntaxe 
Son fonctionnement et sa syntaxe sont très similaire à APT.

| APK | APT | Description |
| :-: | :-: | :-: |
| apk add <paquet\> | apt install <paquet\>| Installer un paquet |
| apk del <paquet\> | apt remove <paquet\> | Supprimer un paquet |
| apk update | apt update | Mettre à jour la liste des paquets |
| apk upgrade | apt upgrade | Mettre à jour les paquets |
| apk search | apt search | Rechercher un paquet |
| apk info | apt list --installed | Lister les paquets installés |
    
## Post-Installation
On fait les mises à jour et on installe les paquets de base pour une utilisation classique.
```console
# pkg update
# pkg add coreutils doas mandoc man-pages shadow util-linux git curl wget sed dialog vim
$ export PAGER=less
```
## Gestion des utilisateurs
| Action | Commande |
| :--- | :--- |    
| Ajouter un utilisateur | adduser <user\> |
| Supprimer un utilisateur | deluser <user\> |
| Ajouter un utilisateur à un groupe | addgroup <user\> <group\> |

## Permissions root à un utilisateur avec doas
[Doas](https://wiki.gentoo.org/wiki/Doas) est une alternative minimaliste à sudo. Il a du sens dans cette distribution ultra légère bien que sudo reste toujours plus sécurisé.  
Le principe est d'utiliser le groupe ==wheel== pour donner aux utilisateurs les permissions root.

Ajouter un utilisateur au groupe wheel
```console
# adduser <user> wheel
```
Autoriser le groupe wheel à exécuter des commandes root  
```title="/etc/doas.d/doas.conf"
permit persist :wheel
```
Exécuter une commande en tant que root
```console
$ doas <command>
```
??? info "Ajouter un alias vers sudo est une TRÈS bonne idée, la procédure est détaillée plus bas, [lien](alpine.md#bash)"
    Un alias est une commande qu'on définit nous même renvoyant à une autre commande
### Dépôts
Il faut activer le dépôt communautaire pour disposer de plus de paquets
```title="/etc/apk/repositories"
décocher la ligne community
```
```console    
# apk update
```

## Bash
Changer d'interpréteur (sh est celui par défaut et est un peu rudimentaire)
```console
# apk add bash bash-completion
# chsh
```
Alias utiles
```title="~/.bashrc"
alias sudo='doas'
alias ll='ls -l'
```
Indiquer à bash le fichier de configuration
```title="~/.profile"
source .bashrc
```


