<figure markdown>
[![debian](../../src/debian.png){ width="300" }](https://www.debian.org/)
</figure>
# Debian
est une distribution communautaire promouvant les logiciels libres.  
Destinée plutôt à l'usage de serveurs, elle est parfaitement utilisable en version desktop pour les utilisateurs avancés sachant se débrouiller avec l'ajout de pilotes sous linux.  
Elle est gérée par une communauté très rigoureuse.  

Son grand axe est la ==stabilité== au coût des fonctionnalités ; ainsi, il ne faut pas s'attendre à disposer des dernières nouveautés avec cette distribution à moins de basculer sur la branche "unstable" ce qui n'est pas recommandé puisque qu'il s'agit plus d'un environnement de développement (de Debian) plutôt qu'une branche pour les tâches quotidiennes.  
<table cellspacing="0" border="0">
 <colgroup width="160"></colgroup>
 <colgroup width="130"></colgroup>
 <tr>
  <td align="left">Type</td>
  <td align="left">Stable</td>
 </tr>
 <tr>
  <td align="left">Versions</td>
  <td align="left">Unique</td>
 </tr>
 <tr>
  <td align="left">Cycle de sortie</td>
  <td align="left">Tout les 2 ans</td>
 </tr>
 <tr>
  <td align="left">Support de version</td>
  <td align="left">5 ans</td>
 </tr>
</table>
## Téléchargement  
Les 2 variantes les plus importantes :  

  - L'installateur simple en netinstall : [Lien](https://www.debian.org/distrib/netinst){ target=about:_blank }  
  en dessous de "Small CDs or USB sticks" -> amd64  
  - Le live pour essayer Debian avant de l'installer : [Lien](https://www.debian.org/distrib/){ target=about:_blank }  
  en dessous de "Try Debian live before installing" -> Live Gnome  

??? note "Debian 11 et anterieur"
    Il existait énormément de versions de l'ISO de Debian  
    Il était necessaire de télécharger une version "non-free" pour installer Debian avec des pilotes propriétaires.  
    La version qui nous intéresse est la ==net-install== en variante ==non-free==.  
    ==net-install== : est un type d'installation consistant à télécharger les composants lors de l'installation afin de réduire la taille de l'image ISO.    
    ==non-free== : permet de disposer du dépôt propriétaire dès l'installation, plus de détails dans la rubrique "Dépôts".  
    L'ISO se nomme ==firmware-[version]-amd64-netinst.iso==  
    ??? warning "Ne pas prendre la version edu !"  
        La version edu, skolelinux, ou debian school est une version de Debian destinée à un usage éducatif.  
        Elle comprend des outils pédagogiques et des services préconfigurés pour un déploiement facile.  
        Une installation Debian se veut minimale, cette version ne convient pas à un usage personnel.
    
    [Lien vers le répertoire d'ISO de Debian](https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/amd64/iso-cd/)  
    ??? note "Méthode de recherche pour accéder à cette page"  
        Le site de Debian étant incommode, la méthode la plus simple pour arriver au répertoire est de passer par Google avec la recherche "debian non-free".  
        Arrivé à l'arborescence, il faudra naviguer comme suit :
        ```
        📁 current/  
        └─ 📁 amd64/ 
           └─ 📁 iso-cd/
              └─ 💿 firmware-...-amd64-netinst.iso  
        ```

## Dépôts

3 dépôts sont disponibles

| | Base | Communautaire |
| :- | :-: | :-: |
| **Open Source** | main | contrib |
| **Propriétaire** | | non-free<br />non-free-firmware |

==main== : Paquets conformes au DFSG ne contenant pas de dépendances au dépôt non-free  
==contrib== : Paquets conformes au DFSG contenant des dépendances au dépôt non-free  
==non-free== : Paquets non conformes au DFSG  
==non-free-firmware== : Paquets des drivers non conformes au DFSG 

!!! info "Définition du DFSG"
    Le DFSG ou "Debian Free Software Guidelines" sont les principes du logiciel libre selon Debian.  
    Ils définissent précisément ce que Debian entend par "logiciel libre".  
    [La description complète de ces principes peut être retrouvée sur la page Wikipedia](https://fr.wikipedia.org/wiki/Principes_du_logiciel_libre_selon_Debian)

??? note "Debian 11 et anterieur"
    3 dépôts sont disponibles
    
    | | Base | Communautaire |
    | :- | :-: | :-: |
    | **Open Source** | main | contrib |
    | **Propriétaire** | | non-free |
    
    ==main== : Paquets conformes au DFSG ne contenant pas de dépendances au dépôt non-free  
    ==contrib== : Paquets conformes au DFSG contenant des dépendances au dépôt non-free  
    ==non-free== : Paquets non conformes au DFSG  


#### Configuration des dépôts  
Ce fichier est configuré automatiquement à l'installation, si Debian a été installée avec l'ISO standard, il est nécessaire d'ajouter le dépôt "non-free" si l'on souhaite disposer de logiciels propriétaires comme les pilotes.  

```title="/etc/apt/sources.list"
deb http://deb.debian.org/debian/ bullseye main contrib non-free non-free-firmware
deb http://deb.debian.org/debian/ bullseye-updates main contrib non-free non-free-firmware
deb http://security.debian.org/debian-security bullseye-security main contrib non-free non-free-firmware
deb http://deb.debian.org/debian/ bullseye-backports main contrib non-free non-free-firmware
```
??? question "Description détaillée"
    ##### Détails  
    Exemple de ligne :  
    `deb http://deb.debian.org/debian/ bullseye main contrib non-free`  
    ==http://== : l'URL du dépôt où se trouvent les archives    
    ==bullseye== : le nom de code de la distribution  
    ==main contrib non-free== : [vu plus haut](#Dépôts)  
    [Plus d'informations sur les noms de code de Debian](https://wiki.debian.org/DebianReleases)  
    ##### Explications  
    Il y a 4 dépôts détaillés dans ce fichier.  
    Le **premier** renvoie aux paquets, ce sont les binaires de logiciels.  
    Le **deuxième** renvoie aux mises à jour, elles corrigent les bugs uniquement, elle ne s'occupe pas des failles.  
    Le **troisième** renvoie aux mises à jour de sécurité, elles corrigent les failles uniquement, elle ne s'occupe pas des bugs.  
    Le **quatrième** renvoie vers des versions de logiciels supérieures à celles de la version courante. Ce dépôt n'est pas prioritaire et sert à sélectionner manuellement des logiciels pour les mettre à jour. Ça peut s'avérer utile dans certains cas, mais il faut garder en tête que peu de logiciels sont disponibles dans ce dépôt, ils sont choisis rigoureusement pour ne pas causer de problèmes.

#### Cycle de sortie
Bien qu'une version de Debian sorte tous les 2 ans, la distribution est en perpétuel développement.  

- Unstable  

Les paquets sont constamment mis à jour dans la branche =="unstable"== de Debian (aussi appelée Sid).  
Arrivés dans la unstable, ils sont examinés à la recherche de **bugs critiques** (bugs cassant des paquets ou tout le système).  
Après 2,5 ou 10 jours, et s'ils ne contiennent plus ou moins de bugs critiques que dans la version ==testing==, ils peuvent rentrer dans cette dernière.  

- Testing

Arrivés dans la testing, les mainteneurs doivent régler les problèmes plus mineurs, afin d'arriver à une version ==stable== du paquet.  
Mais la version stable arrivant tous les 2 ans, et la branche unstable étant mis à jour toutes les ==6 heures==, les mainteneurs mettent à jour ces paquets vers de nouvelles versions, eux même contenant des bugs, et le cycle continue... jusqu'au ==freeze==.  

- Stable  

Le freeze est une période ou les mises à jour des paquets de la version testing s'arrêtent, et les mainteneurs doivent corriger les derniers bugs.  
Cette étape a une durée ==indéterminée==, elle peut être d'une durée d'un mois, comme 6 mois !  
Arrivé à une version satisfaisante, la distribution testing est renommée "stable", et le cycle se répète.  
Une version stable n'est pas pour autant dépourvue de mises à jour. Elle peut disposer de ==révisions==, comme des patchs de sécurité ou des corrections de bugs mineurs.

![debian-repos](../../src/debian-repos.svg){ width="600" }

#### Backports et FastTrack
Ce sont des dépôts supplémentaires visant à inclure des versions de paquets d'une branche instable vers la branche stable.  
Ces paquets doivent répondre à la condition stricte de dépendre des versions présentes dans la version stable.  
Le dépot ==backports== va inclure des packets de ==testing==.  
Le dépot ==FastTrack== va inclure des packets de ==unstable==.  
Du fait de la nature instable de ces paquets, peu d'entre eux sont disponibles, mais demeurent très utiles dans le cas où une version inutilisable d'une application arriverait en stable. C'est probable quand du contenu distant est chargé dans une application et que les développeurs ont oubliés que certains utilisateurs ne sont pas à jour en permanence...  

### Syntaxe d'APT 
| Action | Commande |
| :- | :- |
| Installer un paquet | `apt install <paquet>` |
| Supprimer un paquet | `apt remove <paquet>` |  
| Mettre à jour la liste des paquets | `apt update` | 
| Mettre à jour les paquets | `apt upgrade` | 
| Supprimer les paquets en cache | `apt clean` |
| Supprimer les dépendances inutiles | `apt autoremove` |

??? info "Commandes complexes"  
    Supprimer un paquet, ses dépendances et sa configuration
    ```console 
    # apt purge --autoremove <paquet>
    ```
    Installer manuellement un fichier .deb
    ```console
    # dpkg -i <paquet>.deb
    ```
    Rétrograder un paquet
    ```console
    $ apt-cache showpkg <paquet>
    # apt install <paquet>=<version>
    ```
### Problèmes
un memo de la resolution de differents problèmes lié à debian

```
Gave up waiting for suspend/resume device
```
Occasioné par le service "resume" de initramfs, le problème survient après l'installation d'un dualboot linux.  
La partition swap est formatée donc change d'UUID.  
Resolution : determiner l'UUID de la partition swap avec ```blkid``` et changer les fichiers ```/etc/fstab``` et ```/etc/initramfs.d/conf.d/resume```.  
En général on ne formatera pas la partition swap.
