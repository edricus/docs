<figure markdown>
[![ubuntu-logo](../../src/ubuntu.png){ width="300" }](https://ubuntu.com)
</figure>
# Ubuntu
est une distribution basée sur Debian.  
Elle est gérée par la société Canonical et axée sur la facilité d'utilisation pour le grand public, plus précisément l'utilisation ==desktop== de Linux.  
Contrairement à Debian, Ubuntu n'est pas destinée à être utilisé en tant que serveur et facilite l'installation de logiciels ==propriétaires==.  
## Cycle de vie
<table cellspacing="0" border="0">
	<colgroup width="160"></colgroup>
	<colgroup width="180"></colgroup>
	<colgroup width="150"></colgroup>
	<tr>
		<td align="left">Type</td>
		<td colspan=2 align="center">Stable</td>
		</tr>
	<tr>
		<td align="left">Versions</td>
		<td align="left">LTS</td>
		<td align="left">Intermédiaire</td>
	</tr>
	<tr>
		<td align="left">Cycle de sortie</td>
		<td align="left">Tout les 2 ans (avril)</td>
		<td align="left">Tout les 6 mois</td>
	</tr>
	<tr>
		<td align="left">Support de version</td>
		<td align="left">5 ans + 5 ans (payant)</td>
		<td align="left">9 mois</td>
	</tr>
</table>  

## Téléchargement
Le téléchargement est disponible depuis l'onglet "Download" du site [ubuntu.com](https://ubuntu.com)  
??? question "Intermédiaire ou LTS ?"  
    Il est fortement recommandé de choisir la version LTS pour sa nature stable.  
    La version intermédiaire doit être choisie selon un besoin spécifique, par exemple un matériel récent qui demande des pilotes pas encore disponibles sur la version LTS ou encore un besoin vital d'avoir une application à jour avec les dernières fonctionnalités.  
    À noter que les mises à jour de sécurité sont disponibles dans la LTS, pas besoin d'une intermédiaire si l'on attend la correction d'une faille.  

## Gestion des paquets
Ubuntu utilise actuellement ==APT== (Advanced Packet Manager).  
Canonical veut à terme le remplacer par ==snap==, un gestionnaire de paquets exploitant la conteneurisation.  

??? quote "Le drama snap" 
    
    Certains paquets s'installent automatiquement avec snap même lorsqu'on souhaite passer par apt, [ce qui en fait un aspect controversé de la distribution](https://ubuntu-mate.community/t/poll-opinion-on-snaps-and-if-you-would-like-to-see-them-as-default/22848) menant certains à migrer Linux Mint, une distribution basée sur Ubuntu qui à ce jour [ne souhaite pas utiliser snap par défaut](https://blog.linuxmint.com/?p=3906).  
    Néanmoins, les utilisateurs ne sont pas si hostiles que ça au remplacement de apt par snap, la controverse est dûe uniquement à la mauvaise optimisation de snap en général [comme le montre ce sondage](https://twitter.com/ubuntufriends/status/1449518297469407233).  
    À l'heure actuelle, ce que l'on appelle une mauvaise optimisation est la lenteur, la taille des paquets et leur intégration dans l'environnement graphique (une application snap peut utiliser un thème clair alors que l'on utilise un thème sombre).  

### Dépôts
Comme Debian, Ubuntu sépare les dépôts open-source et propriétaire. Elle dispose aussi d'un dépôt communautaire.  

| | Base | Communautaire  |
| :- | :-: | :-: |
| **Open source** | main | universe |
| **Propriétaire** | restricted | multiverse |

#### Configuration des dépôts
Pas besoin de faire des modifications dans les dépôts après l'installation, cet exemple est là à titre indicatif de la structure du fichier `/etc/apt/sources.list` d'Ubuntu.  
Additionnellement, c'est une version compacte de ce fichier qui présente normalement beaucoup plus de lignes commentées.
```title="/etc/apt/sources.list"
deb http://fr.archive.ubuntu.com/ubuntu/ focal main restricted universe multiverse
deb http://fr.archive.ubuntu.com/ubuntu/ focal-updates main restricted universe multiverse
deb http://security.ubuntu.com/ubuntu focal-security main restricted universe multiverse
deb http://fr.archive.ubuntu.com/ubuntu/ focal-backports main restricted universe multiverse
```
??? question "Description détaillée"
    ##### Détails
    ==http://== : l'url du dépôt, elle est choisie en fonction de la localisation (fr=France)  
    ==focal-updates== : le nom de code de la version d'Ubuntu suivie de l'emplacement des paquets  
    ==main restricted universe multiverse== : [vu plus haut](#Dépôts)  
    [Plus d'information sur les noms de code d'Ubuntu](https://wiki.ubuntu.com/DevelopmentCodeNames)  
    ##### Explication :
    Il y a 4 dépôts détaillés dans ce fichier.  
    Le **premier** renvoie aux paquets de base, ce sont les binaires de logiciels  
    Le **deuxième** renvoie aux mises à jour, elles corrigent les bugs uniquement, elle ne s'occupe pas des failles  
    Le **troisième** renvoie aux mises à jour de sécurité, elles corrigent les failles uniquement, elle ne s'occupe pas des bugs.  
    Le **quatrième** renvoie vers des versions de logiciels supérieures à celles de la version courante. Ce dépôt n'est pas prioritaire et sert à sélectionner manuellement des logiciels pour les mettre à jour. Cela peut s'avérer utile dans certains cas, mais il faut garder en tête que peu de logiciels sont disponibles dans ce dépôt, ils sont choisis rigoureusement pour ne pas causer de problèmes.  

### Syntaxe d'APT & Snap
| Action | APT | Snap |
| :- | :- | :- |
| Installer un paquet | apt install | snap install |
| Supprimer un paquet | apt remove | snap remove |
| Mettre à jour la liste des paquets | apt update | N/A |
| Mettre à jour les paquets | apt upgrade | Automatique (4 fois par jour)<br />Manuellement : `snap refresh`|
| Supprimer les paquets en cache | apt clean | `rm -rf /var/lib/snapd/cache/*` |
| Supprimer les dépendances inutiles | apt autoremove | On peut pas/il faut un script compliqué |  

??? info "Commandes complexes"
    Rétrograder un paquet
    === "APT" 
        ```console title="Chercher une version à installer"
        $ apt-cache showpkg <paquet>
        ```
        ```console title="Installer la version du paquet"
        # apt install <paquet>=<version>
        ```
    === "Snap"
        ```console title="Chercher une version à installer"
        $ snap list --all <paquet>
        ```
        ```console title="Installer la version du paquet"
        # snap revert <paquet> --revision <n°>
        ```  
### Supprimer snap
!!! warning "Avertissement"
    La suppression (et le blocage) de snap rend impossible l'installation des paquets snap imposés par Ubuntu. Pour installer ces derniers (ex: chromium) il faudra trouver un moyen détourné comme télécharger un .deb sur un site ou trouver un dépôt pour ce logiciel en particulier."  

```console title="Supprimer le paquet snap et ses dépendances"
# apt purge --autoremove snapd
```  

```console title="Supprimer les dossiers config et cache de snap"
$ rm -rf ~/snap
# rm -rf /var/cache/snapd
```
```title="/etc/apt/preferences.d/nosnap.pref"
# Règle pour interdire l'installation des paquets snap
Package: snapd
Pin: release a=*
Pin-Priority: -10
```


