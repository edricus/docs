![syncthing](../src/syncthing.png){ align=left width=190 }
# Syncthing
est un des logiciels de synchronisation de fichiers auto-hébergeable les plus populaire.  
Il est disponible sur Windows, Linux et MacOS sous forme d'application bureau ou client léger.  

## Installation
Son utilisation avec Docker est un peu exotique donc on va plutôt l'installer à l'aide de son paquet APT : 
```console
# apt-get install syncthing -y
``` 

## Configuration
Syncthing s'exécute exclusivement en tant qu'utilisateur, pour le démarrer on utilise le service en tant qu'utilisateur :
```console
# systemctl start syncthing@<user>.service
```
Où <user> est l'utilisateur avec lequel on veut l'utiliser

### Ports
Syncthing utilise les ports suivants : 

  - **8384/TCP**    : GUI (serveur web, obligatoire)  
  - **22000/TCP**   : Synchronisation basée sur TCP (recommandé)  
  - **22000/UDP**   : Synchronisation basée sur QUIC (facultatif)  
  - **21027/UDP**   : Utilisé pour la découverte automatique par broadcast (facultatif)  

### Autoriser l'accès à distance
Par défaut le GUI est accessible seulement depuis localhost.  
Après un premier lancement il faut modifier son fichier config :
```console
$ vim ~/.config/syncthing/config.xml
```
et remplacer "127.0.0.1:8384" par "0.0.0.0:8384"
```console
# systemctl restart syncthing@<user>.service
```

### Configurer le comportement de Syncthing
Arrivé sur l'interface on va dans "Actions" > "Settings" en haut à droite.  
![syncthing-1](../src/syncthing-1.png){ width=250 }  

  - Onglet "GUI" on va changer le l'==utilisateur== et le ==mot de passe==  
  - Onglet "Connections" :
    - **Sync Protocol Listen Addresses** : "tcp://0.0.0.0:22000"  
    - Décocher "NAT traversal", "Local Discovery", "Global Discovery" et "Relaying" (sauf si necessaire)  
    - Configurer la limite upload/download au besoin (ℹ️  voir [Activer les limites sur le LAN](#activer-les-limites-sur-le-lan))  

## Synchronisation
Petit résumé :  

  - On installe déjà Syncthing sur tous les appareils  
  - On ajoute des dossiers à synchroniser sur l'appareil distant qui a les fichiers déjà présents / les plus récents en premier (le master)  
  - On connecte ensuite l'appareil distant qui a le moins de fichiers / les plus obsolètes (le slave) depuis l'interface du master.  
  - On revient sur l'interface du slave et on connecte l'appareil distant master  
  - Dès que la connection est faite le slave va recevoir des demandes de synchronisation, de là on défini le dossier qui va recueillir les fichiers.  

### Ajouter un appareil distant
Après avoir installé et fait les paramètrages de base sur l'appareil distant, on doit afficher ses informations d'identification :  

![syncthing-2](../src/syncthing-2.png){ width=500 }  
![syncthing-3](../src/syncthing-3.png){ width=500 }  

De retour sur l'appareil local (le master) on le rajoute :  

![syncthing-4](../src/syncthing-4.png){ width=500 }  

#### General

  - Device ID 
  coller le code d'identification copié précedement  
  - Device name  
  n'importe quel nom pour identifier l'appareil  

#### Sharing
  
  - Cocher les partages dans la rubrique "Unshared folders" s'il sont déjà créés  
  Ils peuvent être ajouter à posteriori  

#### Advanced

  - Addresses  
  changer "dynamic" en "tcp://<ip-de-lappareil-distant>"  

!!! info "L'IP peut être en local si le partage ce fait sur le réseau local mais doit être publique si on fait un partage sur un réseau distant, il faudra ouvrir le port 22000 sur la box de l'appareil distant dans ce cas"  

### Créer un partage  
Sur l'appareil avec les modifications les plus récentes (master) :  

![syncthing-5](../src/syncthing-5.png){ width=250 }  

#### General

  - Folder label  
  n'importe quel nom (c'est celui qui va s'afficher dans l'interface)  
  - Folder path  
  la source du dossier, là on sont contenus les fichiers  

#### Sharing 
  
  - Unshared devices  
  cocher les appareils distants à synchroniser  

#### File Versionning  

  - En fonction des options, laisser les anciens fichiers pendant un certain temps, pour des fichiers peu volumineux et critique, il est recommandé d'activer cette option  

#### Advanced  

 - Scanning  
 laisser cocher pour que Syncthing déclanche une synchronisation quand il détecte un changement  
 - Folder Type  
 très important, c'est ici qu'on défini vraiment qui est le "master" et le "slave"  
   - Send & Receive  
   le document le plus récent l'emporte, et la copie écrase l'ancien fichier (sauf si on utilise le file versionning)  
   - Send Only  
   le comportement d'un master  
   - Receive Only : le comportement d'un slave  

### Recevoir un partage
Sur l'appareil avec les modifications les plus anciennes (slave) :  

### Activer les limites sur le LAN
En haut à droite :  

  - Actions - Advanced  
  - Options - Limit Bandwidth in Lan (cocher)  


