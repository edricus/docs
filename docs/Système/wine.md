<figure markdown>
[![winehq](../src/wine.png){ width="200" }](https://www.winehq.org/)
</figure>
# Wine
pour **Wine Is Not An Emulator** est un logiciel libre permetant l'execution de binaires Windows (.exe) sur Linux.  
Il fourni une couche de compatibilité pour l'environnement d'exécution de Windows.  
Cette couche traduit les appels de l'API Windows en appels vers l'API Unix et offre des alternatives aux librairies de Windows avec **wineserver**.  
#### Winetricks
est une application permetant de faciliter l'ajout de dépendances Windows dans l'environnement de Wine.  
Il fourni beaucoup de scripts pour contourner les erreurs pouvant faire échouer ces installations.  
Winetricks est **indispensable** la majeure partie du temps.
## Lexique
- ==Prefixe==  
Un prefixe Wine est un répertoire contenant toute la configuration et les logiciels installés dans Wine.  
On peut en créer plusieurs.  
Le préfixe par defaut est `~/.wine`.  
On utilise un préfixe en spécifiant la variable d'environnement `WINEPREFIX=<préfixe>` avec une commande wine.  
Un préfixe est créé au moment de l'execution d'une commande avec la variable d'environnement s'il n'existe pas.  
Généralement on utilise le préfixe par defaut et on en créé un quand on souhaite faire fonctionner plusieurs logiciels avec des dépendances incompatibles entre elles.
- ==Wine32==  
Généralement on execute wine en **32bit** pour des raisons de compatibilité.  
Ce n'est pas le choix par défaut il faut alors **toujours** placer la variable d'environnement `WINEARCH=win32` devant une commande.  
Cette commande peut être ommise si on a vraiment besoin d'executer le programme en 64bits mais il faut toujours garder en tête que ==la compatibilité passe avant la performance==. 
### Installation (Debian)
!!! warning "Sur Debian il n'est pas recommandé d'installer winetricks depuis les dépots car les scripts sont constament mises à jour et les derniers patchs sont necessaires pour mener à bien de nombreuses installations"
#### Dépots officiels
```console
# apt install wine winetricks
```
#### Dernière version
```console title="Wine"
# dpkg --add-architecture i386
# wget -nc https://dl.winehq.org/wine-builds/winehq.key
# mv winehq.key /usr/share/keyrings/winehq-archive.key
# wget -nc https://dl.winehq.org/wine-builds/debian/dists/bullseye/winehq-bullseye.sources
# mv winehq-bullseye.sources /etc/apt/sources.list.d/
# apt update
# apt install --install-recommends winehq-stable
```
```console title="Winetricks"
$ wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
$ chmod u+x winetricks
$ mv winetricks ~/.local/bin/
$ winetricks --self-update
``` 
!!! warning "Winetricks ne se met pas à jour automatiquement, la commande `winetricks --self-update` est à executer de temps en temps, par exemple si un composant Windows ne s'installe pas correctement (typiquement une application qui ne trouve pas le .dll d'un composant déjà installée)"  
## Configuration
La configuration de Wine dépend de la l'application qu'on souhaite installer.  
#### Winecfg  
est un outil avec une interface graphique qui permet de configurer un préfixe.  
Elle permet de configurer :  

- La version de Windows (utiliser Windows 7 au lieu de Windows XP si possible)  
- Les libraries (DLL). Wine fournit des librairies mais le choix est limité.  
Les plus utiles (.NET et Visual C++) ne sont pas présentes.  
Cet onglet est important à vérifier quand on installe manuellement des librairies déjà présentes dans wine, il faut choisir "Native (Windows)" sinon wine pourra utiliser sa version qui n'est pas celle désirée.  
- L'interface  
Permet de régler l'apparence des applications Windows.  
Il est parfois necessaire de régler la police d'ecriture et le DPI, elle peut être minuscule selon les applications.  
On peut aussi modifier la décoration des fenètres pour la rendre moins archaïque.  

Les autres options de Winecfg ne sont pas très utilisés  
Pour executer winecfg :
```console
$ WINEARCH=win32 winecfg
```
#### Winetricks
s'utilise à la fois en ligne de commande et en interface graphique.  
Pour executer des scripts d'installation en ligne de commande, on les renseigne sans arguments.  
Par exemple
```console
$ winetricks dotnet35 vcruntime2008
```
Installera .NET 3.5 et Visual C++ 2008.  
Pour obtenir la liste des composants installable on execute la commande
```console
$ winetricks dlls list
```
On peut alors faire un `grep` pour chercher dans cette liste
```console
$ winetricks ddls list | grep '.NET 3.5'
```
```console
dotnet35                 MS .NET 3.5 (Microsoft, 2007) [downloadable,cached]
dotnet35sp1              MS .NET 3.5 SP1 (Microsoft, 2008) [downloadable,cached]
```
Sans arguments Winetricks s'executera graphiquement.  
Pour installer un composant Windows avec cette méthode :
![winetricks1.png](../src/winetricks1.png){ width="550" }
![winetricks2.png](../src/winetricks2.png){ width="550" }
![winetricks3.png](../src/winetricks3.png){ width="550" }

## Lutris
est un logiciel avec une interface graphique fournissant des scripts (rédigés par la communautée) configurant automatiquement un environnement Wine pour des applications (et jeux) Windows spécifiques.  
Determiner les composants necessaires à une application étant parfois assez compliquée, Lutris est une bonne solution face aux applications necessitant beaucoup de dépendances à des logiciels comme **.NET** ou **Visual C++** ou autres librairies pas incluses par defaut dans Wine.  
### Installation (Debian)
L'option `--install-recommends` installera des librairies Wine dont Lutris peut avoir besoin.  
Pour une liste complète ces dépendances sont disponible [sur la page github de Lutris](https://github.com/lutris/docs/blob/master/WineDependencies.md)
```console
# apt install --install-recommends lutris
```

