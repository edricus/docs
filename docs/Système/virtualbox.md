<figure markdown>
[![virtualbox.svg](../src/virtualbox.svg){ width="400" }](https://www.virtualbox.org/)
</figure>
# Virtualbox
est un logiciel de virtualisation, c'est-à-dire un logiciel capable d'exécuter un système d'exploitation virtuellement sur un ordinateur.  
C'est un hyperviseur de type 2.

## Hyperviseurs  
est le nom donné aux logiciels de virtualisation. Il en existe 2 types :  

- **Type 1**  
Le logiciel s'exécute directement sur le matériel. Il a un accès direct aux composants de la machine.  
Un hyperviseur de type 1 ==remplace le système d'exploitation==.  
Les types 1 sont à usage professionnel.  
Les plus connus : Proxmox et ESXi.  

- **Type 2**  
Le logiciel s'exécute sur un système d'exploitation classique (Windows, Linux).  
Un hyperviseur de type 2 ==ne remplace pas le système d'exploitation==.  
Les types 2 sont plutôt à usage personnel.  
Les plus connus : Virtualbox et VMWare Workstation.  

## Virtualbox
est l'hyperviseur de type 2 développé par Oracle le plus populaire et le plus facile d'utilisation.  
??? note "Installation sur Debian 11"  
    Virtualbox n'est pas encore disponible sur Debian à cause d'une mauvaise coopération avec l'équipe d'Oracle concernant le support d'anciennes versions de Virtualbox. [794466](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=794466).  
    ```console
    # export codename=$(grep VERSION_CODENAME /etc/os-release | cut -d '=' -f2)
    # echo "deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian $codename contrib" > /etc/apt/sources.list.d/virtualbox.list
    # wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
    # wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
    # apt-get update
    # apt-get install -y linux-headers-$(uname -r)
    # apt-get install -y virtualbox-6.1
    # apt install -f -y
    # vboxconfig
    ```  

??? note "Installation sur Ubuntu"  
    ```console
    # apt install virtualbox
    ```
??? note "Installation sur Windows"
    [https://www.virtualbox.org/wiki/Downloads](https://www.virtualbox.org/wiki/Downloads)

### VboxManage
est l'outil permettant d'utiliser VirtualBox en ligne de commande.  
Il existe beaucoup de commandes, un wiki très complet peux être retrouvé [ici](https://www.virtualbox.org/manual/ch08.html).
```console title="Créer une VM"
$ VBoxManage createvm --name "Nom de la VM" --ostype "Linux_64" --register
```
```console title="Parametrer le materiel"
$ VBoxManage modifyvm "Nom de la VM" --cpus 1 --memory 1024 --vram 128 --graphicscontroller vmsvga \
  --nic1 nat --nic2 intnet
```
```console title="Créer le disque dur"
$ VBoxManage createhd --filename "Nom de la VM".vdi --size 15360 --format VDI
```
```console title="Définir un controleur de disque dur"
$ VBoxManage storagectl "Nom de la VM" --add sata --name SATA
```
```console title="Attacher le disque dur virtuel au controleur"
$ VBoxManage storageattach "Nom de la VM" --storagectl SATA --port 0 \
  --type hdd --medium "Nom de la VM".vdi
```
```console title="Ajouter un port DVD au controleur"
$ VBoxManage storageattach "Nom de la VM" --storagectl SATA --port 1 \
  --type dvddrive --medium emptydrive
```
```console title="Démarrer une VM"
$ VBoxManage startvm "Nom de la VM"
```
```console title="Démarrer une VM sans affichage"
$ VBoxManage startvm "Nom de la VM" --type headless
```
```console title="Sauvegarder l'état et arrêter la VM"
$ VBoxManage controlvm "Nom de la VM" savestate
```
```console title="Faire une snapshot (instantané)"
$ VBoxManage snapshot "Nom de la VM" take "Nom du snapshot"
```
```console title="Restorer une snapshot"
$ VBoxManage snapshot "Nom de la VM" restore "Nom du snapshot"
```
```console title="Arrêter la VM"
$ VBoxManage controlvm "Nom de la VM" acpipoweroff
```
```console title="Lister les VMs"
$ VBoxManage list vms
```


