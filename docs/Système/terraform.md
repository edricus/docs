![ansible](../src/terraform.svg){ align=left width=190 }
# Terraform
est un logiciel d'infrasctructure as code libre et open-source.  
Il s'opère grâce à des fichiers .tf.  


## Installation
```console
# apt install gpg -y
# wget -O- https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
# echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
# apt update
# apt install terraform -y
```


