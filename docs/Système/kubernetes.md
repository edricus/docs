![kubernetes](../src/k8s.png){ align=left width=160 }  
# Kubernetes
est un orchestrateur de conteneurs open-source servant à automatiser le deploiement, le scaling et la gestion des applications conteneurisées. Développé par Google.  

## Lexique
- #### K8s
  "Kubernetes" en abrégé, où le "8" est le nombre de lettres entre 'K' et 's'.  
  Il y a aussi "K3s" qui est une version légère de K8s pour les systèmes embarqués.  
- #### Workload  
  pour charge de travail  
  Une application fonctionnant avec Kubernetes, composée de **Pods**
- #### Pods
  Un ou plusieurs conteneurs s'exécutant sur un **Node**
- #### Nodes  
  Machines physiques ou virtuelles où est installé K8s.  
- #### Control Plane
  Un type de node, c'est ceux qui vont gérer les nodes **Workers**  
  Ils on le rôle d'assurer le bon fonctionnement des Pods notamment en gérant la haute dispo et le redémarrage des Pods.  
- #### Worker
  Un type de node, c'est ceux qui vont exécuter les Pods.
- #### Cluster  
  Un réseau de nodes interconnectés.

## Utilitaires
 - #### kubeadm
   Commande qui gère le réseau de notre cluster
 - #### kubectl
   Commande qui gère le reste de notre cluster
 - #### kubelet
   Composant installé sur les nodes qui permet d'exécuter les commandes  
 - #### minikube
   Utilitaire d'apprentissage de kubernetes en local  

## Minikube
est un fork de K8s permettant de déployer un cluster en local.  
Il est utilisé comme introduction à K8s  
Premiers pas : [https://minikube.sigs.k8s.io/docs/start/](https://minikube.sigs.k8s.io/docs/start/){ target=about:_blank }  

## Installation de Kubernetes  
Procedure visant à déployer une application en production grâce à K8s.  
### Configuration des nodes

< All Nodes >
#### Initialiser les modules

```console
# cat << EOF > /etc/modules-load.d/containerd.conf
overlay
br_netfilter
EOF
```
```console
# modprobe overlay 
# modprobe br_netfilter
```

### Configurer le kernel 
pour accepter l'ip forwarding et le passer à iptables
```console
# cat << EOF > /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF
```
```console
# sysctl --system
```

### Install Containerd
Le moteur de contenerisation
```console
# apt-get update && apt-get install containerd -y
# mkdir -p /etc/containerd
# containerd config default > /etc/containerd/config.toml
# systemctl restart containerd
```

### Installer Kubernetes
On installe la version **1.28.0** et on la bloque avec apt-mark pour éviter de la mettre à jour automatiquement.  
On désactive aussi le swap, [qui est une étape controversée du processus d'installation de K8s](https://serverfault.com/questions/881517/why-disable-swap-on-kubernetes){ target=about:_blank }, mais voulue par les mainteneurs.   
```console
# swapoff -a
# apt-get install apt-transport-https curl gpg -y
# curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.28/deb/Release.key | gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
# echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.28/deb/ /' > /etc/apt/sources.list.d/kubernetes.list
# apt-get update
# apt-get install kubelet=1.28.0-1.1 kubeadm=1.28.0-1.1 kubectl=1.28.0-1.1 -y
# apt-mark hold kubelet kubeadm kubectl
```

[Télécharger le script d'installation de Kubernetes](https://gitlab.com/edricus/docs/-/raw/main/scripts/k8s-install.sh?ref_type=heads){ target=about:_blank }  
[Télécharger le script de configuration du control node de Kubernetes](https://gitlab.com/edricus/docs/-/raw/main/scripts/k8s-control.sh?ref_type=heads){ target=about:_blank }  

### Initialisation des nodes
< Control Node >

#### Créer un réseau
```
# systemctl start kubelet
# kubeadm init --pod-network-cidr 192.168.0.0/16 --kubernetes-version 1.28.0
```

#### Gérer le cluster à partir de l'utilisateur courant
```console
$ mkdir -p $HOME/.kube
$ sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
$ sudo chown $(id -u):$(id -g) $HOME/.kube/config
```
```console
$ kubectl get nodes
```
```
NAME          STATUS     ROLES           AGE     VERSION
k8s-control   NotReady   control-plane   9m31s   v1.28.0
```

### Installation de Calico
Calico est un utilitaire pour K8s servant à faire communiquer des nodes de façon sécurisé.  

< Control Node >
```console
$ kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
```
Après quelque minutes. . .  
```console
$ kubectl get nodes
```
```
NAME          STATUS     ROLES           AGE     VERSION
k8s-control   Ready      control-plane   12m09s  v1.28.0
```
On va ensuite créer une commande à exécuter sur nos workers qui va les faire rejoindre le cluster.  
```console
$ kubeadm token create --print-join-command
```
< Worker Nodes >
```console
$ kubeadm join 10.0.1.101:6443 --token <...> --discovery-token-ca-cert-hash <sha256:...>
```
< Control Node >
```console
$ kubectl get nodes
```
```
NAME          STATUS   ROLES           AGE     VERSION
k8s-control   Ready    control-plane   36m     v1.28.0
k8s-worker1   Ready    <none>          4m45s   v1.28.0
k8s-worker2   Ready    <none>          4m42s   v1.28.0
```
### 
## Commandes
!!! warning "Pas de retour à la ligne comme sur le tableau, il y a qu'une commande par cellule"
kubctl est l'outil de K8s permetant de gérer nos clusters  

| Commande | Action |
| :--- | :--- |
| <h3> Kubectl </h3> |
| `kubectl get services` | Liste les services |
| `kubectl get services -o=wide` | Affiche plus d'informations sur les services |
| `kubectl get pods` | Liste les pods |
| `kubectl get pv` | Liste les volumes persistants |
| `kubectl apply -k ./` | Deploie notre service dans le répertoire courant |
| `kubectl apply -f wordpress.yaml` | Deploie notre service à partir |
| `kubectl create namespace wordpress` | Créé un namespace "wordpress" |
| `kubectl scale --replicas=3 service/wordpress` | Duplique le service "wordpress" |
| `kubectl edit deployment wordpress -n wordpress` | Edite le deployment "wordpress" du namespace "wordpress" |
| `kubectl edit service wordpress -n wordpress` | Edite le service "wordpress" du namespace "wordpress" |
| `kubectl create namespace wordpress` | Edite le deployment "wordpress" |
| `kubectl scale --replicas=3 service/wordpress` | Duplique le service "wordpress" |
| `kubectl delete service wordpress` | Supprime tous les services avec le nom "wordpress" |
| <h3> Minikube </h3> |
| `minikube start` | Deploie minikube |
| `minikube dashboard` | Deploie le dashboard minikube |
| `kubectl create deployment hello-minikube --image=k8s.gcr.io/echoserver:1.4` | Deploie un service "hello-minikube" de type "deployment" avec l'image "echoserver" |
| `minikube delete --all` | Supprime totalement minikube |

