![deluge](../src/deluge.svg){ align=left width=160 }  
# Deluge
est une application BitTorrent disponible à la fois en tant que client et serveur.  
En mode serveur Deluge est utilisable depuis un navigateur web.  

## Installation

### Installation des paquets  

**deluged** est le daemon s'executant en arrière-plan et gérant les opérations.  
**deluge-web** est l'interface web.  
```console
# apt install deluged deluge-web
```

### Creation de l'utilisateur deluge

```console
# adduser --disabled-password --system \
  --home /var/lib/deluge \
  --gecos "Deluge service" \
  --group deluge
```

### Services systemd à créer

```title="/etc/systemd/system/deluged.service"
[Unit]
Description=Deluge Bittorrent Client Daemon
After=network-online.target

[Service]
Type=simple
User=deluge
Group=deluge
UMask=000
ExecStart=/usr/bin/deluged -d
Restart=on-failure
TimeoutStopSec=300

[Install]
WantedBy=multi-user.target
```
```title="/etc/systemd/system/deluge-web.service"
[Unit]
Description=Deluge Bittorrent Client Web Interface
After=network-online.target

[Service]
Type=simple
User=deluge
Group=media
UMask=027
ExecStart=/usr/bin/deluge-web -d
Restart=on-failure

[Install]
WantedBy=multi-user.target
```
```console
# systemctl daemon-reload
# systemctl enable --now deluged deluge-web
```

## Configuration
Ports à ouvrir :  

  - tcp/8112    (web)  
  - tcp/58846   (daemon, optionel)  
  - tcp/5000    (incoming, optionel)  

L'interface web de deluge est disposible sur le port **8112**, le mot de passe est **deluge**.  
Ensuite, configurer comme suit :  

#### Téléchargements  
![deluge-1](../src/deluge-1.png){ width=450 }  

#### Réseau  
![deluge-2](../src/deluge-2.png){ width=450 }  
!!! info "L'incoming port est à ouvrir sur la box et le pare-feu, sans lui nous sommes en mode 'passif', ce qui signifie qu'on ne pourra se connecter qu'aux personnes ayant leur port d'ouvert."  
!!! info "Les DHT sont à activer si on utilise des torrents publics comment ceux de distributions linux ou de trackers publics comme ThePirateBay"  

#### Encryption  
![deluge-3](../src/deluge-3.png){ width=450 }  

#### Bande passante  
![deluge-4](../src/deluge-4.png){ width=450 }  
!!! info "-1 = illimité"  

#### Daemon  
![deluge-5](../src/deluge-5.png){ width=450 }  
!!! info "Seulement pour l'accès à distance, à ouvrir sur sa box si on veut acceder à deluge hors du réseau local"  

#### Liste d'attente
![deluge-6](../src/deluge-6.png){ width=450 }  
!!! info "Décocher le 'share ratio' pour seeder, en raison du fait que je n'utilise pas de VPN, je préfère ne pas le faire"  

#### Plugins
![deluge-7](../src/deluge-7.png){ width=450 }  
!!! info "Label pour l'integration avec Radarr/Sonarr, pas obligatoire"  

#### Blocklist
![deluge-8](../src/deluge-8.png){ width=450 }  
Utiliser l'URL suivante :  
[http://list.iblocklist.com/?list=ydxerpxkpcfqjaybcssw&fileformat=p2p&archiveformat=gz](http://list.iblocklist.com/?list=ydxerpxkpcfqjaybcssw&fileformat=p2p&archiveformat=gz){ target=about_blank }  
Et cliquer sur "Check Download and Import"  

## Connection à distance
Il faut créer un utilisateur dans deluge à l'aide du fichier **/var/lib/deluge/.config/deluge/auth**.  
L'utilisateur "localclient" est déjà présent, il n'est pas utilisable à distance.  
La syntaxe est : ```utilisateur:mot-de-passe:niveau``` où le niveau est :  

  - 0: aucun accès  
  - 1: lecture seule  
  - 5: tout sauf gérer les utilisateurs  
  - 10: admin  

!!! info "La gestion des utilisateurs à l'aide du compte se fait avec l'API, ce qui nous interesse pas ici"

```title="/var/lib/deluge/.config/deluge/auth"
localclient:[CENSURÉ]:10
trireme:[CENSURÉ]:5
```
### Client Android
Trireme est un client Deluge avec une interface moderne (Flutter) :  
Lien github     : [https://github.com/teal77/trireme/releases/latest](https://github.com/teal77/trireme/releases/latest){ target=about_blank }  
Lien f-droid    : [https://f-droid.org/packages/org.deluge.trireme/](https://f-droid.org/packages/org.deluge.trireme/){ target=about_blank }  
Pour se connecter à notre instance :  

  - Panneau de gauche  
  - Add a server  
  - Host : l'addresse du ==serveur== et non de l'instance (ex: 192.168.1.150, ou l'IP publique)  
  - Port : 58846  
  - Certificate : laisser l'app en générer un (continuer)
  - User : celui rajouté dans le fichier auth

### Client Desktop
Pas de fork necessaire l'application fournie est suffisant.  
Il faut néanmoins le mettre en mode "Thin client" pour le connecter à notre serveur.  
![deluge-9](../src/deluge-9.png){ width=500 }  
![deluge-10](../src/deluge-10.png){ width=450 }  
![deluge-11](../src/deluge-11.png){ width=450 }  
![deluge-12](../src/deluge-12.png){ width=450 }  

## Installation sur Docker
à faire...  

## Problèmes

### Erreur ou le torrent ne se télécharge pas
99% du temps c'est à cause du dossier de téléchargement qui n'a pas les bons droits.  
Vérifier que celui si appartient bien à l'utilisateur deluge et qu'il a les droits de lecture et d'écriture.  

### Torrent ne se télécharge pas malgré qu'il a des peers  
En général sur des torrents où il y a très peu de peers, le fait d'être en mode passif peut nous bloquer.  
Si un torrent a 5 peers, tous en mode passif, on ne pourra pas le télécharger.  
Passer en mode actif en ouvrant le ==incoming port== dans le pare-feu et sur la box.  

### Impossible de se connecter
Le service ==deluge-web== se connecte au daemon ==deluged==.  
Ces deux services doivent être opérationels.  
==Ne pas bidouiller le umask, l'utilisateur ou groupe des services sans savoir ce que l'on fait==  
```console
# systemctl restart deluged deluge-web
# systemctl enable deluged deluge-web
```
