# Lexique système
### Open-source (logiciel)
Un logiciel open-source est un logiciel dont le code est disponible en libre accès sur internet, ceci afin de garantir une transparence ainsi que la possibilité aux utilisateurs de contribuer à son developpement, ce qui est très pratique vu qu'en dehors d'employés payés à développer, les utilisateurs peuvent reporter, corriger des erreurs, voir proposer de nouvelles fonctionnalités.  

![gitlab.jpg](../src/gitlab.png){ width="64" } ![github.jpg](../src/github.png){ width="64" }
<p style="color:grey;font-size:13px">Les platformes d'hebergement de logiciels open-source les plus connues: Gitlab et Github</p>
### Libre (logiciel)  
Un logiciel libre est un logiciel dont l'utilisation, la modification et la duplication en vue de sa diffusion sont permises techniquement et juridiquement, ceci afin de garantir certaines libertés induites, dont le contrôle du programme par l'utilisateur et la possibilité de partage entre individus.  
Cette definition se rapproche beaucoup de celle de l'open-source et pour cause:  
**un logiciel libre est forcement open-source**.  
La difference et la notion de **libertée** et non de **practicité**.   
Le terme "open-source" est souvant utilisé comme argument de vente pour prouver une  certaine fiabilitée et valeur pratique, ce qui n'est pas l'image que veut renvoyer les éditeurs de logiciels libres.  

![stallman.jpg](../src/stallman.jpg)
<p style="color:grey;font-size:13px">Richard Stallman, fondateur de la notion de logiciel libre</p>
!!! quote "FOSS vs FLOSS"
    Le terme **FOSS** pour "Free & Open Source Software" est utilisé pour nommer ce genre de logiciel mais au vu de l'ambigiuté du terme "free" qui peut designer à la fois la libertée et la gratuitée, un nouveau terme a vu le jour: **FLOSS** (Free/Libre & Open Source Software) qui vise à retirer tout ambiguités.
### Propriétaire (logiciel)
À l'inverse du logiciel libre et open-source, le logiciel propriétaire ne permet pas la modification, la duplication et la diffusion de son code.  
On les appeles aussi des logiciels closed-source.  
### Fork (logiciel)
Un fork désigne un nouveau logiciel créé à partir du code source d'un logiciel existant.  
Certains apparaissent après la mort d'un projet open-source comme Libreoffice, fork de OpenOffice, d'autres on pour but de partir d'une base solide pour commencer un projet avec une philosophie de developpement précise comme Brave, navigateur basé sur Chromium (version open-source de Chrome) centré sur la protection de la vie privée.  

![brave.png](../src/brave.png){ width="64px" } ![libreoffice.png](../src/libreoffice.png){ width="64px" }  
<p style="color:grey;font-size:13px">Logos de Brave et Libreoffice</p>
### Système d'exploitation
Un système d'exploitation (ou OS) est un ensemble de logiciels permettant l'utilisation d'un appareil informatique.  
### Kernel
Le kernel, ou noyau en français, permet d'exploiter le materiel pour faire fonctionner les logiciels, il fonctionne de pair avec le système d'exploitation, c'est d'ailleurs le kernel qui donne son nom au système d'exploitation.
### Driver
Ou pilote en français, est un logiciel externe au kernel permetant d'exploiter un materiel informatique, le pilote est souvant propriétaire 
