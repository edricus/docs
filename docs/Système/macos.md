<figure markdown>
![macos](../src/macos.svg){ width="250" }
</figure>
# MacOS
est le système d'exploitation créé par Apple est équipant tous les ordinateurs Apple.  
Jusqu'en 2006, MacOS s'appellait OSX. Ce changement de nom avait pour but de coller aux autres noms des systèmes d'exploitation de la marque (iOS, tvOS, watchOS etc...)  

## Recovery  
Pour acceder au recovery :  
Version > 10.8 : ++command+r++  
Version < 10.8 : boot depuis un CD (ou clé USB ?)

## Boot USB/CD
++option++ au démarrage

## HFS+ sur Linux
Le format de partition par defaut sous MacOS, a besoin d'un driver sur linux.
```console
# apt install hfsprogs
# mount -t hfsplus -o force,rw /dev/sdXy /mnt 
```
Si la partition se monte en lecture seule, il faut la nettoyer
```console
# umount /dev/sdXy
# fsck.hfsplus -f /dev/sdXY
```
On peut vérifer les options de la partition avec le fichier mtab
```console
# grep hfsplus /etc/mtab
```
```console
hfsplus rw,relatime,umask=22,uid=0,gid=0,nls=utf8 0 0
```
`rw` indique que la partition est bien en lecture/écriture alors que `ro` indique que la commande fsck est necessaire  

## Partitionnement
1. Booter sur le Recovery  
2. Onglet "Utilitaires" en haut de l'écran  
3. Cliquer sur "Utilitaire de disque..."  
4. Choisir le disque dur (premier dans la liste)  
5. Onglet "Partionner"  

!!! danger "Ne pas formater la partition MacOS !"
    Le recovery se trouve sur cette partition, en la supprimant il sera necessaire de se procurer un CD ou une clé USB pour réinstaller MacOS.  
    Ça peut être un choix sur un très vieux Mac que l'on sait compatible avec Linux mais pas sur un Mac où MacOS peut être encore utile.

## iPhoto
Les données de ce logiciel se trouvent dans  
`/Users/<user>/Pictures/Libraire iPhoto/Master`

## MacOS sur VirtualBox
Il est possible de faire une machine virtuelle MacOS sur virtualbox mais la procèdure n'est pas commune.  
On utilise un script qui va récuperer les fichiers d'installation sur les serveurs d'Apple et automatiser l'installation.  
```console
$ git clone https://gitlab.com/edricus/edricus-macos-vbox
$ cd edricus-macos-vbox
$ ./macos.sh
```
### Redimensionnement
Si la taile du disque n'est pas suffisante et que l'on souhaite l'agrandir sans réinstaller, proceder comme suit:

1. Fermer la VM completement  
2. Aller dans **Tools > Media > dérouler MacOS.vdi > {...}.vdi**
3. En bas, faire glisser le curseur pour ajuster la taille  
4. Cliquer sur ==Apply==  
5. Démarrer la VM et tapoter la touche Esc pour faire apparaitre le menu EFI  
6. Sur ce menu, selectionner ==Boot order==  
7. Selectionner ==HardDisk ...==  
8. Arrivé à la fenêtre, cliquer sur **Tools > Terminal**  
9. Rentrer la commande `diskutil repairdisk disk0`  
10. Quitter le terminal  
11. Cliquer sur ==Disk Utility== depuis la fenêtre  
12. Selectionner le disque ==MacOS== (et pas Data ni Base System)  
13. En haut de la fenetre cliquer sur ==Partition==  
14. Valider en recliquant sur ==Partition==  
15. Selectionner l'espace vide et cliquer sur le ==-== en bas  
16. Cliquer sur ==Apply==  
17. Quand tout est fini, redémarrer MacOS sur le premier disque: Pomme > Startup Disk > MacOS  
