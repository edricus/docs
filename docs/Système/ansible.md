![ansible](../src/ansible.png){ align=left width=190 }
# Ansible
est un logiciel d'Infrastructure as Code permettant de gérer la configuration et le déploiement d'applications sur des hôtes distants sans avoir besoin d'installer d'agents sur ceux-ci.    
Il opère via SSH.  

## Installation
Ansible est disponible depuis les dépôts de Debian
```console
# apt update
# apt install ansible -y
```
Pour installer une version plus à jour, il faut passer par les dépôts Ubuntu
```console
$ UBUNTU_CODENAME=jammy
$ wget -O- "https://keyserver.ubuntu.com/pks/lookup?fingerprint=on&op=get&search=0x6125E2A8C77F2818FB7BD15B93C4A3FD7BB9C367" | sudo gpg --dearmour -o /usr/share/keyrings/ansible-archive-keyring.gpg
$ echo "deb [signed-by=/usr/share/keyrings/ansible-archive-keyring.gpg] http://ppa.launchpad.net/ansible/ansible/ubuntu $UBUNTU_CODENAME main" | sudo tee /etc/apt/sources.list.d/ansible.list
$ sudo apt update && sudo apt install ansible
```

## Inventaire
Ce sont les hôtes qu'Ansible va gérer.   
Par défaut, le fichier d'inventaire se trouve dans ==/etc/ansible/hosts==  
Bonne pratique : créer un dossier pour nos fichiers ansible avec un fichier 'inventory' à la racine  

#### Déclaration des hôtes
On peut les diviser par groupes avec des crochets []  
2 groupes sont présents automatiquement :  

<table>
<tr>
    <td> all 
    <td> contient tous les hôtes listés dans le fichier
</tr>
<tr>
    <td> ungrouped
    <td> contient tous les hôtes sans aucun groupe
</tr>
</table>
    

Exemple de fichier avec des groupes :    
```
[webservers]
app1.example.com
app2.example.com

[dbs]
db.example.com
db2.example.com
```

On peut aussi regrouper les groupes dans un groupe parent :  
```
[nginx]
app1.example.com

[apache]
app2.example.com

[webservers:children]
apache
nginx

[postgresql]
db.example.com

[oracle]
db2.example.com

[dbs:children]
postgresql
oracle
```

On peut spécifier les hosts avec une range alphanumérique :  
```
[webservers]
app[1:2].example.com

[dbs]
db[1:2].example.com
```

#### Commandes d'inventaire

Lister les hosts de l'inventaire :  
```console
$ ansible all --list-hosts
```

Lister les hosts d'un groupe :  
```
$ ansible webservers --list-hosts
```

## Configuration
Il s'agit de la configuration du comportement de Ansible.  
Par défaut, le fichier d'inventaire se trouve dans ==/etc/ansible/ansible.cfg==  
Bonne pratique : créer un dossier pour nos fichiers ansible avec un ansible.cfg à la racine  
On peut aussi spécifier la variable ANSIBLE_CONFIG avec le chemin du fichier config.  

#### Contenu
```
[defaults]
inventory = ./inventory
remote_user = user
ask_pass = false

[privilege_escalation]
become = true
become_method = sudo
become_user = root
become_ask_pass = false
```

| Instruction | Déscription |
| :- | :- |
| inventory | Le chemin du fichier inventory |
| remote_user | L'utilisateur distant |
| ask_pass | Si SSH doit demander un mot de passe |
| become | Si Ansible dois changer d'utilisateur immédiatement après la connexion |
| become_method | Comment changer d'utilisateur, sudo ou su |
| become_user | L'utilisateur final |
| become_ask_pass | Si la become_method doit demander un mot de passe, defaut: false |

!!! info "Dans cet exemple, on `ask_pass` est sur `false`, ça veut dire que la seule façon pour Ansible de s'authentifier est pas la clé publique. Il faudra faire un `ssh-keygen -t rsa` suivi d'un `ssh-copy-id <host>` pour faire fonctionner cette configuration."

## Commandes Ad-Hoc
Ce sont des commandes exécutant des tâches sur les hôtes sans devoir faire de playbooks.  
Utile pour tester la connexion d'Ansible aux hôtes avec un ping.
```console
$ ansible all -m ping
```
```
172.22.0.15 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}

```
Pour spécifier une option, on utilise -a 
```console
$ ansible all -m package -a "name=vim state=present"
```
On peut utiliser l'option -o pour afficher les résultats sur une seule ligne  
```console
$ ansible all -m command -a "cat /etc/hostname"
```
```
172.22.0.15 | CHANGED | rc=0 | (stdout) lab
```

#### Commandes utiles
Ci-dessous une liste des commandes fréquemment utilisés   

| Commande | Déscription |
| :- | :- |
| **ping** | Envoie un ping |
| **reboot** | Redémarre un hôte |
| **command** [*commande*] | Exécute une commande shell |
| **package** -name=[*nom*] -state=[*present/absent*] | Installe/Désinstalle un paquet |
| **copy** -src=[*source*] -dest=[*destination*] | Copier un fichier local vers un hôte |
| **file** -path=[*cible*] | Change les propriétés d'un fichier |
| **service** -name=[*nom*] -state=[*started/stopped/restarted*] | Gère un service |
| **user** -name=[*nom*] -state=[*present/absent*] | Créé/Supprime un utilisateur |
| **lineinfile** -path=[*cible*] -line=[*ligne à ajouter*] | Ajoute une ligne à la fin d'un fichier |
| **get_url** -url=[*url*] -dest=[*destination*] | Télécharge un fichier depuis une url vers la destination |

Pour afficher la documentation d'une commande, on utilise ==ansible-doc [*commande*]==

#### Connexion
En mode Ad-Hoc on peut aussi se passer d'un ==ansible.cfg==  

| Directive | Equivalent en CLI |
| :- | :- |
| inventory | -i |
| remote_user | -u |
| become | --become, -b |
| become_method | --become-method |
| become_user | --become-user |
| become_ask_pass | --ask-become-pass, -K |

## Playbooks
 La puissance d'Ansible réside dans son utilisation de fichiers contenant des instructions.  
 C'est avec les playbooks qu'on utilise Ansible en mode IaS (Infrastructure as Code) et non en CLI.  
 Les playbooks sont des fichiers ==yaml==.  
 Pour comprendre les playbooks on va partir de la commande Ad-Hoc suivante : 
 ```console
 $ ansible 172.22.0.15 -m user -a "name=john uid=4000 state=present"
 ```
 Ici, on a créé un utilisateur ==john== avec l'UID ==4000==.  
 Maintenant, dans un playbook :  
```yaml
---
- name: Création des utilisateurs
  hosts: 172.22.0.15
  tasks:
    - name: John avec l'UID 4000
      user:
        name: john
        uid: 4000
        state: present
```

| Element | Description |
| :- | :- |
| **---** | À placer au début d'un playbook |
| **name (1)** | Un "play", va contenir des tâches avec des instructions |
| **tasks** | La liste des taches du play |
| **name (2)** | Le nom d'une tache |
| **user** | La commande |
| **name/uid/state** | Les options |

Pour exécuter le playbook on utilise la commande **ansible-playbook**  
```console
$ ansible-playbook main.yml
```
```
PLAY [Création des utilisateurs] ******************************************************************

TASK [Gathering Facts] ****************************************************************************
ok: [172.22.0.15]

TASK [John avec l'UID 4000] ***********************************************************************
changed: [172.22.0.15]

PLAY RECAP ****************************************************************************************
172.22.0.15 : ok=2    changed=1   unreachable=0   failed=0    skipped=0   rescued=0   ignored=0   
```

!!! info "On peut exécuter un dryrun avec l'option -C"
#### Verbosité
On peut les rendre plus verbeux avec les options ==-v==  
<table>
  <tr>
    <td> -v
    <td> Les resultats des tâches s'affichent
  </tr>
  <tr>
    <td> -vv
    <td> Les resultats des tâches et leur configuration s'affichent
  </tr>
  <tr>
    <td> -vvv
    <td> Inclus les informations de connexions aux hôtes
  </tr>
  <tr>
    <td> -vvvv
    <td> Inclus encore plus d'informations de connexion
  </tr>
</table>

#### Multiples plays
En utilisant plusieurs plays, il est possible de personnaliser la manière dont Ansible utilise chacun de ceux-ci.  
Par exemple :  
```yaml
---
- name: SETUP USER JOHN
  hosts: all
  tasks: 
    - name: create user
      user:
        name: john
        password: [ sha-512 encrypted password... ]
        home: "/home/john"
        shell: "/bin/bash"
        group: sudo
        state: present

- name: APT INSTALL AS JOHN
  hosts: all
  become: yes
  become_method: su
  become_user: john
  tasks:
    - name: install vim as john
      apt:
        name: 'vim'
        state: present
```

  - Pour le premier play on a créé l'utilisateur john
  - Pour le deuxième play : on a dit à ansible d'utiliser l'utilisateur john avant d'exécuter une commande

!!! question "Pour créer le mot de passe utilisateur, utiliser la commande ==mkpasswd --method=sha-512== du paquet ==whois=="

## Variables
Les variables sur Ansible peuvent se déclarer un peu partout, ==elles ne doivent pas contenir de points, d'espaces ou de signes '$'==  
#### Définitions de variables
Exemples de définitions de variables : 

 - dans les playbooks, directement  

  ```yaml title="main.yml"
  ---
  - name: SETUP USER JOHN
    hosts: all
    vars:
      user: joe
      password: [ sha-512 encrypted password... ]
  [...]
  ```

 - dans les playbooks, depuis un fichier externe

  ```yaml title="main.yml"
  ---
  - name: SETUP USER JOHN
    hosts: all
    vars_files:
      - vars/users.yml
  [...]
  ```

avec comme fichier externe :  

  ```yaml title="users.yml"
  user: joe
  password: [ sha-512 encrypted password... ]  
  ```

  - dans le fichier inventory, depuis un host

  ```title="inventory"
  [servers]
  web.example.com   user=john
  web2.example.com
  ```

  - dans le fichier inventory, depuis un groupe

  ```yaml --title="inventory"
  [servers]
  web.example.com
  web2.example.com

  [servers:vars]
  user=john
  ```

#### Utilisation de variables
On appelle les variables avec la syntaxe =="{{ variable }}"==  
Les guillemets sont obligatoires.  

```yaml title="main.yml"
[...] (avec vars ou vars_files)
  tasks: 
    - name: create user
      user:
        name: "{{ user }}"
        password: "{{ password }}"
        home: "/home/john"
        shell: "/bin/bash"
        state: present
```

Il est possible de redéfinir une variable à la volée depuis la commande ==ansible-playbook==  
```console
$ ansible-playbook main.yml -e "user=joe"
```

#### Tableau de variables (arrays)
La définition des variables suivantes 
```yaml
webserver_apache_user: apa0
webserver_nginx_user: ngx0
database_psql_user: pql0
database_mariadb_user: mb0
```

Peut être redéfinie comme suit 
```yaml
users:
  webservers:
    apache: apa0
    nginx: ngx0
  databases:
    psql: pql0
    mariadb: mdb0
```

...et pour les utiliser  

```yaml title="main.yml"
[...] (avec vars ou vars_files)
  tasks: 
    - name: create user apache
      user:
        name: "{{ users.webservers.apache }}"
        password: "{{ password }}"
        home: "/home/apache"
        state: present
```
!!! info "Il est aussi possible d'appeler les variables avec la syntaxe suivante: `"{{ users['webservers']['apache'] }}"`"

## Vault (secrets)  
Bien que la création d'utilisateurs exige du chiffrage, certaines variables vont devoir être en clair, il faut les protéger.  
Ansible intègre une commande pour chiffrer un fichier: ==ansible-vault==.  
On l'utilise pour chiffrer un fichier variable qu'on va utiliser dans nos playbooks.  

#### Chiffrer un fichier
Depuis le fichier suivant :  

```yaml title="secrets.yml"
password: "P@ssword2024"
```

En utilisant la commande suivante pour chiffrer le fichier :  
```console
$ ansible-vault encrypt secrets.yml
```
```
New Vault password: 
Confirm New Vault password: 
Encryption successful
```

à partir d'ici, on peut utiliser la commande ansible-vault pour :  

| Commande | Description |
| :- | :- |
| edit | Éditer le fichier |
| rekey | Modifier le mot de passe |
| create | Créer le fichier |
| decrypt | Décrypter le fichier |
| view | Afficher le contenu du fichier |

On peut aussi faire un fichier mot de passe pour l'automatisation :  
```console
$ ansible-vault rekey --new-password-file=key secrets.yml
```
Pour l'utiliser :  
```console
$ ansible-playbook --vault-password-file=key main.yml
```

!!! info "L'option --new-password-file fonctionne avec la commande "create" et "encrypt""

#### Utiliser le vault dans un playbook
Le fichier vault n'est qu'un fichier de variables, donc :  

```yaml title="main.yml"
---
- name: SETUP USER JOHN
  hosts: all
  vars_files:
    - vars/users.yml
    - vault.yml
[...]
```

Le playbook ne pourra cependant pas le lire si on ne spécifie pas l'option ==--ask-vault-pass== ou ==--vault-password-file==

## Facts
Les facts sont des variables automatiquement créées par Ansible relatives aux hosts.  
Une liste de quelques facts, ils font partie du groupe "ansible_facts" donc on les appelle avec ==ansible_facts.[variable]==  

| Variable | Description |
| :- | :- |
| hostname | Le nom d'hôte simple |
| fqdn | Le fully qualified domain name |
| default_ipv4.address | L'adresse IPv4 par défaut (basé sur le routage) |
| interfaces | La liste des interfaces réseau |
| devices.sda.partitions.sda1.size | Taille de la partition sda1 |
| kernel | La version du kernel |

#### Afficher les facts
Une méthode pour savoir quels facts sont créés par ansible est d'utiliser la tâche debug :  

```yaml title="main.yml"
- name: Fact dump
  hosts: all
  tasks:
    - name: Print all facts
      debug:
        var: ansible_facts
```
```yaml
PLAY [Fact dump] *********************************************************************************

TASK [Gathering Facts] ***************************************************************************
ok: [jujubbox.duckdns.org]

TASK [Print all facts] ***************************************************************************
ok: [172.22.0.15] => {
    "ansible_facts": {
        "all_ipv4_addresses": [
            "10.8.0.1",
            "192.168.1.100"
        ],
        "all_ipv6_addresses": [
            "fe80::ab67:620b:c224:458c",
            "fe80::12bf:48ff:fee3:856b"
        ],
        "ansible_local": {},
        "apparmor": {
            "status": "enabled"
        },
        "architecture": "x86_64",
        "bios_date": "05/14/2014",
        "bios_vendor": "American Megatrends Inc.",
        "bios_version": "4901",
        "board_asset_tag": "To be filled by O.E.M.",
        "board_name": "[REDACTED]",
        "board_serial": "[REDACTED]",
        "board_vendor": "[REDACTED]",
        "board_version": "Rev 1.xx",
        "chassis_asset_tag": "[REDACTED]",
        "chassis_serial": "Chassis Serial Number",
        "chassis_vendor": "Chassis Manufacture",
        "chassis_version": "Chassis Version",
        "cmdline": {
            "BOOT_IMAGE": "/boot/vmlinuz-6.8.4-2-pve",
            "quiet": true,
            "ro": true,
            "root": "/dev/mapper/pve-root"
        },
[...]
```

??? note "Les facts avant la version 2.5 d'Ansible"  
    Avant de faire partie du groupe ansible_facts, les facts étaient dans des variables individuelles commençant par "ansible_". Par exemple, on utilise ==**ansible_facts.distribution**== alors qu'avant cette variable portait le nom ==**ansible_distribution**==


#### Désactiver les facts
S'il on n'utilise aucun facts, on peut les désactiver pour gagner du temps sur nos plays :  
```yaml title="main.yml"
- name: SETUP USER JOHN
  hosts: all
  gather_facts: no
[...]
```

Il est possible de réactiver les facts avec tâche lorsque ==gather_facts: no== est déclaré avec le module ==setup== :  
```yaml title="main.yml"
[...]
  tasks:
  - name: récolte les facts
    setup:
```
