<figure markdown>
![systemd](../src/systemd.svg){ width=400 }
</figure>

# SystemD
est un logiciel de gestion de services sous Linux.  
Un service, dans le contexte de Linux, est un logiciel s'executant en arrière-plan.  
Un service sur linux est aussi appellé ==daemon==, d'où le nom de ce logiciel, le service du système (SystemDaemon).  
SystemD est le premier service lancé au démarrage de linux, et va s'occuper d'executer tout les autres services.  

## Commandes 
La commande pour interagir avec systemd est `systemctl`  

| Commande | Description |
| :- | :- |
| `systemctl start <service>` | Lancer un service |
| `systemctl restart <service>` | Relancer un service |
| `systemctl status <service>` | Afficher les informations d'un service |
| `systemctl enable <service>` | Définir à service comme s'éxecutant au démarrage |
| `systemctl enable --now <service>` | Pareil que enable et lance le service immédiatement |
| `systemctl daemon-reload` | Recharge la liste des services |
| `systemctl mask <service>` | Equivalent à disable mais plus strict |  

## Création d'un service
Les services sont définis dans des fichiers avec l'extention ".service" dans le repertoire `/etc/systemd/system/`.  
Ce sont de simples fichiers textes.  
### Exemple d'un fichier .service
```
[Unit]
Description=Minecraft server
After=syslog.target network.target
[Service]
User=minecraft
WorkingDirectory=/opt/minecraft/
ExecStart=/usr/bin/java -Xmx4096M -Xms4096M -jar purpur.jar --nogui
Restart=always
RestartSec=30
Type=simple
[Install]
WantedBy=multi-user.target
```
Un service se divise en 3 sections, ==Unit==, ==Service== et ==Install==.  

- [Unit]  
Cette section sert à détailler le service en lui-même.  

==Description==, informatif, décrit seulement le service pour l'utilisateur  
==After==, le service va attendre un autre service avant de démarrer. Dans l'exemple, ce serveur doit attendre l'initialisation du réseau.  
==Before==, même logique, un service peut aussi s'initialiser plus tard si ce paramètre est défini.  

- [Service]  
C'est ici qu'on défini l'execution du service.  

==User==, en tant que tel utilisateur le service va s'executer, si non spécifié, il s'execute en root  
==WorkingDirectory==, le dossier dans lequel va s'éxecuter la commande **ExecStart**  
On l'utilise avec **User** pour limiter l'execution d'un service dans un répertoire dont seul cet utilisateur a accès.  
==ExecStart==, la commande à executer au lancement du service  
==Restart==, permet de redémarrer le service  
Les paramètres: **no**, **on-success**, **on-failure**, **always**  
==RestartSec==, un delai avant de redémarrer le service  
==Type==, le type de service  
Les plus utilisés : **simple** (par defaut) et **oneshot** (quitte après l'éxecution), [plus d'infos ici](https://www.freedesktop.org/software/systemd/man/systemd.service.html#Type=)  

- [Install]  
Utilisé lors des commandes `systemctl enable` et `systemctl disable`, spécifie à quel moment le service va s'éxecuter  

==WantedBy==, comme **After** et **Before** mais au lieu de spécifier d'autres services on définit des **Target**.  
Les plus utilisés: **multi-user.target** (avant l'interface graphique) et **graphical.target** (après l'interface graphique), [plus d'infos ici](https://access.redhat.com/documentation/fr-fr/red_hat_enterprise_linux/7/html/system_administrators_guide/sect-managing_services_with_systemd-targets)  
==RequiredBy==, comme **WantedBy** mais ne démarrera pas si la condition n'est pas remplie.  

