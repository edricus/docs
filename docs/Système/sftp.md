# SFTP
pour SSH File Transfer Protocol, est un protole de transfer de fichier sécurisé.  
Il utilise ssh en surtout de FTP, ce dernier n'étant pas sécurisé.  

## Installation
On commence déjà par installer un serveur ssh
```console
# apt install openssh-server
```
On l'active
```console
# systemctl start ssh
```

## Utilisation
Pour se connecter
```console
# sftp <nom utilisateur destination>@<hote destination>
exemple : # sftp edricus@192.168.1.134
```
Récuperer un fichier depuis l'hôte
```console
# get <fichier à télécharger> <destination>
exemple: # get /home/lab/fichier-test /home/edricus/
```

Récuperer un dossier sur l'hôte
```console
# get -r <dossier à télécharger> <destination>
exemple: # get -r /home/lab/dossier-test /home/edricus/
```

Envoyer un fichier
```console
# put <fichier à envoyer> <destination>
```
```console
# put -r <dossier à envoyer> <destination>
```
