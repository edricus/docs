<figure markdown>
![hacker-cringe.jpg](../src/hacker-cringe.jpg){ width=500" }
</figure>
# Hardening
⚠️  incomplet
## Introduction
Le terme "hardening" ou "renforcement" en Français est le processus permetant de renforcer la sécuritée d'un système.  
Le but est de ==reduire la surface d'attaque==.  
Les principes de base à connaître sont:  

- **Principe du moindre privilège**  
  Consiste à limiter le plus possible les droits des utilisateurs et composants du système.  
  Sert à limiter l'exposition des données.  

- **Garder un système à jour**  
  en m'étant en place des mises à jour automatiques.  
  Sert à limiter le nombre de failes exploitable par l'attaquant et en règle générale permet de rencontrer le moins de bugs possible.

- **Tracabilité**
- **Disponibilité**
- **Integrité**
- **Confidencialité**
## Mises à jour de sécurité automatique
On utilise ==Unattended-Upgrades== pour mettre en place les mises à jour automatiques.  
À noter que Debian et Ubuntu disposent déjà de ce logiciel mais les mises à jour automatiques ne sont pas activées par defaut.  
### Méthode graphique
Disponible sur Ubuntu et Debian  

1. Ouvrir le logiciel "Software & Updates" ou "Logiciels et mises à jour" en français  
2. Proceder comme suit:  
![security-updates.png](../src/security-updates.png)  

La méthode en ligne de commande permet beaucoup plus de customisation.
### Méthode en ligne de commande
Le logiciel à configurer est ==Unattended-Upgrades==.
##### Installation
```console
# apt install unattended-upgrades apt-listchanges
```
##### Configuration  
Configure le comportement pour lancer une mise à jour
```title="/etc/apt/apt.conf.d/50unattended-upgrades"
Unattended-Upgrade::Origins-Pattern {
  "origin=Debian,codename=${distro_codename},label=Debian";
  "origin=Debian,codename=${distro_codename},label=Debian-Security";
  "origin=Debian,codename=${distro_codename}-security,label=Debian-Security";
};
Unattended-Upgrade::Remove-New-Unused-Dependencies "true";
Unattended-Upgrade::Remove-Unused-Dependencies "true";
Unattended-Upgrade::AutoFixInterruptedDpkg "true";
Unattended-Upgrade::MinimalSteps "true";
Unattended-Upgrade::OnlyOnACPower "true";
```
??? note "Détails"
    ==Unattended-Upgrade::Origins-Pattern { }== : définit quel paquets doivent être mis à jour  
    ==origin=Debian,...== : les dépôts  
    ==Remove-[New]-Unused-Dependencies== : un équivalent de `apt autoremove`, supprime les dépendances inutiles  
    ==AutofixInterruptedDpkg== : effectue une réparation automatique de dpkg dans le cas où le processus s'est arrêté de façon inattendue  
    ==MinimalSteps== : fragmente le processus de mise à jour en petits processus pour permettre l'arret du processus en pleine mise à jour  
    ==OnlyOnACPower== : effectue les mises à jour seulement sur secteur (utile pour les PC portables)
Configure le comportement lors d'une mise à jour
```title="/etc/apt/apt.conf.d/20auto-upgrades"
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Download-Upgradeable-Packages "1";
APT::Periodic::Unattended-Upgrade "1";
```
??? note "Détails"
    ==Update-Package-Lists== : met à jour la liste des paquets  
    ==Download-Upgradeable-Packages== : télécharge les mises à jour  
    ==Unattended-Upgrade== : effectue les mises à jour tout les X jours (dans ce cas: tout les jours)  
??? info "10periodic vs 20auto-upgrades"
    Il existe 2 fichiers avec la même syntaxe dans `apt.conf.d` : 10periodic et 20auto-upgrades.  
    Le premier est disponible par defaut sur certaines distributions alors que le deuxième est généré mannuelement.  
    **20auto-upgrades écrase les modifications de 10periodic.**

