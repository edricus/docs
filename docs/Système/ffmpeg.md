<figure markdown>
[![ffmpeg](../src/ffmpeg.svg){ width="300" }](https://ffmpeg.org/)
</figure>
# FFMPEG
est un logiciel de traitement de flux audio et vidéo en ligne de commande.  

### Exemple d'utilisation
La vidéo qu'on veut convertir possède les métadonnées suivantes (formaté à l'essentiel)  
```console
$ ffmpeg -i video.mkv
```
```
Input #0, matroska,webm, from 'video.mkv':
  Metadata:
    Stream #0:0(jpn): Video: hevc (Main 10), yuv420p10le(tv), 1920x1080, (default)
    Metadata:
      title           : Video
    Stream #0:1(jpn): Audio: opus, 48000 Hz, stereo (default)
    Metadata:
      title           : OPUS 2.0 (128kbps)
      ENCODER         : Lavc58.86.101 libopus
    Stream #0:2(jpn): Audio: opus, 48000 Hz, 5.1
    Metadata:
      title           : OPUS 5.1 (320kbps)
      ENCODER         : Lavc58.86.101 libopus
    Stream #0:3(fre): Subtitle: subrip (default)
    Metadata:
      title           : srt
      ENCODER         : Lavc58.134.100 srt
    Stream #0:4(fre): Subtitle: ass
    Metadata:
      title           : ass
      ENCODER         : Lavc58.134.100 ssa
```
On note 5 flux représentés par ==Stream #X:Y== :  

- ==#0:0== : Un flux vidéo en HEVC (x265) 10bits (yuv420p10le)  
- ==#0:1== : Une piste audio au format OPUS 2.0 ayant un bitrate de 120kbps  
- ==#0:2== : Une autre piste audio au format OPUS en 5.1 ayant un bitrate de 320kbps  
- ==#0:3== : Une piste de sous-titres au format SRT (de langue française)  
- ==#0:4== : Une autre piste de sous-titres au format ASS, aussi de langue française  

On veut optimiser ce fichier pour le streaming sur Jellyfin ou PLEX.  
Pour ça, le meilleur est :  

- Vidéo : mkv x264 8bits (yuv420p)  
- Audio : aac  
- Sous-titres : srt

#### Commande
```console
$ ffmpeg -i video.mkv -map 0:0 -map 0:1 -map 0:3 \
    -vcodec libx264 -crf 24 -vf format=yuv420p \
    -acodec aac \
    -scodec copy \
    video-converted.mkv
```
#### Explications

- `-i`  
Le fichier en entrée    
- `-map 0:0 -map 0:1 ...`  
On va sélectionner les flux qu'on désire garder.  
Ici, on a besoin que d'une seule piste audio et sous-titres, ainsi que la vidéo.  
Si on voulait garder tout, on aurait mis `-map 0`  
Ce paramètre n'est pas nécessaire avec un mp4 (il n'y a pas plusieurs flux audio/sous-titres)  
- `-vcodec libx264`  
Pour **video codec**  
On utilise le ==x264==, plus répandu que le ==x265==, c'est le plus compatible.  
Le x265 (ou HEVC) est son successeur et permet un meilleur rapport compression/qualité.  
Pour le moment, seulement Edge peut lire du x265.
- `-crf 18`  
Pour **constant rate factor**  
est un paramètre du codec x264, plus il est haut, plus la compression est forte.  
La valeur par défaut est ==23==, ont choisis ==18== ici pour préserver la qualité de l'image. 23 est tout de même acceptable.    
- `-acodec aac`  
pour **audio codec**  
On utilise le ==AAC== qui est le successeur du MP3. Largement répandu, il offre un meilleur rapport qualité/stockage que son ancêtre.  
Le format ==OPUS== est le successeur de AAC, remplissant la même fonction d'optimisation du rapport qualité/stockage.  
Il est assez récent et même si dorénavant compatible avec les dernières versions des navigateurs, on préférera le AAC pour les navigateurs encore en LTS.  
- `-scodec copy`  
Pour **subtitles codec**  
On utilise la fonction ==copy== pour préserver le format srt, pas besoin de convertir, c'est un vieux format très simple.  
Le format ==ASS== (pour Advanced SubStation Alpha) permet une grande variété d'effets aux sous-titres comme la rotation, la position, le délai d'apparition... Il n'est pas encore pris en charge par les navigateurs.  
- `video-converted.mkv`  
### Métadonnées après conversion
```console
$ ffmpeg -i video-output.mvk
```
```
Input #0, matroska,webm, from 'video-output.mkv':
  Metadata:
    Stream #0:0(jpn): Video: h264 (High), yuv420p(tv, progressive), 1920x1080 (default)
    Metadata:
      title           : [SR-71] X265 10BITS
      ENCODER         : Lavc58.134.100 libx264
    Stream #0:1(jpn): Audio: aac (LC), 48000 Hz, stereo (default)
    Metadata:
      title           : [SR-71] OPUS 2.0 (128kbps)
      ENCODER         : Lavc58.134.100 aac
    Stream #0:2(fre): Subtitle: subrip (default)
    Metadata:
      title           : srt
      ENCODER         : Lavc58.134.100 srt
```
:warning:  Les titres de flux ne sont pas représentatifs des réelles données, il faut regarder la ligne ENCODER

### Convertir des images en masse
Cette commande est un peu complexe pour la tâche, mais elle permet de ==remplacer== les anciennes images.
```console
$ find . -name '*.webp' -printf '%f\n' \
-exec bash -c 'ffmpeg -i "$1" "${1%?????}.png" && \
rm -rf "$1"' \
bash {} \+
```
??? note "Explication"
    **1ère ligne** : On utilise `find` pour chercher tous les fichiers en .webp dans le répertoire courant  
    **2ème ligne** : On exécute ffmpeg pour convertir le résultat de find ($1) en .png.  
    La syntaxe de ffmpeg est `ffmpeg -i input output`, on utilise "$1" pour placer le résultat du find et ${1%?????}.png va prendre le nom du fichier et soustraire les 5 derniers caractères (.webp). On ajoute ensuite ".png".  
    On aurait pu faire `ffmpeg -i $1 $1.png` pour pas se prendre la tête, mais ça aurait donné des fichiers ".webp.png".  
    **3ème ligne** : On supprime les .webp avec un `rm`  
    **4ème ligne** : pour conclure notre `-exec` qui exige de se terminer par `{}\+` ou `{}\;`.  
    `bash -c` est nécessaire pour utiliser la variable "$1" permettant de récupérer la sortie de find 
### Convertir une vidéo en images
```console
$ mkdir output && ffmpeg -i input.mp4 'output/%04d.png'
```
??? note "Explication"
    =='%04d'== où "04" est le nombre de chiffre dans le nom des fichiers.  
    J'ai mis la création d'un dossier "output" dans la commande pour éviter une catastrophe.  

### Convertir des images en vidéo
```console
$ ffmpeg -framerate 30 -pattern_type glob -i '*.png' \
  -c:v libx264 -pix_fmt yuv420p output.mp4
```
??? note "Explication"
    ==-pattern_type glob== permet d'utiliser un wildcard (*) en argument, permettant d'inclure tous les fichiers d'un repertoire.  
    ==-c:v libx264== pour le type d'encodage.  
    ==-pix_fmt yuv420p== définit l'espace colorimétrique à utiliser.  

### Transformer une vidéo 30fps en vidéo 60fps
On utilise pour ça ==l'interpolation.==  
Ce processus va générer une image entre deux.  
Ce processus est **très lent**, car il utilise un algorithme classique et ne pas être calculé sur le GPU, voir [cette réponse](https://stackoverflow.com/a/64380824) pour en savoir plus sur le fonctionnement de cette commande et d'autres méthodes.
```console
$ ffmpeg -i input.mp4 -vf minterpolate=fps=60:mi_mode=mci output60.mp4
```
Je l'utilise pour de très courtes vidéos (<10sec)  

### Utiliser le codec NVIDIA
L'utilisation du codec NVENC va reduire drastiquement le temps necessaire pour convertir une vidéo.  
Pour savoir si on dispose de ce codec, on fait une recherche :  
```console
$ ffmpeg -encoders | grep nvenc
```
```
. . .
 V....D h264_nvenc           NVIDIA NVENC H.264 encoder (codec h264)
 V....D hevc_nvenc           NVIDIA NVENC hevc encoder (codec hevc)
```
Ici, on dispose du codec "h264_nvenc" et "hevc_nvenc"  
Celui qu'on va utiliser est "h264_nvenc", qui est la version NVIDIA de "libx264", il prends les mêmes paramètres.  
Du coup, il suffit juste de remplacer "libx264" par "h264_nvenc".  
```console
$ ffmpeg -i video.mkv \
    -map 0:0 -map 0:1 -map 0:2 -map 0:3 \
    -vcodec h264_nvenc -crf 24 -vf format=yuv420p \
    -acodec aac \
    -scodec copy \
    video-converted.mkv
```
