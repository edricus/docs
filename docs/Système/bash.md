<figure markdown>
![bash](../src/bash.svg){ width="250" }
</figure>
# Bash
pour Bounce Again SHell est un interpreteur et un language de programmation developpé par le projet GNU et succèsseur de SH.  
C'est l'interpreteur par défaut sur la majorité des système basés sur Unix.  

### Exiger l'execution en mode root
```
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi
```

### Télécharger la dernière release d'un projet sur Github
```
curl -s https://api.github.com/repos/jgm/pandoc/releases/latest \
| grep "browser_download_url.*deb" \
| cut -d : -f 2,3 \
| tr -d \" \
| wget --show-progress -qi -
```
### Sed
pour Stream EDitor, est un logiciel de la suite GNU servant à manipuler un flux textuel.  

#### Substitute  
est la fonction la plus utilisée de sed, elle sert à remplacer un motif par un autre  
`g` = permet de remplacer tout les motifs, se place à la fin `s/chercher/replacer/g`  

=== "sed"
	```console
	$ sed 's/texte à remplacer/texte remplacé/' -i fichier.txt
	```
=== "texte"
	=== "avant"
		```
		texte à remplacer
		```
	=== "après"
		```
		texte remplacé
		```


#### Insertion  
permet d'inserer du texte avant ou après un motif  
`a` = append, insert le texte après le motif  
`i` = insert, insert le texte avant le motif

Après le motif

=== "sed"
	```console
	$ sed '/option/a Hello World' fichier.txt
	```
=== "texte"
	=== "avant"
		```
		exemple
		option
		```
	=== "après"
		```
		exemple
		option
		Hello World
		```

Avant le motif

=== "sed"
	```console
	$ sed '/option/i Hello World' fichier.txt
	```
=== "texte"
	=== "avant"
		```
		exemple
		option
		```
	=== "après"
		```
		exemple
		Hello World
		option
		```


