![vagrant](../src/vagrant.png){ align=left width=150 }
# Vagrant
est un logiciel utilisé pour la création et la configuration de machines virtuelles. Il s'oppère avec des Vagrantfile contenant toutes les instructions servant à deployer notre infrastructure virtuelle.  

## Installation
On l'installe à l'aide d'un dépôt externe  
```console
$ wget -O- https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
$ echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
$ sudo apt update && sudo apt install vagrant
```

## Premier deployement
Le but sera de créer une machine Debian 12 sur Virtualbox depuis Vagrant  
On va créer un dossier pour accueilir notre Vagrantfile  
```console
$ mkdir Debian12
$ cd Debian12
```
On va ensuite récuperer un Vagrantfile depuis les dépôts de Vagrant
```console
$ vagrant init debian/bookworm64
```
On a maintenant un Vagrantfile dans notre dossier
```
total 4
-rw-r--r-- 1 edricus edricus 542 Jun 21 14:01 Vagrantfile
```
Vagrant a aussi créé un dossier caché .vagrant qui va contenir toutes les informations sur la VM.  
On va deployer cette VM
```console
$ vagrant up
```
Après ça on peut se connecter en ssh avec une commande Vagrant
```console
$ vagrant ssh
```
```
Linux bookworm 6.1.0-9-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.27-1 (2023-05-08) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
vagrant@bookworm:~$
```
On peut supprimer cette VM avec cette commande
```console
$ vagrant destroy
```

## Plugin VMWare
Vagrant n'est pas capable de deployer des VM VMWare par defaut.  
Il faut installer le plugin ==vagrant-vmware-utility==.  
### Installation
```console
$ curl -s https://api.github.com/repos/hashicorp/vagrant-vmware-desktop/releases/latest \
| grep "browser_download_url.*deb" \
| cut -d : -f 2,3 \
| tr -d \" \
| wget --show-progress -qi -
```
```console
$ sudo apt install ./vagrant-vmware-utility_*_amd64.deb -y
```
### Configuration
Après un `vagrant init`, ajouter dans le fichier Vagrant file le bloc suivant :
```
  config.vm.provider :vmware_desktop do |vmware|
    vmware.utility_certificate_path = "/opt/vagrant-vmware-desktop/certificates"
    vmware.force_vmware_license = "professional" # when doing it here it works
    vmware.gui = true
  end
```
== en dessous de Vagrant.configure("2") do |config| ==
### Lancement
```console
$ vagrant up
```
