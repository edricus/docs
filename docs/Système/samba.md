<figure markdown>
![samba.png](../src/samba.png){ width="350" }
</figure>

# Samba
est un logiciel de partage de fichier qui utilise le protocole propriétaire (Microsoft) SMB.  

## Installation
```console
# apt install samba
```

## Configuration
Ma configuration perso pour un partage privé :  
```title="/etc/samba/smb.conf"
[global]
workgroup = WORKGROUP
disable netbios = yes
security = user
map to guest = never
create mask = 0640
directory mask = 0740
valid users = edricus
log file = /var/log/samba/log.%m
max log size = 1000
logging = file

[media]
comment = Fichiers téléchargés
path = /media/data/
public = no
writable = no
printable = no
```
### **Paramètres pour [global]**
!!! note "Il s'agit juste des paramètres principaux de samba, la liste complète est disponible sur la [documentation officielle de samba](https://www.samba.org/samba/docs/current/man-html/smb.conf.5.html)"
- workgroup  

Le nom du groupe de travail  
Utilisé par Windows quand il va déterminer le "voisinage réseau"  
par défaut: WORKGROUP  

- netbios name   

Le nom NetBIOS  
Utilisé pour définir le nom du PC sur le réseau  

- disable netbios  

*Booléen*  
Utilisé pour désactiver la fonction de découverte de samba, préférable quand on a pas besoin d'annoncer le serveur sur le réseau  

- security  

Le type de sécurité  
- `user` : (défaut) demande un nom d'utilisateur/mot de passe pour lancer une session.  
L'utilisateur pourra monter plusieurs partages sans avoir à rentrer ses identifiants à chaque montage  
- `share` : demande uniquement un mdp  
L'utilisateur devra renseigner ce mdp à chaque fois qu'il souhaite monter un partage  

- map to guest  

Action en faire en cas de connexion sans identifiants  
- `never` : (défaut) renvoi une erreur  
- `bad user` : l'utilisateur se connecte en tant qu'invité  
- `bad password` : entre-deux bizarre où un utilisateur ayant mal tapé son mot de passe se voit authentifié en tant qu'invité, ça sans d'avertissement  

- guest account  

L'utilisateur unix utilisé pour le compte invité  

- create mask  

Les autorisations à la création de fichiers  

- directory mask  

Les autorisations à la création de dossiers  

- valid users  

Les utilisateurs et groupes autorisés à s'authentifier, séparés par une virgule  
Les groupes se définissent par "@" devant le nom, ex: @famille  

- log file

Emplacement et nom du fichier de log

- max log size  

Taille maximale du fichier de log en Kio  

- logging

Le backend a utiliser pour le logging  
[plus d'infos ici](https://www.samba.org/samba/docs/current/man-html/smb.conf.5.html#logging), chercher "logging (G)"
### **Paramètres pour [partage]**
Après [global], on définit nos partages.  
Le nom est choisi par l'utilisateur, dans ma configuration j'ai choisis "media"  

- comment  

Commentaire pour décrire le partage  

- path  

Le chemin du dossier à partager  

- public  

*Booléen*  
Défini si le partage est disponible pour les invités  

- guest ok  

*Booléen*  
Pareil que "public"  

- only guest  

*Booléen*  
Défini le partage comme seulement accessible aux invités  

- writable  

*Booléen*  
Défini le partage comme modifiable (yes) ou en lecture seule (no)  

- printable  

*Booléen*  
Autorise ou non l'impression des documents du partage  

## Gestion des utilisateurs
Samba utilise une base de donnée pour stocker les informations des utilisateurs.  
Certaines distributions linux synchronise automatiquement les utilisateurs unix dans la base de donnée samba, les commandes suivantes ne sont donc pas nécessaires pour ces distributions et les accès sont gérés à partir du fichier **smb.conf** avec le paramètre **valid users**  

```console title="Ajouter un utilisateur à la base de donnée"
# smbpasswd -a UTILISATEUR MOTDEPASSE
```
```console title="Supprimer un utilisateur de la base de donnée"
# smbpasswd -x UTILISATEUR
```
```console title="Désactive l'accès d'un utilisateur"
# smbpasswd -d UTILISATEUR
```
```console title="Réactive l'accès d'un utilisateur"
# smbpasswd -d UTILISATEUR
```

## Utilisation
Samba est géré par le service **smbd**
```console
# systemctl enable --now smbd
```


