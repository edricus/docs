# Commandes système en vrac

### "Unknown display" après avoir installer les drivers NVIDIA

[bug2060268](https://bugs.launchpad.net/ubuntu/+source/linux/+bug/2060268){ target:about_blank }  
```console
$ sudo rm /dev/dri/card0
```


### Afficher la temperature des HDD
```console
# modprobe drivetemp
```
Le charger au démarrage :
```console
# echo 'drivetemp' >> /etc/modules
```

### Copier/Coller à partir d'un TTY
```console
# apt install screen
# screen -S <nom-au-pif>
```
Dans screen : 

  - Ctrl+A + [ : copier
  - Entrée (en mode selection) : commencer/terminer la copie
  - Ctrl+A + ] : coller

### Chroot depuis un ISO live
```console
$ sudo mount /dev/<partition-root-à-monter> /mnt
$ for i in {dev,sys,proc,sys/firmware/efi/efivars,run,udev}; do sudo mount --bind /$i /mnt/$i; done
$ sudo chroot /mnt
```
Ignore les erreurs "Impossible de trouver..." de la commande mount

### Réparer GRUB

  - Démarrer sur un ISO live (Ubuntu)  
  - Ouvrir un terminal  
  - Reperez la partition root et celle de l'EFI : `lsblk`  
  - Monter la partition root : `sudo mount /dev/sdXY /mnt`  
  - Monter la partition efi  : `sudo mount /dev/sdXY /mnt/boot/efi`   
  - Monter les dossiers spéciaux : 
  ```bash
  for i in {dev,sys,proc,run,sys/firmware/efi/efivars}; do sudo mount --bind /$i /mnt/$i; done
  ```
  - Installer GRUB : `sudo grub-install /dev/sda` :warning: Ne pas spécifier une partition  
  - Se chroot dans le système : `sudo chroot /mnt`  
  - Mettre à jour GRUB : `update-grub`  

### Ajouter widevine à Chromium (Netflix, Spotify...)
Chromium n'a pas de support pour widevine par defaut, il est alors impossible de lire du contenu protégé par DRM comme Netflix et Spotify.  
Une solution radicale et d'installer Google Chrome qui dispose de widevine par defaut.  
Le script suivant importe le module dans chromium
!!! warning "Il est necessaire de faire ces étapes à chaque mise à jour de chromium"
```console
$ git clone https://github.com/proprietary/chromium-widevine
$ cd chromium-widevine 
$ ./use-standalone-widevine.sh
$ cd .. && rm -r chromium-widevine
```
??? note "Installation de Google Chrome"
    ```console
    $ wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
    $ sudo dpkg -i google-chrome-stable_current_amd64.deb
    $ rm google-chrome-stable_current_amd64.deb
    ```
### Changer la couleur dans un terminal
Usage: printf '<code\>'
```
\[\033[0m\]       # Text Reset
\[\033[0;30m\]    # Black
\[\033[0;31m\]    # Red
\[\033[0;32m\]    # Green
\[\033[0;33m\]    # Yellow
\[\033[0;34m\]    # Blue
\[\033[0;35m\]    # Purple
\[\033[0;36m\]    # Cyan
\[\033[0;37m\]    # White
```
### Partition Ext4 partagée entre tout les utilisateurs
Sur une partition autre que ext4, il suffit d'ajouter "umask=0000" dans fstab  
Sur l'ext4 le dossier a besoin d'avoir des permissions unix  
Sur Ubuntu le groupe "users" regroupe tous les utilisateurs  
Sur Debian il faut ajouter manuellement son utilisateur à ce groupe avec `adduser`
```console
$ chown -R :users /folder
$ chmod -R g+rw /folder
$ sudo adduser $USER users 
```
### MySQL, drop all tables
##### Démarrer MySQL en mode silencieux
```
# mysql -u root --silent --raw
```
##### Lister les commandes
```
SELECT concat('DROP TABLE IF EXISTS `', table_name, '`;') FROM information_schema.tables WHERE table_schema = 'NOM DE LA DB';
```
##### Selectionner la base de donnée
```
USE NOM_DE_LA_DB;
```
Et CTRL+V
Vérifier avec `show tables` au cas où et `drop table 'nom du tableau'` si jamais il en reste une

### Script au démarrage de GNOME
``` title="~/.config/autostart/appname.desktop"
[Desktop Entry]
Name=Mon script
GenericName=Une description descriptive
Comment=Un commentaire de la description descriptive
Exec=/chemin/du/script.sh
Terminal=false
Type=Application
X-GNOME-Autostart-enabled=true
```    
### Changer la vitesse de défillement

```bash
apt install imwheel
```
```title="~/.imwheelrc`"
".*"
None,      Up,   Button4, 3
None,      Down, Button5, 3
```
"3" est le nombre de pages à défiller  
Additionnelement, changer le profile d'acceleration en "Adaptatif" dans Tweaks

### Flatpak
```console
# apt install flatpak
# flatpak remote-add flathub https://flathub.org/repo/flathub.flatpakrepo
```

### Problèmes extremement spécifiques

Bug graphique pour les CPU AMD A9 9420
```title="vim /etc/xorg.conf"
Section "Device"
   Identifier "AMD"
   Driver "amdgpu"
   Option "TearFree" "true"
EndSection
```

Sensibilité verticale du touchpad sur HP Pavilion G6
```
cp /usr/share/X11/xorg.conf/d/70-synaptics.conf /etc/X11/xorg.conf.d/70-synaptics.conf
```
```title="vim /etc/X11/xorg.conf.d/70-synaptics.conf"
Section "InputClass"
   Identifier "touchpad catchall"
   Driver "synaptics"
   MatchIsTouchpad "on"
   Option "TransformationMatrix" "1 0 0 0 0.443181 0 0 0 1"
EndSection
```
### PGP
Erreur : `gpg: échec de réception depuis le serveur de clefs : Erreur générale`
```title="vim /etc/pacman.d/gnupg/gpg.conf"
keyserver pool.sks-keyservers.net
```
```
gpg --keyserver pool.sks-keyservers.net --recv-keys <CLÉE À AJOUTER>
```

