<figure markdown>
![docker](../src/docker.webp){ width="300" }
</figure>

# Docker
est un moteur de conteneurisation.  
La portativité fait sa grande puissance, on a plus à se soucier des versions des applications et des dépendances, docker promet de pouvoir déployer les applications sans se soucier de l’OS.  
## Notions
- ### Conteneurs  
  une instance d’une ==image== en cours d'exécution.  
  C’est un environnement isolé qui exécute l’application.  
  Le conteneur est en lecture/écriture.
- ### Image  
  c'est une sauvegarde d'un ==conteneur== qu'on peut exporter et exécuter.  
  Une image devient un conteneur losqu'elle est executée.  
  L'image est en lecture seule.
- ### Noeud  
  ou node, est une machine sur laquelle Docker va s'exécuter pour instancier des conteneurs.  
  Il peut s’agir d’une machine physique ou d’une machine virtuelle.
- ### Dépôts  
  zone de stockage des images sur le réseau, le dépôt de référence sur internet est [Docker Hub](https://hub.docker.com/search?type=image) 
- ### Dockerfile
  un fichier permettant de créer des images à partir de commandes ou d’instructions.  
  On télécharge généralement une image de base et on lui ajouter des paquets et des fichiers de configuration pour créer une nouvelle image plus complete et spécifique pouvant être utilisée plus tard par exemple dans un ==docker-compose==.
- ### Docker-compose 
  est le système d’orchestration le plus simple permettant de déployer plusieurs conteneurs.  
  On s'en sert pour deployer une ==application== utilisant plusieurs conteneurs pour fonctionner (exemple: un site complexe a besoin d'un serveur apache et d'une base de données)
- ### Volume  
  une zone de stockage pouvant être attaché à un conteneur afin qu’il puisse conserver des données après son extinction  

### Composition de Docker
![docker-diagram.png](../src/docker.drawio.png)


## Installation
```console
# apt install docker.io
```

### Autoriser Docker à s'executer en mode utilisateur
```console
# groupadd docker
# usermod -aG docker $USER
# newgrp docker
```

### Commandes de base
| Commande | Action |
| :--- | :--- |
| <h3>Images</h3> |
| `docker info` | Information du moteur docker |
| `docker inspect` | Inspecter les objets docker |
| `docker run` | Instance une image en conteneur |
| `docker start` | Lancer un conteneur déjà créé |
| `docker build` | Construire l'image depuis un ==dockerfile== |
| `docker images` | Lister les images présentes en local |
| `docker rmi <image>` | Supprime une image (-f pour tuer le conteneur) |
| `docker pull <image>:<tag>` | Télécharger une image |
| <h3>Conteneurs</h3> |
| `docker ps -a` | Lister les conteneurs |
| `docker rm -rf <conteneur>` | Supprimer un conteneur |
| <h3>Volumes</h3> |
| `docker volume ls` | Lister les volumes |
| `docker volume rm` | Supprimer un volume |
| `docker volume prune` | Supprimer les volumes inutilisés |
| <h3>Réseau</h3> |
| `docker network ls` | Lister les interfaces docker |
| `docker network rm` | Supprimer une interface docker |
| `docker run --network host` | Démarrer un conteneur sans isoler le réseau (mode bridge) |
| `docker run --network bridge` | Démarrer un conteneur en l'isolant du réseau (mode NAT) |

!!! warning "Sur Docker on appelle "bridge" un réseau en NAT et "host" un réseau en bridge"
??? info "Rappel NAT et Bridge"
    <h4>Bridge</h4>la machine va se connecter sur le même réseau que l'hôte, elle est donc ==visible== par toutes machines sur le même réseau  
    <h4>NAT</h4>la machine va utiliser l'adresse IP de l'hôte pour communiquer sur le réseau, elle est donc ==invisible== par toutes machines sur le même réseau

### Commandes avancés
```bash title="Supprime toutes les images"
docker rmi -f $(docker images -aq)
```
```bash title="Supprime tous les conteneurs"
docker rm -rf $(docker ps -aq)
```
```bash title="Créer une image à partir d'un dockerfile"
docker build -t <nom>:<tag> - < dockerfile
```
```bash title="Supprime tous les volumes"
docker volume rm -f $(docker volume -q)
```
```bash title="Purger docker (supprime images, conteneurs, volumes et interfaces réseau)"
killall dockerd
docker rm -f $(docker ps -aq)
docker rmi -f $(docker images -aq)
docker volume rm -f $(docker volume ls -q)
docker network prune -f
```
```bash title="Executer une commande à l'interieur du conteneur"
docker exec <id_conteneur> <commande-à-executer>
```
```bash title="Executer une commande à l'interieur du conteneur en tant qu'utilisateur www-data"
docker exec <id_conteneur> --user www-data <commande-à-executer>
```
```bash title="Ouvrir une console interactive à l'interieur d'un conteneur"
docker exec -it <id_conteneur> /bin/bash
```

## Dockerfile
Un dockerfile est un document texte contenant toutes les commandes servant à contruire une image
### Exemple d'un Dockerfile
``` Docker linenums="1"
FROM ubuntu:20.04

RUN apt-get update && apt-get full-upgrade -y

RUN apt-get install git -y
RUN git clone https://github.com/creativeimofficial/awesome-landing-page.git /var/www/html/

ARG DEBIAN_FRONTEND=noninteractive

EXPOSE 80
CMD apachectl -D FOREGROUND
```
==FROM== : la base de l’image, on récupère l’image de ubuntu 20.04  
==RUN== : ce qu’on ajoute à l’image  
==ARG== : on ajoute une variable d'environnement
!!! quote "`DEBIAN_FRONTEND=noninteractive` permet de passer les interactions avec l'utilisateur lors de la configuration des paquets comme les messages de confirmation. Attention, il y a des paquets qui estime certaines interactions obligatoires, dans ce cas il faudra utiliser la methode [debconf-set-selections](https://sleeplessbeastie.eu/2018/09/17/how-to-read-and-insert-new-values-into-the-debconf-database/)"

==EXPOSE== : pour ouvrir un port  
==CMD== : la commande à exécuter dans le conteneur, cette instruction doit toujours se trouver à la fin du dockerfile, il s'agit du processus attaché au conteneur.  
??? quote "Ne pas utiliser Docker de la même manière qu'une machine virtuelle"
    Une mauvaise pratique est d'utiliser docker comme une machine virtuelle et installer ssh dans son conteneur pour s'y connecter depuis son hôte, voir à distance. Même si dans certains cas on peut utiliser docker comme un lab interactif, il est préférable d'utiliser `docker exec -it /bin/bash <conteneur>` pour interagir avec lui dans le cas où l'on voudrait rajouter des logiciels APRÈS la création de l'image. Dans l'ideal on construit notre image avec tout ses paquets et sa configuration pour ensuite executer qu'un seul processus par conteneur." 

## Docker Compose
est un logiciel permetant de deployer plusieurs conteneurs et les relier entre eux, on nomme ce nouvel objet une application.  

### Installation
```console
# apt install docker-compose
```

### Commandes de base
!!! info "Généralement on se place dans le repertoire où se trouve le docker-compose.yml avant d'executer ces commandes mais il est possible de spécifier son emplacement avec l'option -f"
| Commande | Action |
| :--- | :--- |
| `docker-compose up` | Démarre l'application au premier plan |
| `docker-compose up -d` | Démarre l'application en arrièreplan |
| `docker-compose down` | Arrête l'application |
| `docker-compose logs` | Affichae les informations d'une application en arrière plan |
| `docker-compose exec` <conteneur> <commande> | Execute une commande dans le conteneur |

### Exemple d'un fichier docker-compose
``` Docker linenums="1" title="docker-compose.yml"
version: '2.1'
volumes:
  nextcloud:
  db:
  redis:

services:
  db:
    image: mariadb
    container_name: mariadb
    restart: always
    volumes:
      - db:/var/lib/mysql
    environment:
      - MYSQL_ROOT_PASSWORD:${MYSQL_ROOT_PASSWORD}
      - MYSQL_DATABASE:${MYSQL_DATABASE}
      - MYSQL_USER:${MYSQL_USER}
      - MYSQL_PASSWORD:${MYSQL_PASSWORD}
    env_file:
      - .env
 
  redis:
    image: redis:latest
		container_name: redis
    restart: always
    volumes:
      - redis:/var/lib/redis 
  
  app:
    image: nextcloud
		container_name: nextcloud
    restart: always
    ports:
      - 9001:80
    links:
      - db
      - redis
    volumes:
      - nextcloud:/var/www/html
      - /media/data/nextcloud:/var/www/html/data
    environment:
      - MYSQL_DATABASE:${MYSQL_DATABASE}
      - MYSQL_USER:${MYSQL_USER}
      - MYSQL_PASSWORD:${MYSQL_PASSWORD}
      - MYSQL_HOST=db
      - REDIS_HOST=redis
      - OVERWRITEPROTOCOL=https
    env_file:
      - .env

  update:
    image: containrrr/watchtower
		container_name: watchtower
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
```
```console
$ docker-compose up -d
```
Ce docker-compose deploie un serveur Nextcloud sur le port 9001.  

**MariaDB** - comme base de données  
**Redis** - est aussi une base de données, mais va s'occuper du cache  
**Nextcloud** - est une application pour le stockage de fichiers, contacts, calendrier, ect...  
**Watchtower** - va effectuer des mises à jour automatique des images, à ne pas utiliser en production, on fixe généralement des versions...  

### Composition d'un docker-compose
- version  

La version de la syntaxe à utiliser, change en fonction des versions, [en savoir plus](https://docs.docker.com/compose/compose-file/compose-versioning/)  

- volumes  

Cette rubrique va créer des volumes dans le repertoire `/var/lib/docker/volumes` qu'on va ensuite utiliser comme stockage persistant pour nos conteneurs (rappel: sans volumes les données sont perdues à l'arrêt des conteneurs)  

- services  

C’est dans cette rubrique qu'on va définir nos conteneurs  

- db,redis,nextcloud,etc...

Les conteneurs en question, on les nommes comme on veut, pas forcement pareil que l'image.  

- image   

L'image à utiliser comme base, elle peut être distante ou locale, par defaut l'image va être téléchargée depuis [hub.docker.com](https://hub.docker.com/search?type=image) mais on peut utiliser une image construire auparavant avec un **dockerfile** par exemple  
Un nom seul fait référence à l'image officielle  
Le **:** sert à indiquer le **tag**  
Le **/** est utilisé pour les images non-officielles, et indiquent l'auteur, par exemple on peut utiliser l'image Nextcloud de linuxserver avec linuxserver/nextcloud  

- container_name  

(facultatif), nomme le conteneur  

- restart: always  

Sert à redémarrer le conteneur en cas en fermeture non prévue. L'arret manuel du conteneur ne déclanche pas son redémarrage  

- volume  

Défini le volume utilisé dans ce service, **dossier-local/dossier-dans-le-conteneur**  

- ports  

Expose un port, **port-local/port-dans-le-conteneur**  
Si ce paramètre n'est pas défini, le port ne sera pas exposé de notre côté mais il sera toujours exposé dans le réseau interne de l'application.  
Le conteneur "nextcloud" peut acceder à la base de donnée puisque celle-ci s'expose automatiquement en 3306 dans le réseau interne.  
!!! quote "On peut aussi changer le port interne sans l'exposer à l'exterieur, **:3333**, ou exposer le port de notre côté sans toucher à celui par defaut **3333:**"  

- environment  

Défini des variables d'environnement qui vont être utilisés lors de la génération de l'image. Ici on les défini avec d'autres variables, contenues dans le fichier **.env** se trouvant dans le repertoire du docker-compose.  
!!! quote "Il est important de ne **PAS définir de mot de passes dans le docker-compose, surtout si l'on souhaite le mettre en ligne (comme souvant)**"  

- env_file  

On défini le chemin vers notre .env, qui va être utilisé pour les variables d'environnement  

- links  

Indique les services devant être démarrés **avant** ce service, ici on doit initialiser la base de données avant nextcloud, car celui-ci va devoir accèder à des informations dans cette base de données

### Fichier .env
On le place généralement au même endroit que le docker-compose, mais il peut être placé n'importe où puisque qu'on l'invoque avec l'option `env_file` dans le docker-compose
La syntaxe est assez simple
```title=".env"
VARIABLE1=valeur1
VARIABLE2=valeur2
```
Pour éviter tout problème lors de l'utilisation de ses variables on les appelles sous cette forme dans le docker-compose: `${VARIABLE1}`

### Execution au démarrage
Nos applications doivent survivre à un reboot de la machine.  
On créé un service pour ça
```title="/etc/systemd/system/nextcloud-docker.service"
[Unit]
Description=Nextcloud docker-compose service
Requires=docker.service
After=docker.service

[Service]
Type=oneshot
RemainAfterExit=yes
WorkingDirectory=/opt/nextcloud
ExecStart=docker-compose up -d
ExecStop=docker-compose down
TimeoutStartSec=0

[Install]
WantedBy=multi-user.target
```
!!! warning "L'element le plus important est ==WorkingDirectory==, c'est le dossier où se trouve le docker-compose"
On active ensuite notre service
```console
# systemctl daemon-reload
# systemctl enable --now nextcloud-docker
```

![portainer.svg](https://cdn.worldvectorlogo.com/logos/portainer.svg){ width=80 align=left }  
## Portainer
gerer ses conteneurs avec une interface graphique (web)  

### Installation
On commence par créer un volume dedié
```console
$ docker volume create portainer_data
```
Et lancer portainer en standalone
```console
$ docker run -d -p 9443:9443 --name portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce
```
Portainer est maintenant accessible sur le port 9443 (https).  

![watchtower.svg](https://containrrr.dev/watchtower/images/logo-450px.png){ width=110 align=left }  
## Watchtower
met à jour automatiquement ses images docker  

### Installation
On fait un docker-compose à part
```
version: "3"
services:
  watchtower:
    image: containrrr/watchtower
    container_name: watchtower
    environment:
      - WATCHTOWER_LABEL_ENABLE=true
      - WATCHTOWER_POLL_INTERVAL=43200 # 12 hours
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
```

- `WATCHTOWER_LABEL_ENABLE=true` : active la mise à jour seulement quand le container a le label `com.centurylinklabs.watchtower.enable=true`  
- `WATCHTOWER_POLL_INTERVAL` : intervale de recherche de mise à jour en secondes  

### Configuration
Pour activer la mise à jour automatique d'un container, on ajoute le label `com.centurylinklabs.watchtower.enable=true` dans le bloc 'labels' du container que l'on souhaite mettre à jour
```
---
services:
  app:
    image: <...>
    environment:
      - <...>
    volumes:
      - <...>
    labels:
      - "com.centurylinklabs.watchtower.enable=true"
```

### Lexique
  - rc  
  **Release Candidate**, dans un tag, il s'agit d'une sous-version. Si on ne veut pas mettre à jour vers une version plus haute, on peut mettre à jour vers un tag rc.  
  - fpm  
  **FastCGI Process Manager**, dans un tag, c'est un implémentation de FastCGI dans PHP. FastCGI permet de meilleures performances sur les applications web les plus chargées, de telles images demandent une configuration adaptée du serveur web.
