<figure markdown>
[![git.svg](../src/git.svg){ width=250 }](https://git-scm.com/)
</figure>
# Git
est le logiciel de versionning libre et open-source le plus populaire.  
Il a été devoloppé par Linus Torvard, le créateur du noyau Linux.  

## Lexique
- ==Cloner==  
télécharger le projet en local.  
- ==Commit==  
enregistrer des modifications en local.  
- ==Push==  
publier les modifications vers un dépôt distant.  
- ==Gitlab/Github==  
sont des hébergeur de projets git.  
- ==Branche==  
est une version du projet, la principale se nomme "main" ou "master".  
Le developpeur peut créer des branches supplémentaires, en général les modifications apportées au projets doivent se faire sur une branche intermediaire avant de basculer les modifications sur la main.  
- ==Merge==  
fusionner deux branches.  
- ==LFS==  
pour Large File Storage est une fonctionnalité de Git permetant de stocker les fichiers volumineux  

## Commandes  
#### Cloner un projet
```console
$ git clone <url du projet>
```
#### Cloner un projet avec prise en charge de clé SSH
!!! info "Remplacer `gitlab.com` par `github.com` si le projet est hébérgé sur GitHub"
```console
$ git clone git@gitlab.com:<utilisateur>/<projet>
```
#### Créer une nouvelle branche
```console
$ git checkout -b <branche>
```
#### Créer une nouvelle branche depuis une autre branche
```console
$ git checkout -b <branche> <branche source>
```
#### Ajouter des fichiers à commit
```console
$ git add <fichier à ajouter>
```
#### Effectuer le commit
```console
$ git commit -m "message de commit"
```
#### Effectuer un commit en ajoutant tout les fichiers modifiés automatiquement
!!! warning "N'ajoute pas les fichiers nouvellement créés"
```console
$ git commit -a -m "message de commit"
```
#### Supprimer un fichier
!!! warning "Si le fichier a été supprimé en oubliant cette commande, le fichier reste dans l'archive de git, augmentant le volume du projet dans le temps"
```console
$ git rm <fichier à supprimer>
```
#### Fusionner deux branches
```console
$ git checkout <branche destination>
$ git merge <branche source>
```
#### Publier les modifications
```console
$ git push
```
#### Publier les modifications sur une nouvelle branche  
```console
$ git push -u origin <branche>
```
#### Nettoyer le repertoire .git
À mesure des commits, un projet peut devenir très volumineux.  
Notamment le repertoire "objects" contenant tous les fichiers modifiés dans le temps.  
Cette commande supprime les modifications les plus anciennes.
```console
$ git gc --aggressive --prune
```
#### Historique des commits
```console
$ git history
```
#### Retourner à un ancien commit
```console
$ git revert <commit>
```
#### Supprimer le dernier commit
Ne pas hésiter à supprimer le projet en local et à le re-cloner si vraiment on souhaite revenir au dernier commit à distance.  
```console
$ git reset --hard
```
## Gitlab
est un site permetant d'heberger les projets Git.  
Il est une excellente alternative à Github puisque ce dernier a été racheté par Microsoft en 2018.  
Gitlab permet de mettre des projets en privé comparé à Github qui a une politique aggrésive de l'open-source.  
#### Créer un nouveau projet
![gitlab-1.png](../src/gitlab-1.png){ width="500" }  
#### Configurer la visibilité
![gitlab-2.png](../src/gitlab-2.png){ width="400" }
![gitlab-3.png](../src/gitlab-3.png){ width="500" }
#### Supprimer un projet
![gitlab-2.png](../src/gitlab-2.png){ width="500" }  
![gitlab-4.png](../src/gitlab-4.png){ width="500" }
![gitlab-5.png](../src/gitlab-5.png){ width="500" }

