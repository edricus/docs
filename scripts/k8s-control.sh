#!/bin/bash
sudo kubeadm init --pod-network-cidr 192.168.0.0/16 --kubernetes-version 1.28.0
mkdir -p $HOME/.kube  
sudo cp /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
while [[ $(kubectl get nodes | grep NotReady) ]]; do
  echo "not ready"
  sleep 5
done
kubeadm token create --print-join-command


