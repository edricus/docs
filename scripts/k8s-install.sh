#!/bin/bash
# Modules setup
cat << EOF > /etc/modules-load.d/containerd.conf
overlay
br_netfilter
EOF
modprobe overlay 
modprobe br_netfilter
# System setup
cat << EOF > /etc/sysctl.d/k8s.conf         
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF
sysctl --system
# Containerd setup
apt update && apt install containerd -y
mkdir -p /etc/containerd
containerd config default > /etc/containerd/config.toml
systemctl restart containerd
# K8s setup
swapoff -a
apt install apt-transport-https curl gpg -y
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.28/deb/Release.key | gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.28/deb/ /' > /etc/apt/sources.list.d/kubernetes.list
apt update
apt install kubelet=1.28.0-1.1 kubeadm=1.28.0-1.1 kubectl=1.28.0-1.1 -y
apt-mark hold kubelet kubeadm kubectl

